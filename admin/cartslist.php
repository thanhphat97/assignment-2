<?php include_once './header.php';
?>
 <div class=" pl-0 pr-0" id="detail-menu">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Cart</li>
                        <li class="breadcrumb-item active" aria-current="page">Total Cart</li>
                    </ol>
                </nav>
                <div class="container-fluid">
                    <div class="content">
                        <div class="container-fluid mr-0 ml-0 pl-0 pr-0">
                            <div class="content-header">
                                <h3>
                                    <i class="fa fa-list"></i>
                                    Cart List
                                </h3>
                            </div>
                            <div class="content-body">
                                <table class="table table-bordered">
                                  <thead>
                                    <tr>
                                        <th scope="col">
                                            <input type="checkbox" aria-label="Checkbox for following text input">
                                        </th>  
                                        <th scope="col">Order ID</th>
                                        <th scope="col">User Name</th>
                                        <th scope="col" >Product Name</th>
                                        <th scope="col" >Quantity</th>
                                        <th scope="col" >Day order</th>
                                        <th scope="col" >Method</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                        <td>
                                            <input type="checkbox" aria-label="Checkbox for following text input">
                                        </td>
                                        <td>01</td>
                                        <td>Thanh Phat</td>
                                        <td >Sách Kinh Tế</td>
                                        <td >100</td>
                                        <td >15/10/2018</td>
                                        <td >cod</td>
                                        <td>
                                            <button type="button" class="btn btn-danger" data-placement="top" title="Delete">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#id03" data-placement="top" title="Edit">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="checkbox" aria-label="Checkbox for following text input">
                                        </td>
                                        <td>02</td>
                                        <td>Thanh Phat</td>
                                        <td >Sách Kinh Tế</td>
                                        <td >100</td>
                                        <td >15/10/2018</td>
                                        <td >cod</td>
                                        <td>
                                            <button type="button" class="btn btn-danger" data-placement="top" title="Delete">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#id03" data-placement="top" title="Edit">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="checkbox" aria-label="Checkbox for following text input">
                                        </td>
                                        <td>03</td>
                                        <td>Thanh Phat</td>
                                        <td >Sách Kinh Tế</td>
                                        <td >100</td>
                                        <td >15/10/2018</td>
                                        <td >cod</td>
                                        <td>
                                            <button type="button" class="btn btn-danger" data-placement="top" title="Delete">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#id03" data-placement="top" title="Edit">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                  </tbody>
                                </table>
                                <nav aria-label="Page navigation example">
                                  <ul class="pagination justify-content-end">
                                    <li class="page-item">
                                      <a class="page-link" href="#" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                        <span class="sr-only">Previous</span>
                                      </a>
                                    </li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item active"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item">
                                      <a class="page-link" href="#" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                        <span class="sr-only">Next</span>
                                      </a>
                                    </li>
                                  </ul>
                                </nav>

                                <button type="button" class="btn btn-primary" data-toggle="modal"  data-placement="top" title="Add new" data-target="#id03">
                                    <i class="fa fa-plus"></i>
                                </button>
                                <button type="button" class="btn btn-danger" data-placement="top" title="Delete all">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  


    
    <div class="modal fade" id="id03" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="exampleModalCenterTitle">Product Details</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
             <form>
                <div class="modal-field border-bottom">
                    <h5>Product Infomation</h5>
                    <div class="form-group">
                        <label for="OrderID">Order ID</label>
                        <input type="text" class="form-control" id="OrderID" placeholder="Order ID">
                    </div>
                    <div class="form-group">
                        <label >Type</label>
                         <select class="form-control">
                            <option>Wood</option>
                            <option>Paper</option>
                        </select>  
                    </div>
                    <div class="form-group">
                        <label >Count</label>
                        <input type="number" class="form-control" id="count" placeholder="Count">
                    </div>
                    <div class="form-group">
                        <label for="Price">Price</label>
                        <input type="number" class="form-control" id="Price" placeholder="Price">
                    </div>
                </div>
                <div class="modal-field border-bottom">
                    <h5>Custom Infomation</h5>
                    <div class="form-group">
                        <label for="CustID">Custum ID</label>
                        <input type="text" class="form-control" id="CustID" placeholder="Custom ID">
                    </div>
                    <div class="form-group">
                        <label for="Custname">Custom Name</label>
                        <input type="text" class="form-control" id="Custname" placeholder="Custom Name">
                    </div>
                    <div class="form-group">
                        <label >Type</label>
                         <select class="form-control">
                            <option>Vip</option>
                            <option>Normal</option>
                            <option>Sách Kinh Tế</option>
                        </select>  
                    </div>
                    <div class="form-group">
                        <label for="CustAddr">Custum Address</label>
                        <input type="text" class="form-control" id="CustAddr" placeholder="Custom Address">
                    </div>
                </div>
                <div class="modal-field border-bottom">
                    <h5>Others</h5>
                    <div class="form-group">
                        <label for="Dayorder">Day Order</label>
                        <input type="text" class="form-control" id="Dayorder" placeholder="Day Order">
                    </div>
                    <div class="form-group">
                        <label for="Dayoff">Day off</label>
                        <input type="text" class="form-control" id="Dayoff" placeholder="Day Off">
                    </div>
                    <div class="form-group">
                        <label >Status</label>
                         <select class="form-control">
                            <option>Unprocessing</option>
                            <option>Processing</option>
                            <option>Complete</option>
                            <option>Cancle</option>
                        </select>  
                    </div>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>
<?php
include_once './end.php';
?>