<?php
include_once './header.php';
include_once '../modal/database.php';
include_once '../modal/product.php';
include_once '../modal/category.php';
include_once '../modal/user.php';
include_once '../modal/cart_item.php';

$database = new Database();
$db = $database->getConnection();

$cartModal = new CartItem($db);
$product = new Product($db);
$category = new Category($db);
$user = new User($db);  

?>
<div class=" pl-0 pr-0" id="detail-menu">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                    </ol>
                </nav>
                <div class="row mr-0 ml-0 mt-3 border">
                    <div class="col-md-3 bg-f pt-3 pb-3">
                        <div class="dash-field">
                            <div class="dash-header pt-1 pb-1  text-center">
                                TOTAL USER
                            </div>
                            <div class="dash-body pl-3 pr-3">
                                <i class="fa fa-user"></i>
                                <h2 class="right"><?php echo isset($_SESSION['user']) ? $user->count() : 0;   ?></h2>
                            </div>
                            <div class="dash-footer pl-3 pr-3">
                                <i><a href="userlist.html">View more....</a></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 bg-f pt-3 pb-3">
                        <div class="dash-field">
                            <div class="dash-header pt-1 pb-1  text-center">
                                TOTAL CATEGORY
                            </div>
                            <div class="dash-body pl-3 pr-3">
                                <i class="fa fa-leaf"></i>
                                <h2 class="right"><?php echo isset($_SESSION['user']) ? $category->count() : 0; ?></h2>
                            </div>
                            <div class="dash-footer pl-3 pr-3">
                                <i><a href="dashproduct.html">View more....</a></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 bg-f pt-3 pb-3">
                        <div class="dash-field">
                            <div class="dash-header pt-1 pb-1  text-center">
                                TOTAL PRODUCT   
                            </div>
                            <div class="dash-body pl-3 pr-3">
                                <i class="fa fa-leaf"></i>
                                <h2 class="right"><?php echo isset($_SESSION['user']) ? $product->count() : 0; ?></h2>
                            </div>
                            <div class="dash-footer pl-3 pr-3">
                                <i><a href="dashproduct.html">View more....</a></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 bg-f pt-3 pb-3">
                        <div class="dash-field">
                            <div class="dash-header pt-1 pb-1  text-center">
                                TOTAL CARTS
                            </div>
                            <div class="dash-body pl-3 pr-3">
                                <i class="fa fa-shopping-cart"></i>
                                <h2 class="right"><?php echo isset($_SESSION['user']) ? $cartModal->count() : 0; ?></h2>
                            </div>
                            <div class="dash-footer pl-3 pr-3">
                                <i><a href="#">View more....</a></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
    </div>
<?php
include_once './end.php';
?>