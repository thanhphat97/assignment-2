<?php
include_once './header.php';
?>
<div class=" pl-0 pr-0" id="detail-menu">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">Products</li>
                <li class="breadcrumb-item active" aria-current="page">Orders list</li>
            </ol>
        </nav>
        <div class="container-fluid">
            <div class="col-md-6 offset-md-3 bg-f pt-3 pb-3 mt-3 border">
                <div class="dash-field">
                    <div class="dash-header pt-1 pb-1  text-center">
                        TOTAL
                    </div>
                    <div class="dash-body pl-3 pr-3">
                        <i class="fa fa-product-hunt"></i>
                        <h2 class="right">150</h2>
                    </div>
                    <div class="dash-footer pl-3 pr-3">
                        <i><a href="totalorder.html">View more....</a></i>
                    </div>
                </div>
            </div>
            <div class="row mr-0 ml-0 border mt-3">
                <div class="col-lg-6 bg-f pt-3 pb-3 order-dash">
                    <div class="dash-field">
                    <div class="dash-header pt-1 pb-1  text-center">
                        TOTAL ORDERS
                    </div>
                    <div class="dash-body pl-3 pr-3">
                        <i class="fa fa-stop"></i>
                        <h2 class="right">50</h2>
                    </div>
                    <div class="dash-footer pl-3 pr-3">
                        <i><a href="totalunprocess.html">View more....</a></i>
                    </div>
                    </div>
                </div>
                <div class="col-lg-6 bg-f pt-3 pb-3  order-dash">
                    <div class="dash-field">
                        <div class="dash-header pt-1 pb-1  text-center">
                            TOTAL CARTS
                        </div>
                        <div class="dash-body pl-3 pr-3">
                            <i class="fa fa-motorcycle"></i>
                            <h2 class="right">30</h2>
                        </div>
                        <div class="dash-footer pl-3 pr-3">
                            <i><a href="totalprocess.html">View more....</a></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
include_once './end.php';
?>