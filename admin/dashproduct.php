<?php
include_once './header.php';
include_once '../modal/database.php';
include_once '../modal/product.php';

$database = new Database();
$db = $database->getConnection();

$product = new Product($db);
include_once './header.php';
?>
<div class=" pl-0 pr-0" id="detail-menu">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">Products</li>
                <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
            </ol>
        </nav>
        <div class="container-fluid">
            <div class="col-md-6 offset-md-3 bg-f pt-3 pb-3 mt-3 border">
                <div class="dash-field">
                    <div class="dash-header pt-1 pb-1  text-center">
                        TOTAL PRODUCT
                    </div>
                    <div class="dash-body pl-3 pr-3">
                        <i class="fa fa-product-hunt"></i>
                        <h2 class="right"><?php echo isset($_SESSION['user']) ? $product->count() : 0; ?></h2>
                    </div>
                    <div class="dash-footer pl-3 pr-3">
                        <i><a href="totalproduct.html">View more....</a></i>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
<?php
include_once './end.php';
?>