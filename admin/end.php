
           
            <div class=" pt-5 pb-3 text-center border-top" id="footer">
                <span> TikiBK © 2019</span>
            </div>           

        </div>
    </div>  
<div class="modal fade" id="profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalCenterTitle">User Details</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="modal-field border-bottom">
                            <h5>Basis infomation</h5>
                            <div class="form-group">
                                <label for="Name">Name</label>
                                <input type="text" class="form-control" id="Name" placeholder="Name" value="<?php echo isset($_SESSION['user']) ?  $_SESSION['user']['fullname'] : ''?>">
                            </div>
                            <div class="form-group">
                                <label for="email">Mail</label>
                                <input type="email" class="form-control" id="email" placeholder="Email" value="<?php echo isset($_SESSION['user']) ?  $_SESSION['user']['email'] : ''?>">
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="text" class="form-control" id="phone" placeholder="Phone" value="<?php echo isset($_SESSION['user']) ?   $_SESSION['user']['phone'] : null ?>">
                            </div>
                        </div>
                        <div class="modal-field border-bottom">
                            <h5>Password</h5>
                            <div class="form-group">
                                <label for="pass">New Password</label>
                                <input type="password" class="form-control" id="pass" placeholder="Password"
                                    autocomplete="new-password">
                            </div>
                            <div class="form-group">
                                <label for="confirmPass">Confirm</label>
                                <input type="password" class="form-control" id="confirmPass"
                                    placeholder="Confirm password">

                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="btn">Save changes</button>

                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
    <script src="js/style.js"></script>

</body>

</html>