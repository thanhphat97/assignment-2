<?php
include_once '../util/main.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="imgs/favicon.jpg">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
          <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
      <!-- Awesome icon -->
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-0c38nfCMzF8w8DBI+9nTWzApOpr1z0WuyswL4y6x/2ZTtmj/Ki5TedKeUcFusC/k" crossorigin="anonymous" />
    <!-- bootstrap CDN -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    
	<script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/styles/metro/notify-metro.css" />

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>


</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light pt-0 pb-0 border-bottom pr-0">
        <div class="container-fluid mycontainer pl-0 pr-0">
            <a class="navbar-toggler float-left" id="button-menu">
                <span class="navbar-toggler-icon"></span>
            </a>
            <a class="navbar-brand name-shop mr-auto float-left " href="#">TikiBK</a>

            <ul class="nav navbar-nav navbar-right justify-content-end">
                <li class="dropdown nav-item">
                    <a href="#" class="dropdown-toggle nav-link pt-2 pb-2" data-toggle="dropdown" aria-expanded="false">
                        <img src="imgs/profile.png" alt="admin" class="rounded-circle">
                        Your name
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right pos-absolute">
                        <li class="dropdown-header">
                            <?php if (!isset($_SESSION['user'])): ?>
                            <a data-toggle="modal" data-target="#profile" data-placement="top" title="Edit">
                                <i class="fa fa-user-circle-o fa-fw"></i>
                                Your profile
                            </a>
                         <?php else: ?>
                            <a data-toggle="modal" data-target="#profile" data-placement="top" title="Edit">
                                <i class="fa fa-user-circle-o fa-fw"></i>
                                <?php echo $_SESSION['user']['fullname']?>
                            </a>
                        <?php endif; ?>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link pt-2 pb-2">
                        <i class="fa fa-sign-out"></i>
                        <span class="hidden-lg">Log out</span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container-fluid pos-relative pl-0 pr-0">
        <div class="row mr-0 ml-0">
            <div class=" pl-0 pr-0 border pt-3" id="sidebar">
                <div class="pt-1 pb-1 pl-3 text-light bg-dark">
                    <span class="mb-0">NAVIGATION</span>
                </div>
                <ul id="list-sidebar" class="border-bottom pl-0">
                    <li class="border-bottom" id="dashboard">
                        <a href="dashboard.php" class="pl-3 pt-3 pb-3">
                            <i class="fa fa-dashboard fw mr-2"></i>
                            Dashboard
                        </a>
                    </li>
                    <li class="border-bottom hover-sidebar" id="users">
                        <a class="pl-3 pt-3 pb-3" href="userlist.php" aria-expanded="false">
                            <i class="fa fa-user fw mr-2"></i>
                            USERS
                        </a>
                    </li>
                    <li class="border-bottom hover-sidebar" id="product">
                        <a href="#product-feature" class=" parent action collapsed pl-3 pt-3 pb-3" aria-expanded="false"
                            data-toggle="collapse">
                            <i class="fa fa-leaf fw mr-2"></i>
                            PRODUCTS
                        </a>
                        <ul id="product-feature" class="collapse" aria-expanded="false">
                            <li><a href="./dashboard.php"> Dashboard </a></li>
                            <li><a href="./listproduct.php"> List Products </a></li>
                        </ul>
                    </li>
                    <li class="hover-sidebar" id="cart">
                        <a href="#cart-feature" class="parent action collapsed pl-3 pt-3 pb-3" aria-expanded="false"
                            data-toggle="collapse">
                            <i class="fa fa-shopping-cart fw mr-2"></i>
                            CART
                        </a>
                        <ul id="cart-feature" class="collapse" aria-expanded="false">
                            <li><a href="dashcart.php"> Dashboard </a></li>
                            <li><a href="cartslist.php"> Carts </a></li>
                            <li><a href="orderslist.php"> Orders </a></li>
                        </ul>
                    </li>
                </ul>
            </div>