<?php
include_once './header.php';
?>
<div class=" pl-0 pr-0" id="detail-menu">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">Products</li>
                <li class="breadcrumb-item active" aria-current="page">Total Products</li>
            </ol>
        </nav>
        <div class="container-fluid">
            <div class="content">
                <div class="content-header">
                    <h3>
                        <i class="fa fa-list"></i>
                        Total Products
                    </h3>
                </div>

                <div class="content-body">
                    <button type="button" class="btn btn-primary" id="btn-new" data-toggle="modal"  data-placement="top" title="Add new" data-target="#id03">
                        <i class="fa fa-plus"></i>
                    </button>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th scope="col">
                                <input type="checkbox" aria-label="Checkbox for following text input">
                            </th>  
                            <th scope="col">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Category</th>
                            <th scope="col">Seller</th>
                            <th scope="col">Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody id="products-container">
                        <tr>
                            <td>
                                <input type="checkbox" aria-label="Checkbox for following text input">
                            </td>
                            <td>01</td>
                            <td>Ten sach</td>
                            <td>Kinh te</td>
                            <td>Phat dep trai</td>
                            <td>3000</td>
                            <td>10</td>
                            <td>
                                <button type="button" class="btn btn-danger" data-placement="top" title="Delete">
                                    <i class="fa fa-trash"></i>
                                </button>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#id03" data-placement="top" title="Edit">
                                    <i class="fa fa-pencil-square-o"></i>
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <nav style="width: 100%">
                        <div id="paging"></div>
                    </nav>
                </div>
                
            </div>
        </div>
    </div>
    <div class="modal fade" id="id03" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="exampleModalCenterTitle">Product Details</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
             <form>
                <div class="modal-field border-bottom">
                    <h5>Basis infomation</h5>
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label>Seller</label>
                        <select id="select-seller" name="seller" class="form-control seller">
                            <option> </option>
                        </select> 
                    </div>
                    <div class="form-group">
                        <label >Category</label>
                         <select id="select-category" name="category" class="form-control category">
                            <option> </option>
                        </select>  
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" class="form-control" id="description" name="description" placeholder="Description">
                    </div>
                    <div class="form-group">
                        <label>Introduction</label>
                        <input type="text" class="form-control" id="introduction" name="introduction" placeholder="Introduction">
                    </div>
                    <div class="form-group">
                        <label>Price</label>
                        <input type="number" class="form-control" id="price" name="price" placeholder="Price">
                    </div>
                    <div class="form-group">
                        <label>Quantity</label>
                        <input type="number" class="form-control" id="quantity" name="quantity" placeholder="Quantity">
                    </div>
                    <div class="form-group">
                        <label>Image</label>
                        <input type="text" class="form-control" id="image" name="image" placeholder="Full name of image">
                    </div>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="btn-add" >Add</button>
            <button type="button" class="btn btn-primary" id="btn-save" style="display: none;">Save</button>
          </div>
        </div>
      </div>
    </div>
<?php
include_once './end.php';
?>
<script>
//collapse sidebar
$(document).ready(function() {
  document.title = "Products";
  loadSetting();
  showListProduct();  
});


function getAllUrlParams() {
  var url = window.location.href;
  // get query string from url (optional) or window
  var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

  // we'll store the parameters here
  var obj = {};

  // if query string exists
  if (queryString) {

      // stuff after # is not part of query string, so get rid of it
      queryString = queryString.split('#')[0];

      // split our query string into its component parts
      var arr = queryString.split('&');

      for (var i = 0; i < arr.length; i++) {
          // separate the keys and the values
          var a = arr[i].split('=');

          // set parameter name and value (use 'true' if empty)
          var paramName = a[0];
          var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];

          // (optional) keep case consistent
          paramName = paramName.toLowerCase();
          if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();

          // if the paramName ends with square brackets, e.g. colors[] or colors[2]
          if (paramName.match(/\[(\d+)?\]$/)) {

              // create key if it doesn't exist
              var key = paramName.replace(/\[(\d+)?\]/, '');
              if (!obj[key]) obj[key] = [];

              // if it's an indexed array e.g. colors[2]
              if (paramName.match(/\[\d+\]$/)) {
                  // get the index value and add the entry at the appropriate position
                  var index = /\[(\d+)\]/.exec(paramName)[1];
                  obj[key][index] = paramValue;
              } else {
                  // otherwise add the value to the end of the array
                  obj[key].push(paramValue);
              }
          } else {
              // we're dealing with a string
              if (!obj[paramName]) {
                  // if it doesn't exist, create property
                  obj[paramName] = paramValue;
              } else if (obj[paramName] && typeof obj[paramName] === 'string'){
                  // if property does exist and it's a string, convert it to an array
                  obj[paramName] = [obj[paramName]];
                  obj[paramName].push(paramValue);
              } else {
                  // otherwise add the property
                  obj[paramName].push(paramValue);
              }
          }
      }
  }

  return obj;
}
function showListProduct() {
  var html = '';
  var params = getAllUrlParams()
  if (!('page' in params)){
      curr_page = 1;
  }
  else{
      curr_page = params.page;
  }
  // get list of products from the API
  $.getJSON("http://localhost/assignment-2/controller/api/products/admin_read_paging.php?page=" + curr_page, function (data) {
      $.each(data.records, function (key, val) {
          html += `<tr>
                      <td>
                          <input type="checkbox" aria-label="Checkbox for following text input">
                      </td>
                      <td>${key}</td>
                      <td>${val.name}</td>
                      <td>${val.category_name}</td>
                      <td>${val.seller_name}</td>
                      <td>${val.price}</td>
                      <td>${val.quantity}</td>
                      <td>
                          <button type="button" class="btn btn-danger btn-delete" data-id="${val.id}" data-provideid="${val.provide_id}" data-placement="top" title="Delete">
                              <i class="fa fa-trash"></i>
                          </button>
                          <button type="button" class="btn btn-primary btn-edit" data-id="${val.id}" data-provideid="${val.provide_id}" data-toggle="modal" data-target="#id03" data-placement="top" title="Edit">
                              <i class="fa fa-pencil-square-o"></i>
                          </button>
                      </td>
                  </tr>`
      });
      $("#products-container").html(html);
  });
}

// load categories list
function loadSetting(){
  html1 = '';
  html2 = '';
  $.getJSON("http://localhost/assignment-2/controller/api/categories/readAll.php", function (data) {
      $.each(data.records, function (key, val) {
          html1 += `<option value="${val.id}">${val.name}</option>`
      });
      $("#select-category").html(html1);
  });

  $.getJSON("http://localhost/assignment-2/controller/api/authors/readAll.php", function (data) {
      $.each(data.records, function (key, val) {
          html2 += `<option value="${val.id}">${val.storeName}</option>`
      });
      $("#select-seller").html(html2);
  });
}


function checkInput(name, seller, category, description, introduction, price, quantity, image){
  return name != '' && seller != '' && category != '' && price != '' && quantity != '';
}

$(document).on('click','#btn-add',function(){

  name = $("#name").val().trim();
  seller = $("#select-seller").val().trim();
  category = $("#select-category").val();
  description = $("#description").val().trim();
  introduction = $("#introduction").val().trim();
  price = $("#price").val().trim();
  quantity = $("#quantity").val().trim();
  image = $("#image").val().trim();
  if (checkInput(name, seller, category, description, introduction, price, quantity, image)){
      var obj = {};
          obj.name = name;
          obj.seller = seller;
          obj.category_id = category;
          obj.description = description;
          obj.introduction = introduction;
          obj.price = price;
          obj.quantity = quantity;
          obj.image_name = image;
          var myJson = JSON.stringify(obj)

          // call ajax
          $.ajax({
              url: 'http://localhost/assignment-2/controller/api/products/create.php',
              type: 'POST',
              dataType: 'json',
              data: myJson,
          }).done(function(data){
            $.notify(data.RETURN_TEXT, "info");
            setTimeout(function(){ location.reload() }, 1500);
             })
          $('#id03').hide();
  } else {
    $.notify('Miss data', "error");
  }

});

$(document).on('click', '#btn-new', function (){
    $('#btn-add').show();
    $('form').get(0).reset();
});

$(document).on('click','.btn-edit',function(){
  $('#btn-add').hide();
  $('#btn-save').show();
  // get and fill value to modal
  provide_id= $(this).data('provideid');
  id = $(this).data("id");
  $('#btn-save').data('id', id); 

  $.ajax({
      url: 'http://localhost/assignment-2/controller/api/products/read_oneadmin.php?id=' + id + '&provide=' + provide_id,
      type: 'GET',
      success: function(data){
          let item = data.record[0];
          $("#name").val(item.name);
          $("#select-category").val(item.category_id);
          $("#description").val(item.description);
          $("#select-seller").val(item.seller_id);
          $("#introduction").val(item.introduction);
          $("#price").val(item.seller_price);
          $("#quantity").val(item.seller_quantity);
          $("#image").val(item.image_name);
      },
      error: function(error){
          alert(error.message);
      }
  });

});

$(document).on('click','#btn-save', function(){
  
  id = $('#btn-save').data('id'); 
  name = $("#name").val().trim();
  seller = $("#select-seller").val().trim();
  category = $("#select-category").val().trim();
  description = $("#description").val().trim();
  introduction = $("#introduction").val().trim();
  price = $("#price").val().trim();
  quantity = $("#quantity").val().trim();
  image = $("#image").val().trim();
 if (checkInput(name, seller, category, description, introduction, price, quantity, image)){
      var obj = {};
          obj.id = id;
          obj.name = name;
          obj.seller = Number(seller);
          obj.category_id = Number(category);
          obj.description = description;
          obj.introduction = introduction;
          obj.price = Number(price);
          obj.quantity = Number(quantity);
          obj.image_name = image;
          var myJson = JSON.stringify(obj)
          console.log(myJson)

          $.ajax({
              url: 'http://localhost/assignment-2/controller/api/products/update.php',
              type: 'POST',
              dataType: 'json',
              data: myJson,
            }).done(function(data){
            $.notify(data.RETURN_TEXT, "info");
            setTimeout(function(){ location.reload() }, 1500);
             })
          $('#id03').hide();
  } else {
    $.notify('Miss data', "error");
  }
});

$(document).on('click','.btn-delete', function(){
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
    if (result.value) {
        id = $(this).data("id");
        var obj = {};
        obj.id = id;
        var myJson = JSON.stringify(obj)
        $.ajax({
            url: 'http://localhost/assignment-2/controller/api/products/delete.php',
            type: 'POST',
            dataType: 'json',
            data: myJson,
        }).done(function(data) {
            Swal.fire(
            'Deleted!',
            data.RETURN_TEXT,
            'info'
           ).then(()=> location.reload() )
        })
       
    }
    })
});


</script>



