<?php
include_once './header.php';
?>
<div class=" pl-0 pr-0" id="detail-menu">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Users</li>
            <li class="breadcrumb-item active" aria-current="page">List Users</li>
        </ol>
    </nav>
    <div class="container-fluid">
        <div class="content">
            <div class="content-header">
                <h3>
                    <i class="fa fa-list"></i>
                    Users List
                </h3>
            </div>
            <div class="content-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">
                            <input type="checkbox" aria-label="Checkbox for following text input">
                        </th>  
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">E-mail</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Birthdate</th>
                        <th scope="col">Role</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody id="users-container">
                    <tr>
                        <td>
                            <input type="checkbox" aria-label="Checkbox for following text input">
                        </td>
                        <td>01</td>
                        <td>phat</td>
                        <td>anc@abc</td>
                        <td >0984747472</td>
                        <td >14/12/2019</td>
                        <td>User</td>
                        <td>
                            <button type="button" class="btn btn-danger" data-placement="top" title="Delete">
                                <i class="fa fa-trash"></i>
                            </button>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#id02" data-placement="top" title="Edit">
                                <i class="fa fa-pencil-square-o"></i>
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>
    </div>
    <div class="modal fade" id="id02" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="exampleModalCenterTitle">User Details</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
             <form>
                <div class="modal-field border-bottom">
                    <h5>Basis infomation</h5>
                    <div class="form-group">
                        <label for="Name">Name</label>
                        <input type="input" class="form-control" id="name" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label for="email">Mail</label>
                        <input type="email" class="form-control" id="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="number" class="form-control" id="phone" placeholder="Phone">
                    </div>
                    <div class="form-group">
                        <label >Role</label>
                         <select id="role" name="role" class="form-control">
                            <option value="0">Admin</option>
                            <option value="1">User</option>
                        </select>  
                    </div>
                </div>
                <div class="modal-field">
                    <h5>Password</h5>
                    <div class="form-group">
                        <label for="pass">Old password</label>
                        <input type="password" class="form-control" id="old_pass" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label for="pass">Password</label>
                        <input type="password" class="form-control" id="pass" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label for="confirmPass">Confirm</label>
                        <input type="password" class="form-control" id="confirmPass" placeholder="Confirm password">
                    </div>      
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" id="btn-save" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>
<?php
include_once './end.php';
?>
<script>
$(document).ready(function () {
    document.title = "Users";
    // loadSetting();
    showListUser();
});


function showListUser() {
    var html = '';
    // get list of products from the API
    $.getJSON("http://localhost/assignment-2/controller/api/user/readAll.php", function (data) {
        $.each(data.records, function (key, val) {
            html += `   <tr>
                        <td>
                            <input type="checkbox" aria-label="Checkbox for following text input">
                        </td>
                        <td>${val.id}</td>
                        <td>${val.fullname}</td>
                        <td>${val.email}</td>
                        <td >${val.phone}</td>
                        <td >${val.birthdate}</td>
                        <td>${val.role}</td>
                        <td>
                            <button type="button" class="btn btn-danger btn-delete" data-id="${val.id}"data-placement="top" title="Delete">
                                <i class="fa fa-trash"></i>
                            </button>
                            <button type="button" class="btn btn-primary btn-edit" data-id="${val.id}" data-toggle="modal" data-target="#id02" data-placement="top" title="Edit">
                                <i class="fa fa-pencil-square-o"></i>
                            </button>
                        </td>
                    </tr>`
        });
        $("#users-container").html(html);
    });
}

$(document).on('click','.btn-edit',function(){
    $('#btn-add').hide();
    $('#btn-save').show();
    // get and fill value to modal
    id = $(this).data("id");
    $.ajax({
        url: 'http://localhost/assignment-2/controller/api/user/read_one.php?id=' + id,
        type: 'GET',
        success: function(data){
            $("#name").val(data.fullname);
            $("#email").val(data.email);
            $("#phone").val(data.phone);
            $("#birthdate").val(data.birthdate);
            data.role == "1" ?    $('#role option[value=1]').attr('selected','selected') :   $('#role option[value=0]').attr('selected','selected');
        },
    });

});

$(document).on('click','.btn-delete', function(){
    if (confirm("Do you want to delete?")){
        id = $(this).data("id");
        var obj = {};
        obj.id = id;
        var myJson = JSON.stringify(obj)
        $.ajax({
            url: 'http://localhost/assignment-2/controller/api/user/delete.php',
            type: 'POST',
            dataType: 'json',
            data: myJson,
            success: function(data){
                alert(data.message);
            },
            error: function(error){
                alert(error.message);
            }
        });
        location.reload();
    }
    
});

</script>
