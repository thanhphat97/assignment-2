CREATE OR ALTER  PROCEDURE [dbo].[insert_donHang](
	@trangThaiDonHang AS nvarchar(30) = N'Chưa xác nhận',
	@diaChiGiaoHang AS nvarchar(50),
	@ngayDatHang AS date =NULL,
	@hinhThucThanhToan AS nvarchar(30) = NULL,
	@ngayThanhToan AS date = NULL,
	@ngayGiaoHang AS date = NULL,
	@maKhachHang_DH AS varchar(30),
	@maGoiKhuyenMai_DH AS CHAR(9) = NULL,
	@tongTienChuaTruKM AS DECIMAL(18,0) = 0,
	@soTienKM AS DECIMAL(18,0) = 0,
	@thanhTienDaTruKM AS DECIMAL(18,0) = 0,
	@result SMALLINT=1 OUTPUT)
AS
BEGIN
IF @ngayDatHang IS NOT NULL AND @ngayDatHang > GETDATE()
    BEGIN
    RAISERROR ('Ngay dat hang khong hop le',1,1)
	SET @result = 0
    END
IF @ngayDatHang IS NULL
	SET @ngayDatHang = GETDATE()
IF NOT EXISTS (SELECT * FROM dbo.khachHang WHERE maKhachHang = @maKhachHang_DH) 
    BEGIN
    RAISERROR('MA KHACH HANG KHONG TON TAI',1,1)
	SET @result = 0
    END
IF @maGoiKhuyenMai_DH IS NOT NULL AND NOT EXISTS (SELECT * FROM dbo.goiKhuyenMai WHERE maGoiKM = @maGoiKhuyenMai_DH)
    BEGIN
    RAISERROR ('MA GOI KHUYEN MAI KHONG TON TAI',1,1)
	SET @result = 0
    END
IF @ngayThanhToan < @ngayDatHang OR @ngayThanhToan > GETDATE()
    BEGIN
    RAISERROR ('Ngay thanh toan khong hop le',1,1)
	SET @result = 0
    END
IF @ngayGiaoHang < @ngayDatHang OR @ngayGiaoHang > GETDATE()
    BEGIN 
    RAISERROR ('Ngay giao hang khong hop le',1,1)
	SET @result = 0
    END
IF @result = 1
	BEGIN
    SET NOCOUNT ON
    INSERT 
        INTO [dbo].[donHang]
            ([trangThaiDonHang], [diaChiGiaoHang], [ngayDatHang], [hinhThucThanhToan], [ngayThanhToan], [ngayGiaoHang], 
			[maKhachHang_DH], [maGoiKhuyenMai_DH], [tongTienChuaTruKM], [soTienKM], [thanhTienDaTruKM])
        VALUES
            (@trangThaiDonHang, @diaChiGiaoHang, @ngayDatHang, @hinhThucThanhToan, @ngayThanhToan, @ngayGiaoHang, 
			@maKhachHang_DH, @maGoiKhuyenMai_DH, @tongTienChuaTruKM, @soTienKM, @thanhTienDaTruKM)
	PRINT('INSERT THANH CONG')
    RETURN @@ROWCOUNT
	END
ELSE
	BEGIN
	PRINT('INSERT KHONG THANH CONG')
	END
END
GO

-- EXECUTE dbo.insert_donHang
-- 	@trangThaiDonHang =	N'Đã xác nhận',
-- 	@diaChiGiaoHang	= N'102 Điện Biên Phủ Bình Thạnh',
-- 	@ngayDatHang	= null,
-- 	@hinhThucThanhToan	=	N'Tiền mặt',
-- 	@ngayThanhToan		=	N'2019-11-30 00:00:00.000',
-- 	@ngayGiaoHang	=	NULL,
-- 	@maKhachHang_DH	=	'dungtran82',
-- 	@maGoiKhuyenMai_DH = 'KM01'     , 
-- 	@tongTienChuaTruKM = NULL, 
-- 	@soTienKM= NULL, 
-- 	@thanhTienDaTruKM = NULL;
-- GO


------------------------------------TRIGGER--------------------------------------
----------------------------Update tính tổng tiền của đơn hàng-------------------
CREATE OR ALTER  TRIGGER [dbo].[update_thanhTien] ON [dbo].[donHang]
AFTER UPDATE AS
IF (SELECT inserted.tongTienChuaTruKM FROM inserted) <> (SELECT deleted.tongTienChuaTruKM FROM deleted) OR
	(SELECT inserted.soTienKM FROM inserted) <> (SELECT deleted.soTienKM FROM deleted)
BEGIN 
    UPDATE donHang
    SET thanhTienDaTruKM = (SELECT tongTienChuaTruKM FROM inserted WHERE maDonHang = donHang.maDonHang) - donHang.soTienKM
    FROM donHang JOIN inserted on donHang.maDonHang = inserted.maDonHang
END
GO


CREATE OR ALTER  TRIGGER [dbo].[insert_thanhTien] ON [dbo].[donHang]
AFTER INSERT AS
BEGIN
IF (SELECT inserted.tongTienChuaTruKM FROM inserted) > 0 OR (SELECT inserted.soTienKM FROM inserted) >0
    BEGIN
        UPDATE donHang
        SET thanhTienDaTruKM = (SELECT tongTienChuaTruKM FROM inserted WHERE maDonHang = donHang.maDonHang) - donHang.soTienKM
        FROM donHang JOIN inserted on donHang.maDonHang = inserted.maDonHang
    END
END
GO

----------------Update tổng tiền đã thanh toán của khách hàng khi đơn hàng ở trạng thái đã giao---------------
CREATE OR ALTER  TRIGGER [dbo].[IU_tinh_tongTienDTT] ON [dbo].[donHang]
AFTER INSERT, UPDATE AS
	IF (SELECT inserted.trangThaiDonHang FROM inserted) = N'Đã giao' 
	BEGIN
		UPDATE khachHang
		SET tongTienDTT = tongTienDTT + (SELECT donHang.thanhTienDaTruKM FROM donHang WHERE donHang.maDonHang = inserted.maDonHang)
		FROM khachHang JOIN inserted on khachHang.maKhachHang = inserted.maKhachHang_DH
	END

GO

CREATE OR ALTER  TRIGGER [dbo].[D_tinh_tongTienDTT] ON [dbo].[donHang]
AFTER DELETE AS
	IF (SELECT deleted.trangThaiDonHang FROM deleted) = N'Đã giao' 
	BEGIN
		UPDATE khachHang
		SET tongTienDTT = tongTienDTT - deleted.thanhTienDaTruKM
		FROM khachHang JOIN deleted on khachHang.maKhachHang = deleted.maKhachHang_DH
	END

GO

-- UPDATE donHang
-- SET trangThaiDonHang = N'Đã giao'
-- WHERE maDonHang = 203

----------------------TRIGGER BẢNG itemGioHang --------------------------------------
CREATE OR ALTER  TRIGGER [dbo].[I_tinh_tongTienChuaTruKM] ON [dbo].[itemGioHang]
AFTER INSERT AS
BEGIN
    UPDATE donHang
    SET tongTienChuaTruKM = tongTienChuaTruKM + inserted.soLuong*(SELECT donGiaHienTai FROM sanPham JOIN inserted ON maSanPham = inserted.maSanPham_IGH)
    FROM donHang JOIN inserted ON donHang.maDonHang = inserted.maDonHang_IGH
END
GO

CREATE OR ALTER  TRIGGER [dbo].[U_tinh_tongTienChuaTruKM] ON [dbo].[itemGioHang]
AFTER UPDATE AS
BEGIN
	IF (SELECT deleted.donGia FROM deleted) IS NOT NULL
	BEGIN
		UPDATE donHang
		SET tongTienChuaTruKM = tongTienChuaTruKM + inserted.soLuong*(SELECT donGiaHienTai FROM sanPham JOIN inserted ON maSanPham = inserted.maSanPham_IGH)
												- deleted.soLuong*deleted.donGia
		FROM donHang JOIN inserted ON donHang.maDonHang = inserted.maDonHang_IGH JOIN deleted on donHang.maDonHang = deleted.maDonHang_IGH
	END
END

GO

CREATE OR ALTER TRIGGER D_tinh_tongTienChuaTruKM ON dbo.itemGioHang
AFTER DELETE AS
BEGIN
    UPDATE donHang
    SET tongTienChuaTruKM = tongTienChuaTruKM - deleted.soLuong*deleted.donGia
    FROM donHang JOIN deleted ON donHang.maDonHang = deleted.maDonHang_IGH
END
GO


CREATE OR ALTER  TRIGGER [dbo].[IU_donGiaItem] ON [dbo].[itemGioHang]
AFTER INSERT, UPDATE AS
BEGIN
    UPDATE itemGioHang
    SET donGia = (SELECT donGiaHienTai FROM sanPham WHERE maSanPham = inserted.maSanPham_IGH)
    FROM itemGioHang JOIN inserted ON itemGioHang.maGioHang = inserted.maGioHang
END
GO
-- INSERT INTO itemGioHang VALUES (2,203,'dungquanq71','P0517',NULL)

-- UPDATE itemGioHang
-- SET soLuong = 3
-- WHERE maGioHang = 174


-------------------------------STORED PROCEDURE----------------------------
------------Liệt kê top 10 khách hàng có nhiều đơn hàng nhất trong năm truyền vào---------
CREATE PROCEDURE pr_top10nhieuDonHangTrongNam(@nam DECIMAL)
AS
BEGIN
    SELECT TOP 10 khachHang.ho+' '+ khachHang.tenLot+' '+ khachHang.ten AS 'Ten Khach Hang',COUNT(*) AS 'So don hang'
    FROM donHang JOIN khachHang on donHang.maKhachHang_DH = khachHang.maKhachHang
    WHERE YEAR(donHang.ngayThanhToan) = @nam
    GROUP BY maKhachHang_DH,ten,ho,tenLot
    HAVING COUNT(*)>0
    ORDER BY COUNT(*) DESC
END
GO

-- EXEC dbo.pr_top10nhieuDonHangTrongNam @nam = 2019

----------Tra cứu trạng thái đơn hàng và tình trạng vận chuyển của những đơn hàng có giá lớn hơn minPrice----------
CREATE PROCEDURE pr_traCuuDonHang(@maKhachHang VARCHAR(30))
AS
BEGIN
    SELECT khachHang.ho+' '+ khachHang.tenLot+' '+ khachHang.ten AS 'Ten Khach Hang',sdt,maDonHang,trangThaiDonHang,maKienHang,maVanChuyen_KH,thanhTienDaTruKM
    FROM donHang JOIN khachHang on maKhachHang_DH = maKhachHang JOIN kienHang on maDonHang = maDonHang_KH
    WHERE @maKhachHang = donHang.maKhachHang_DH
    ORDER BY thanhTienDaTruKM
END
GO

-- EXEC pr_traCuuDonHang 'oanhnguyen.dn'

-------------------------------FUNCTION-------------------------------
--------Function tính tổng số tiền của các đơn hàng trong tháng--------
CREATE OR ALTER FUNCTION fc_tongSoDonHangDaGiaoTrongThang(@month int,@year int)
RETURNS NVARCHAR(200)   
BEGIN
	IF @year < 1992 OR @year >YEAR(GETDATE())
		BEGIN
		RETURN N'Năm nhập vào không hợp lệ'
		END
	IF @month > 12 OR @month <1 OR (@month > MONTH(GETDATE()) AND @year = YEAR(GETDATE()))
		BEGIN 
		RETURN N'Tháng nhập vào không hợp lệ'
		END

	BEGIN
		DECLARE @result INT
		SELECT @result = COUNT(*)
		FROM donHang
		WHERE @month = MONTH(donHang.ngayGiaoHang) AND @year = YEAR(donHang.ngayGiaoHang) AND donHang.trangThaiDonHang = N'Đã giao'
		RETURN CAST(@result as NVARCHAR)
	END
END
GO

----------Function tính tổng đơn hàng của một khách nhập vào mã khách hàng-----------
CREATE FUNCTION tong_DonHang(@maKhachHang VARCHAR(30))
RETURNS INT
BEGIN
    DECLARE @count_donHang INT
    SET @count_donHang =(SELECT COUNT(maDonHang)
    FROM donHang
    WHERE maKhachHang_DH = @maKhachHang
    GROUP BY maKhachHang_DH)
    IF NOT EXISTS (SELECT maDonHang FROM donHang WHERE maKhachHang_DH = @maKhachHang)
        BEGIN
        SET @count_donHang = 0
        END
    RETURN @count_donHang
END
GO
---------------------------------------------------------
SELECT dbo.tong_DonHang('duc.ute') AS tongDonHang
GO
SELECT dbo.fc_tongSoDonHangDaGiaoTrongThang(8, 2020) AS tongSoDonHangDaGiaoTrongThang
GO
----------------------------------------------------------