-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2020 at 01:06 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tikidatabase`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cart_item`
--

CREATE TABLE `cart_item` (
  `id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `item_price` decimal(10,3) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `parentID` int(11) NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `parentID`, `created`, `modified`) VALUES
(1, 'Điện thoại - Máy tính bảng', 0, '2020-07-08 22:55:39', '2020-07-08 22:55:39'),
(2, 'Điện tử - Điện lạnh', 0, '2020-07-08 23:01:12', '2020-07-08 23:01:12'),
(3, 'Phụ kiện - Thiết bị số', 0, '2020-07-08 23:01:12', '2020-07-08 23:01:12'),
(4, 'Laptop - Thiết bị IT', 0, '2020-07-08 23:01:12', '2020-07-08 23:01:12'),
(5, 'Máy ảnh - Quay phim', 0, '2020-07-08 23:01:12', '2020-07-08 23:01:12'),
(6, 'Điện gia dụng', 0, '2020-07-08 23:01:12', '2020-07-08 23:01:12'),
(7, 'Nhà cửa đời sống', 0, '2020-07-08 23:01:12', '2020-07-08 23:01:12'),
(8, 'Hàng tiêu dùng - Thực phẩm', 0, '2020-07-08 23:01:12', '2020-07-08 23:01:12'),
(9, 'Đồ chơi, mẹ và bé ', 0, '2020-07-08 23:01:12', '2020-07-08 23:01:12'),
(10, 'Làm đẹp - Sức khỏe', 0, '2020-07-08 23:01:12', '2020-07-08 23:01:12'),
(11, 'Thời trang - Phụ kiện', 0, '2020-07-08 23:01:12', '2020-07-08 23:01:12'),
(12, 'Apple', 1, '2020-07-08 23:04:02', '2020-07-08 23:04:02'),
(13, 'Samsung', 1, '2020-07-08 23:04:02', '2020-07-08 23:04:02'),
(14, 'Âm thanh - Phụ kiện', 2, '2020-07-08 23:05:53', '2020-07-08 23:06:38'),
(15, 'Máy lạnh - Điều hòa', 2, '2020-07-08 23:05:53', '2020-07-08 23:05:53'),
(16, 'Thể thao - Dã ngoại', 0, '2020-07-08 23:12:48', '2020-07-08 23:12:48'),
(17, 'Xe máy - Ô tô', 0, '2020-07-08 23:12:48', '2020-07-08 23:12:48');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment` varchar(512) NOT NULL,
  `shipping_address` varchar(512) NOT NULL,
  `total` decimal(10,3) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `item_price` decimal(10,3) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `category_id` int(11) NOT NULL,
  `description` text DEFAULT NULL,
  `introduction` longtext DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `status` varchar(200) DEFAULT NULL,
  `image_name` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `category_id`, `description`, `introduction`, `quantity`, `status`, `image_name`, `created`, `modified`) VALUES
(1, 'Điện Thoại iPhone SE 64GB ( 2020) - Hàng Chính Hãng', 12, NULL, 'Mang tình yêu trở lại với thiết kế sang trọng\r\nĐiện Thoại iPhone SE 64GB ( 2020) có một thiết kế quen thuộc, thiết kế đã dành được rất nhiều tình cảm của người dùng trong những năm qua. Kiểu dáng nhỏ gọn, bo tròn các cạnh, thân máy siêu mỏng tinh tế và hoàn thiện từ chất liệu nhôm kết hợp kính cao cấp, iPhone SE 2020 mang đến cảm hứng bất tận cho người dùng. Chiếc iPhone nhỏ gọn rất dễ dàng cầm nắm, thao tác hay cho vào túi mang đi bất cứ đâu nhưng vẫn toát lên vẻ sang trọng đậm chất Apple, iPhone SE 2020 cho bạn cảm giác thân thuộc, thoải mái hơn bao giờ hết', 0, '1', 'iphone8.jpg', '2020-07-09 06:33:46', '2020-07-09 09:39:44'),
(2, 'iPad 10.2 Inch WiFi 32GB New 2019 - Hàng Nhập Khẩu Chính Hãng', 12, NULL, NULL, 0, '1', 'ipadgen7.jpg', '2020-07-09 07:54:45', '2020-07-09 09:39:34'),
(3, 'Đồng Hồ Thông Minh Apple Watch Series 5', 12, NULL, NULL, 0, '1', 'applewatch.jpg', '2020-07-09 09:00:12', '2020-07-09 09:40:16'),
(4, 'Tai Nghe Bluetooth Apple AirPods Pro True Wireless', 12, NULL, NULL, 0, '1', 'airpod.jpg', '2020-07-09 09:05:21', '2020-07-09 09:40:23'),
(5, 'Apple Macbook Pro 2020 - 13 Inchs', 12, NULL, NULL, 0, NULL, 'macbook.jpg', '2020-07-09 09:11:29', '2020-07-09 09:11:29'),
(6, 'Điện Thoại Samsung Galaxy S20 Ultra', 13, NULL, NULL, 0, NULL, 'samsungdt.jpg', '2020-07-09 09:15:53', '2020-07-09 09:15:53'),
(7, 'Đồng Hồ Samsung Galaxy Watch 46mm', 13, NULL, NULL, 0, NULL, 'samsungwatch.jpg', '2020-07-09 09:19:08', '2020-07-09 09:19:08'),
(8, 'Tai Nghe True Wireless Samsung Galaxy Buds+ ', 13, NULL, NULL, 0, NULL, 'samsungbud.jpg', '2020-07-09 09:22:19', '2020-07-09 09:22:19'),
(9, 'Máy tính bảng Samsung Galaxy Tab S6', 13, NULL, NULL, 0, NULL, 'samsungtab.jpg', '2020-07-09 09:24:38', '2020-07-09 09:24:38'),
(10, 'Smart Tivi Samsung 4K 55 inch', 13, NULL, NULL, 0, NULL, 'samsungtv.jpg', '2020-07-09 09:27:12', '2020-07-09 09:27:12');

-- --------------------------------------------------------

--
-- Table structure for table `provide`
--

CREATE TABLE `provide` (
  `id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` double DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `provide`
--

INSERT INTO `provide` (`id`, `seller_id`, `product_id`, `price`, `quantity`) VALUES
(1, 1, 1, 9000000, 20),
(2, 2, 1, 8900000, 15),
(3, 3, 1, 10000000, 10),
(4, 3, 2, 12000000, 1),
(5, 1, 2, 20000000, 5),
(6, 1, 3, 8000000, 10),
(7, 2, 3, 8120000, 4),
(8, 3, 3, 8500000, 12),
(9, 1, 4, 5800000, 31),
(10, 2, 4, 6000000, 23),
(11, 3, 4, 5950000, 6),
(12, 1, 5, 33000000, 8),
(13, 2, 5, 35000000, 10),
(14, 3, 5, 32500000, 15),
(15, 1, 6, 21500000, 3),
(16, 2, 6, 20000000, 2),
(17, 3, 2, 22000000, 6),
(18, 1, 7, 7500000, 12),
(19, 2, 7, 8000000, 2),
(20, 3, 7, 7200000, 5),
(21, 1, 8, 2100000, 9),
(22, 2, 8, 2150000, 10),
(23, 3, 8, 2300000, 20),
(24, 1, 9, 16000000, 4),
(25, 2, 9, 16500000, 7),
(26, 3, 9, 17000000, NULL),
(27, 1, 10, 10500000, NULL),
(28, 2, 10, 10000000, NULL),
(29, 3, 10, 11000000, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `seller`
--

CREATE TABLE `seller` (
  `id` int(11) NOT NULL,
  `storeName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seller`
--

INSERT INTO `seller` (`id`, `storeName`) VALUES
(1, 'Tiki Trading'),
(2, 'NewTechShop'),
(3, 'FPT Shop');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(60) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `password` varchar(60) NOT NULL,
  `fullname` varchar(60) NOT NULL,
  `birthdate` date DEFAULT NULL,
  `role` tinyint(1) NOT NULL DEFAULT 1,
  `address` varchar(512) DEFAULT NULL,
  `gender` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `phone`, `password`, `fullname`, `birthdate`, `role`, `address`, `gender`, `created`, `modified`) VALUES
(1, 'thanhphattran@gmail.com', '0397400056', 'd1708279c5e2306830202ada5b66a3964d95529f', 'Trần Thành Phát', '0000-00-00', 1, NULL, 1, '2020-07-08 18:43:31', '2020-07-08 18:43:31'),
(2, 'phatkoyz@gmail.com', '0397400057', 'caa3596721856027546aff983438d399a4593734', 'Trần Thành Phát', '2020-08-07', 1, NULL, 0, '2020-07-08 18:57:12', '2020-07-08 18:57:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `cart_item`
--
ALTER TABLE `cart_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `seller_id` (`seller_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `provide`
--
ALTER TABLE `provide`
  ADD PRIMARY KEY (`id`),
  ADD KEY `seller_id` (`seller_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `seller`
--
ALTER TABLE `seller`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cart_item`
--
ALTER TABLE `cart_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_item`
--
ALTER TABLE `order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `provide`
--
ALTER TABLE `provide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `seller`
--
ALTER TABLE `seller`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cart_item`
--
ALTER TABLE `cart_item`
  ADD CONSTRAINT `cart_item_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cart_item_ibfk_2` FOREIGN KEY (`seller_id`) REFERENCES `seller` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_item`
--
ALTER TABLE `order_item`
  ADD CONSTRAINT `order_item_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_item_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `provide`
--
ALTER TABLE `provide`
  ADD CONSTRAINT `provide_ibfk_1` FOREIGN KEY (`seller_id`) REFERENCES `seller` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `provide_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
