<?php
include 'header.php';
?>
<main>
    <div class="page-breadcrumb">
        <div class="container">
            <ul id="breadcrumb">
                <li><a href="./index.php">Trang chủ</a></li>
                <li><a class="active">Tủ sách</a></li>
            </ul>
        </div>
    </div>
    <section class="authorsection">
        <div class="container">
            <div class="home-moblie-title">TỦ SÁCH</div>
            <div class="author-slide row">
                
            </div>
        </div>
    </section>

<script src="./controller/author/showAuthors.js"></script>

<script src="./assets/authors/"></script>

<?php
include 'footer.php';
?>
