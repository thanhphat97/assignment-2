<?php
require_once('./util/main.php');
require_once('./modal/database.php');
require_once('./modal/cart_item.php');
require_once('./modal/product.php');


$database = new Database();
$db = $database->getConnection();

$cartModal = new CartItem($db);
$product = new Product($db);

if (isset($_SESSION['user'])) {
    $cartList=array();
    $cartModal->user_id = $_SESSION['user']['id'];
    $stmt = $cartModal->readJoin();
    $num = $stmt->rowCount();
//   p.name, p.price, p.discount, c.product_id, c.quantity 
    if($num > 0 ) {
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            extract($row);
            $cart_item = array(                             
                "name" =>  $name,
                "price" => $price,
                "discount" => $discount,
                "product_id" => $id, 
                "quantity" => $quantity,
                "image_name" => $image_name, 
            );
            array_push($cartList, $cart_item);
        }
        $_SESSION['checkout'] = array();
        $_SESSION['checkout']['products'] = $cartList;
        $number1 = count($_SESSION['checkout']['products']);
    }
    else $number1 = 0; 
} 
else {
    $cartList=array();
        if (isset($_SESSION['cart'])) {
            foreach($_SESSION['cart'] as $key => $value) {
                if($key != 'total_quantity') {
                    $product->id = $key;
                    $stmt = $product->readOne();
                    $num = $stmt->rowCount();
                    if($num > 0 ) {
                        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                            extract($row);
                            $cart_item = array(
                                "name" => $name,
                                "product_id" => $id,
                                "price" => $price,
                                "discount" => $discount,
                                "quantity" => $value,
                                "image_name" => $image_name,
                            );
                            array_push($cartList, $cart_item);
                        }
                    }
                }
            }
            $number2 = count($cartList);
        }
        else $number2 = 0;
}

include 'header.php';
?>
<main>
    <div class="page-breadcrumb">
        <div class="container">
            <ul id="breadcrumb">
                <li><a href="#">Trang chủ</a></li>
                <li><a class="active" href="#">Giỏ hàng</a></li>
            </ul>
        </div>
    </div>
    <section class="cart-section">
        <div class="container">
            <div class="home-moblie-title">GIỎ HÀNG</div>
            <div class="cart-slide row no-gutters">
                <?php if((isset($_SESSION['user']) && $number1 > 0) || (!isset($_SESSION['user']) && $number2 > 0)): ?>
                    <div class="col-12 col-lg-8">
                        <div class="cart-table">
                            <?php foreach ( $cartList as $item): ?>
                                <div class="item-row" >
                                    <div class="cart-item" >
                                        <div class="img-cart">
                                            <img src="./assets/images/<?php echo $item['image_name']; ?>"
                                                alt="<?php echo $item['image_name']; ?>">
                                        </div>
                                        <div class="information">
                                            <div class="item-info">
                                                <a class="name-book" href="http://localhost/assignment-2/product-details.php?product_id=<?php echo $item['product_id']; ?>"><?php echo $item['name']; ?></a>
                                                <div class="amount" data-id = "<?php echo $item['product_id']; ?>">
                                                    <div class="input-number">
                                                        <span class="minus">-</span>
                                                        <input type="text" maxlength="10" value="<?php echo $item['quantity']; ?>" />
                                                        <span class="plus">+</span>
                                                    </div>
                                                    <a class="btn-remove mr-3">
                                                    <i class="fas fa-trash-alt"> </i>
                                                </a>
                                                </div>
                                               
                                            </div>
                                            <div class="item-price">
                                                <div class="product-price" id="per">
                                                    <div class="label">Giá: </div>
                                                    <div class="per-price">
                                                    <?php echo number_format($item['price'] * (1 - $item['discount'] / 100) , 0, ',', '.') ; ?> ₫</div>
                                                </div>
                                                <div class="product-price" id="multi">
                                                    <div class="label">Tổng: </div>
                                                    <div class="type-price">
                                                     <?php echo number_format($item['price'] * (1 - $item['discount'] / 100)*$item['quantity'] , 0, ',', '.'); ?> ₫</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>    
                        </div>
                    <div class="total-pay row mb-3">
                        <div class="col-12 col-md-6 col-lg-7 pt-3">
                            <a class="btn-backhome" href="./index.php">
                                Mua thêm
                            </a>
                        </div>
                        <div class="col-12 col-md-6 col-lg-5 pt-3">
                            <div class="total">
                                <div class="totals-value" id="cart-subtotal">
                                    <div class="label">Tạm tính: </div>
                                    <div class="value">
                                        <?php
                                        $sum = 0;
                                        foreach($cartList as $item){
                                            $sum += $item['price'] * (1 - $item['discount'] / 100)*$item['quantity'];
                                        }
                                        $_SESSION['checkout']['sum'] = $sum;
                                        echo number_format($sum , 0, ',', '.')
                                         ?>
                                     ₫</div>
                                </div>
                                <div class="totals-value" id="discount">
                                    <div class="label">Giảm giá: </div>
                                    <div class="value">0 ₫</div>
                                </div>
                                <div class="totals-value" id="cart-shipping">
                                    <div class="label">Phí vận chuyển: </div>
                                    <div class="value">0 ₫</div>
                                </div>
                                <div class="totals-value" id="cart-total">
                                    <div class="label">
                                        <strong class="text-uppercase">Tổng cộng:</strong>
                                    </div>
                                    <div class="value" style="color: red;">
                                        <strong class="order-total"> <?php echo number_format($sum , 0, ',', '.') ?> ₫</strong>
                                        <span>(Giá đã bao gồm VAT)</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4 border-left">
                    <section class="cart-step" id="cart-step-address">
                        <div class="step-title">
                            ĐỊA CHỈ GIAO HÀNG
                            <i class="fa fa-angle-down"></i>
                        </div>
                        <div class="step-content">
                            <form>
                                <div class="form-group">
                                    <input id="user_name" type="text" class="form-control form-control-md mt-3" id="full-name"
                                        required placeholder="Nhập họ tên" value= "<?php echo isset($_SESSION['user']) ? $_SESSION['user']['fullname'] : ''?>" />
                                    <input id="user_phone"  type="tel" class="form-control form-control-md mt-2"
                                        required placeholder="Nhập số điện thoại"  value= "<?php echo isset($_SESSION['user']) ? $_SESSION['user']['phone'] : ''?>" />
                                    <input id="user_email" type="email" class="form-control form-control-md mt-2"
                                        required placeholder="Nhập email"  value= "<?php echo isset($_SESSION['user']) ? $_SESSION['user']['email'] : ''?>" />
                                    <input id="address" type="text" class="form-control form-control-md mt-2" id="address"
                                        required placeholder="Nhập địa chỉ giao hàng">
                                </div>
                            </form>
                        </div>
                    </section>
                    <section class="cart-step" id="cart-step-payment">
                        <div class="step-title">
                            THANH TOÁN ĐẶT MUA
                            <i class="fa fa-angle-down"></i>
                        </div>
                        <div class="step-content">
                            <div class="cart-choice">
                                <div class="group-payment mb-3  ">
                                    <div class="title-payment">CHỌN PHƯƠNG THỨC THANH TOÁN</div>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input id="payment_radio" value="cod" required type="radio" class="form-check-input"
                                                name="payment-radio">Thanh toán tiền mặt khi nhận hàng
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input id="payment_radio" value="visa" required type="radio" class="form-check-input"
                                                name="payment-radio">Thanh toán bằng thẻ quốc tế Visa, Master, JCB
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input id="payment_radio" value="atm" required type="radio" class="form-check-input"
                                                name="payment-radio">Thẻ ATM nội địa/Internet Banking(Miễn phí thanh toán)
                                        </label>
                                    </div>
                                </div>
                                <div class="checkout mb-3">
                                    <button class="btn btn-warning btn-block">Đặt mua</button>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <?php else: ?>
                <div class="cart-des-empty">
                    <p>Chưa có sản phẩm nào trong giở hàng của bạn</p>
                    <img src="./assets/shopping-cart-not-product-2x.png">
                </div>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <script src="./controller/cart/checkout.js"></script>
<?php
include 'footer.php';
?>