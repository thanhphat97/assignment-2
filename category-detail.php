<?php
include 'header.php';
?>
    <main>
    <div class="page-breadcrumb">
        <div class="container">
            <ul id="breadcrumb">
                <li><a href="#">Trang chủ</a></li>
                <li><a class="active" href="#"></a></li>
            </ul>
        </div>
    </div>
    <section class="detailsection">
        <div class="container">
            <div class="home-title col-xl-8 col-lg-7 col-md-7 col-sm-12 col-12"></div>
            <div class="sort col-xl-4 col-lg-5 col-md-5 col-sm-12 col-12">
                <div class="input-group group-sel">
                    <label>Hiển thị</label>
                    <select class="custom-select select-sort" id='count'>
                        <option selected value='15'>15</option>
                        <option value='25'>25</option>
                        <option value='35'>35</option>
                    </select>
                </div>
                <div class="input-group group-sel"> 
                    <label>Sắp Xếp</label>
                    <select class="custom-select select-sort cost-sort" id='trend'>
                        <option value='new'>Mới nhất</option>
                        <option value='asc'>Giá: Thấp - Cao</option>
                        <option value='desc'>Giá: Cao - Thấp</option>
                    </select>
                </div>
            </div>

            <div class="details-silde row"></div>
            <div style="width: 100%">
                <div id="paging"></div>
            </div>
        </div>
    </section>

<link rel="stylesheet" type="text/css" href="./plugin/pagination/simplePagination.css">
<script src="./plugin/pagination/jquery.simplePagination.js"></script>
<script src="./controller/products/showCategoryDetail.js"></script>
<script src="./controller/products/showRecommend.js"></script>



<?php
include 'footer.php';
?>
<script src="./controller/products/product-detail.js"></script>