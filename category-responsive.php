<main>
    <div class="page-breadcrumb">
        <div class="container">
            <ul id="breadcrumb">
                <li><a href="#">Trang chủ</a></li>
                <li><a class="active" href="#">Tất cả thể loại</a></li>
            </ul>
        </div>
    </div>
    <section class="categories-section">
        <div class="container">
            <div class="home-moblie-title">TẤT CẢ THỂ LOẠI</div>
            <div class="category-slide row">
                <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                    <a class="category-item" href="./details-category.php">
                        <div class="img-category" width="64" height="64">
                            <img src="./public/img/category mobile/001-graph.svg"
                            alt="Kinhtế">
                        </div>
                        <span>Kinh Tế - Kỹ Năng</span>
                    </a>
                </div>
                <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                    <a class="category-item" href="#">
                        <div class="img-category" width="64" height="64">
                            <img src="./public/img/category mobile/002-library.svg"
                                alt="Vănhọc">
                        </div>
                        <span>Văn Học - Nghệ Thuật</span>
                    </a>
                </div>
                <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                    <a class="category-item" href="#" >
                        <div class="img-category" width="64" height="64">
                            <img src="./public/img/category mobile/003-teddy-bear.svg"
                                alt="Thiếunhi">
                        </div>
                        <span>Gia Đình - Thiếu Nhi</span>
                    </a>
                </div>
                <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                    <a class="category-item" href="#">
                        <div class="img-category" width="64" height="64">
                            <img src="./public/img/category mobile/004-gravity.svg"
                                alt="Khoa Học-Triết Học">
                        </div>
                        <span>Khoa Học - Triết Học</span>
                    </a>
                </div>
                <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                    <a class="category-item" href="#">
                        <div class="img-category" width="64" height="64">
                            <img src="./public/img/category mobile/005-crucifixion.svg"
                                alt="Tôn Giáo-Tâm Lý">
                        </div>
                        <span>Tôn Giáo - Tâm Lý</span>
                    </a>
                </div>
                <div class="col-4 col-sm-4 col-md-4 col-lg-4">
                    <a class="category-item" href="#">
                        <div class="img-category" width="64" height="64">
                            <img src="./public/img/category mobile/006-pharmacy.svg"
                                alt="Y Học-Thực Dưỡng">
                        </div>
                        <span>Y Học - Thực Dưỡng</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
</main>