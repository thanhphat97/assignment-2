<?php
// required header
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files

include_once "../../../modal/database.php";
include_once  "../../../modal/author.php";
 
// instantiate database and seller object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$seller = new Author($db);
 
// query sellers
$stmt = $seller->readAll();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // products array
    $seller_arr=array();
    $seller_arr["records"]=array();
 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $seller_item=array(
            "id" => $id,
            "storeName" => $storeName,
        );
 
        array_push($seller_arr["records"], $seller_item);
    }
 
    http_response_code(200);

    echo json_encode($seller_arr);
}
 
else{
 
    http_response_code(404);

    echo json_encode(
        array("message" => "No categories found.")
    );
}
?>
