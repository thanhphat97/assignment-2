<?php 
include_once "../../../modal/database.php";
include_once "../../../modal/cart_item.php";
include_once  "../../../modal/product.php";
require_once "../../../util/main.php";

$database = new Database();
$db = $database->getConnection();

$cart_item = new CartItem($db);
$product = new Product($db);

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

// function getTotal($data) {
//     $total = 0;
//     foreach($data as $key => $value){
//         $total += $data[$key] 
//     }

// } 

if (!isset($_POST['quantity']) || !isset($_POST['product_id'])) {
    $output = json_encode(array(
        'type' => 'error',
        'text' => 1 //empty input value
    ));
    
    echo $output;
    exit();
}
$quantity = test_input($_POST['quantity']);
$product_id = test_input($_POST['product_id']);
// $total_quantity = test_input($_POST['total_quantity']);
echo $quantity;

$product->id = $product_id;

$stmt = $product->readOne();
$num = $stmt->rowCount();

if($num > 0 ) {
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    if($quantity > $row['seller_quantity']) {
        $output = json_encode(array(
            "type" => "error",
            "text" => 2 //not enough
        ));
        echo $output;
        exit();
    }
}else {
    $output = json_encode(array(
        'type' => 'error',
        'text' => 3 //product was deleted
    ));
    echo $output;
    exit();
}

if(!isset($_SESSION['user'])) {
    if(!isset($_SESSION['cart'])){
        $_SESSION['cart'] = array();
    }
    // check if the item is in the array, if it is, do not add
    if(array_key_exists($product_id, $_SESSION['cart'])) {
        $_SESSION['cart'][$product_id] += $quantity;
    } else{
        $_SESSION['cart'][$product_id] = $quantity;
    }
    if(isset( $_SESSION['cart']['total_quantity'])){
        $_SESSION['cart']['total_quantity'] += $quantity;
    }else {  
        $_SESSION['cart']['total_quantity'] = $quantity;
    }   
    $total = $_SESSION['cart']['total_quantity'];
   
}else{
    if(array_key_exists($product_id, $_SESSION['usercart'])) {
        $newQuantity = $_SESSION['usercart'][$product_id] + $quantity;
        $_SESSION['usercart'][$product_id] = $newQuantity;
        $cart_item->quantity = $newQuantity;
        $cart_item->product_id = $product_id;
        $cart_item->update();
    } else {
        $_SESSION['usercart'][$product_id] = $quantity;
        $cart_item->quantity = $quantity;
        $cart_item->product_id = $product_id;
        $cart_item->user_id = $_SESSION['user']['id'];
        $cart_item->create();
    }
    if(isset( $_SESSION['usercart']['total_quantity'])){
        $_SESSION['usercart']['total_quantity'] += $quantity;
    }else{
        $_SESSION['usercart']['total_quantity'] = $quantity;
    }   
    $total = $_SESSION['usercart']['total_quantity'];
   
}

$output = json_encode(array(
    "type" => "success",
    "text" => $total
));
echo $output;

$database->closeConnection();
exit();
?>