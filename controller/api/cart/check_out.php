<?php
    require_once('../../../modal/database.php');
    require_once('../../../modal/product.php');
    require_once('../../../util/main.php');

    $database = new Database();
    $db = $database->getConnection();
    $pro = new Product($db);

    //Check login or not?
    if (!isset($_SESSION['user'])) {
        $output = json_encode(array(
            'type' => 'error',
            'text' => 1 
        ));
        echo $output;
        exit();
    }

    if (!isset($_POST['name_order']) || !isset($_POST['phone_order']) || !isset($_POST['email_order']) || !isset($_POST['payment']) || !isset($_POST['shipping_address']) ) {
        $output = json_encode(array(
            'type' => 'error',
            'text' => 2 //empty input value
        ));
        
        echo $output;
        exit();
    }

    // Set at cart.php
    $products = $_SESSION['checkout']['products'];
    $number  = count($products);

    
    $restQuanity = array();
    foreach($products as $product) {
        $pro->id = $product['product_id'];
        $stmt = $pro->readOne();
        $num = $stmt->rowCount();

        if($num > 0 ) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if($row['quantity'] < $product['quantity']) {
                $output = json_encode(array(
                    'type' => 'error',
                    'text' => 3 //not enough
                ));
                    echo $output;
                    exit();
            }
            else {
                $restQuanity[$product['product_id']] = $row['quantity'] - $product['quantity'];
            }
        }else {
            $output = json_encode(array(
                'type' => 'error',
                'text' => 4 //product was deleted
            ));
                echo $output;
                exit();
        }
    }



    $query = 'INSERT INTO `orders` (user_id, payment, shipping_address, total, discount) VALUES 
            (:user_id, :payment, :shipping_address, :total, :discount)';

    $stmt = $db->prepare($query);
    $stmt->bindValue(':user_id', $_SESSION['user']['id']);
    $stmt->bindValue(':payment', $_POST['payment']);
    $stmt->bindValue(':shipping_address',$_POST['shipping_address']);
    $stmt->bindValue(':total', $_SESSION['checkout']['sum']);
    $stmt->bindValue(':discount', 0);
    $stmt->execute();
    $lastId = $db->lastInsertId();
    $stmt->closeCursor();


    //Updated quantity after checkout
    foreach($products as $product) {
        $query = 'UPDATE products SET quantity = :quantity WHERE id = :id';
        $stmt = $db->prepare($query);
        $stmt->bindValue(':id', $product['product_id']);
        $stmt->bindValue(':quantity', $restQuanity[$product['product_id']]);
        $stmt->execute();
        $stmt->closeCursor();

        $query2 = 'INSERT INTO order_items(product_id, order_id, name, item_price, quantity)
                    VALUES(:product_id, :order_id, :name, :item_price, :quantity)';
        $stmt = $db->prepare($query2);
        $stmt->bindValue(':order_id', $lastId);
        $stmt->bindValue(':product_id', $product['product_id']);
        $stmt->bindValue(':name', $product['name']);
        $stmt->bindValue(':item_price', $product['price']);
        $stmt->bindValue(':quantity', $product['quantity']);
        $stmt->execute();
        $stmt->closeCursor();
    }

        $query = 'DELETE FROM cart_item WHERE user_id = :userid';
        $stmt = $db->prepare($query);
        $stmt->bindValue(':userid', $_SESSION['user']['id']);
        $stmt->execute();
        $stmt->closeCursor();    

    unset($_SESSION['checkout']);
    unset($_SESSION['usercart']);

    $database->closeConnection();

    $output = json_encode(array(
        'type' => 'success',
        'text' => $app_path,
    ));
    echo $output;
    
?>