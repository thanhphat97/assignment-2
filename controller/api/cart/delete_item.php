<?php
    require_once('../../../util/main.php');
    require_once('../../../modal/database.php');

    $database = new Database();
    $db = $database->getConnection();
    
    $id = $_POST['id'];

    if ($_SESSION['user']) {
        $query = 'delete from cart_item where user_id = :user_id AND product_id = :product_id';
        $statement = $db->prepare($query);
        $statement->bindValue(':product_id', $id);
        $statement->bindValue(':user_id', $_SESSION['user']['id']);
        $statement->execute();
        $statement->closeCursor();

        unset($_SESSION['usercart'][$id]);
    } else {
        unset($_SESSION['cart'][$id]);
    }

?>