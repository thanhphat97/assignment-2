<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../../../modal/database.php';
include_once '../../../modal/product_image.php';
 
// instantiate database and product_image object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$product_image = new ProductImage($db);

// set ID property of record to read
$product_image->id = isset($_GET['id']) ? $_GET['id'] : die();
 
// read products will be here
// query products
$stmt = $product_image->read();
$num = $stmt->rowCount();

// check if more than 0 record found
if($num>0){
 
    // products array
    $product_imgages_arr=array();
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        
        $product_imgages_arr[]=array(
            "id" => $id,
            "name" => $name,
            "product_id" => $product_id
        );
    }
    http_response_code(200);
    echo json_encode($product_imgages_arr);
}
else{
    http_response_code(404);
    echo json_encode(
        array("message" => "No product image found.")
    );
}

?>