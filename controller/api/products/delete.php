<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// include database and object file
include_once "../../../modal/database.php";
include_once  "../../../modal/product.php";
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
$product = new Product($db);
 
$data = json_decode(file_get_contents("php://input"));
if (!empty($data->id)) {

	$id = $data->id;
	$stmt = $product->delete($id);
	$results = $stmt->fetch(PDO::FETCH_ASSOC);
	echo json_encode($results);
}

else {
	http_response_code(400);
	echo json_encode(array("RETURN_TEXT" => "Unable to delete product. Data is incomplete."));
}
?>