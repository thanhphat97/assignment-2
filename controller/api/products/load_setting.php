<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../../core.php';
include_once '../../utilities.php';
include_once "../../../modal/database.php";
include_once  "../../../modal/product.php";
 
// utilities
$utilities = new Utilities();
 
// products array
$products_arr=array();

if (isset($_GET['number'])){
	$records_per_page = $_GET['number'];
	$_SESSION["records_per_page"] = $records_per_page;
}
else{
	if (!isset($_SESSION["records_per_page"])){
		$_SESSION["records_per_page"] = $records_per_page;
	}
	$records_per_page = $_SESSION["records_per_page"];
}

if (isset($_GET['order_by'])){
	$order_by = $_GET['order_by'];
	$_SESSION["order_by"] = $order_by;
}
else{
	if (!isset($_SESSION["order_by"])){
		$_SESSION["order_by"] = $order_by;
	}
	$order_by = $_SESSION["order_by"];
}

$setting = array();
$setting["list_records_per_page"] = $list_records_per_page;
$setting["list_view_sort"] = $list_view_sort;

$setting["records_per_page"] = $records_per_page;

$setting["current_sort"] = $order_by;

$products_arr["setting"]=$setting;

// set response code - 200 OK
http_response_code(200);
echo json_encode($products_arr);

?>
