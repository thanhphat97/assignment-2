<?php

// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
include_once "../../../modal/database.php";
include_once  "../../../modal/product.php";
$database = new Database();
$db = $database->getConnection();
 
$product = new Product($db);
$product->id = isset($_GET['id']) ? $_GET['id'] : die();
 
$stmt = $product->read();
$num = $stmt->rowCount();

if($num > 0 ) {
    $products_arr = array();
    $products_arr["record"] = array();
    // create array
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        extract($row);
        $product_item = array(
            "id" =>  $id,
            "name" => $name,
            "seller_name" => $seller_name,
            "description" => $description,
            "introduction" => nl2br($introduction), 
            "seller_price" => $seller_price,
            "seller_quantity" => $seller_quantity,
            "image_name" => $image_name,
            "category_id" => $category_id,
            "category_name" => $category_name
        );
        array_push($products_arr["record"], $product_item);
    }
    http_response_code(200);
    echo json_encode($products_arr);
}
 
else{
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user product does not exist
    echo json_encode(array("message" => "Product does not exist."));
}
?>