<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../../core.php';
include_once '../../utilities.php';
include_once "../../../modal/database.php";
include_once  "../../../modal/product.php";
 
// utilities
$utilities = new Utilities();
 
// get database connection
$database = new Database();
$db = $database->getConnection();

$product = new Product($db);

$user_id = isset($_GET['user_id']) ? $_GET['user_id'] : die();
 
$stmt = $product->readOrderbyId($user_id);
$num = $stmt->rowCount();

if ($num > 0) {
	$products_arr = array();
	$products_arr["records"] = array();

	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		extract($row);

		$product_item = array(
            "order_id" => $id,
            "name" => $name,
            "item_price" => $item_price,
            "modified" => $modified
		);
		array_push($products_arr["records"], $product_item);
	}
	http_response_code(200);
	echo json_encode($products_arr);
}
else{
    http_response_code(404);
    echo json_encode(array("message" => "No order"));
}
?>