<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../../core.php';
include_once '../../utilities.php';
include_once "../../../modal/database.php";
include_once  "../../../modal/product.php";
 
// utilities
$utilities = new Utilities();
 
// instantiate database and product object
$database = new Database();
$db = $database->getConnection();


$product = new Product($db);

// query products
$order_by =  $_SESSION["order_by"];
$records_per_page = $_SESSION["records_per_page"];

$category_id = isset($_GET['category_id']) ? $_GET['category_id'] : die();
$stmt = $product->readAll($category_id, $order_by);
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // products array
    $products_arr=array();
    $products_arr["records"]=array();
    $products_arr["paging"]=array();
 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $product_item=array(
            "id" => $id,
            "name" => $name,
            "description" => html_entity_decode($description),
            "introduction" => html_entity_decode($introduction), // htmlentities
            "quantity" => $quantity,
            "seller_name" => $seller_name,
            "min_price" => $min_price,
            "category_name" =>  $category_name,
            "status" => $status,
            "image_name" => $image_name,
            "category_id" => $category_id,
        );
 
        array_push($products_arr["records"], $product_item);
    }
 
 
    // include paging
    $product->category_id = $category_id;
    $total_rows=$product->count();
    $paging=$utilities->getPaging($page, $total_rows, $records_per_page, $order_by);

    $products_arr["paging"]=$paging;
 
    // set response code - 200 OK
    http_response_code(200);
    echo json_encode($products_arr);
}
 
else{
    http_response_code(404);
    echo json_encode(array("message" => "No products found."));
}
?>
