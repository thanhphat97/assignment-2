<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once "../../../modal/database.php";
include_once  "../../../modal/product.php";

$database = new Database();
$db = $database->getConnection();

$product = new Product($db);

$keywords= isset($_GET["keyword"]) ? $_GET["keyword"]: '';
 
// query products
$stmt = $product->search($keywords);
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    $products_arr=array();
    $products_arr["records"]=array();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);
 
        $product_item=array(
            "id" => $id,
			"name" => $name,
			"author" => $author,
			"description" => html_entity_decode($description),
			"introduction" => html_entity_decode($introduction), // htmlentities
			"discount" => $discount,
			"quantity" => $quantity,
			"price" => $price,
			"image_name" => $image_name,
			"category_id" => $category_id,
			"category_name" => $category_name,
        );
 
        array_push($products_arr["records"], $product_item);
    }
    http_response_code(200);

    echo json_encode($products_arr);
}
 
else{
    http_response_code(404);

    echo json_encode(
        array("message" => "No products found.")
    );
}
?>
