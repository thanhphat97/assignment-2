<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once "../../../modal/database.php";
include_once  "../../../modal/product.php";

$database = new Database();
$db = $database->getConnection();

$product = new Product($db);
$data = json_decode(file_get_contents("php://input"));

if (!empty($data->name) || !empty($data->id) || !empty($data->seller) || !empty($data->category_id) ||
	!empty($data->description) || !empty($data->introduction) || !empty($data->price)  || !empty($data->quantity) || !empty($data->image_name)) {

    $name = $data->name;
    $id = $data->id;
	$seller = $data->seller;
	$category_id = $data->category_id;
	$description = $data->description;
	$introduction = $data->introduction;
	$price = $data->price;
	$quantity = $data->quantity;
	$image_name = $data->image_name;
	$stmt = $product->update($name, $seller, $id, $category_id, $description, $introduction, $price, $quantity, $image_name);
	$results = $stmt->fetch(PDO::FETCH_ASSOC);
	echo json_encode($results);
}

else {
	http_response_code(400);
	echo json_encode(array("RETURN_TEXT" => "Unable to update product. Data is incomplete."));
}


?>
