<?php

include_once "../../../modal/database.php";
include_once "../../../modal/user.php";
include_once "../../../util/main.php";

$database = new Database();
$db = $database->getConnection();
$user = new User($db);

if (!isset($_POST['emailphone-login']) || !isset($_POST['password-login']) ) {
    $output = json_encode(array(
        'type' => 'error',
        'text' => 0 // input values are empty
    ));
    
    http_response_code(404);
    echo $output;
    exit();
   
}
// check if e-mail address is well-formed
$email = trim($_POST['emailphone-login']);
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $output = json_encode(array(
        'type' => 'error',
        'text' => 1 // Invalid email format
    ));
    echo $output;
    exit();
}

$password = trim($_POST['password-login']);
$password = sha1($email . $password);

//query database
$user->password = $password;
$user->email = $email;

$stmt = $user->validate();
$row = $stmt->fetch(PDO::FETCH_ASSOC);
$valid= $user->password ==  $row['password'] ;
if ($valid) {
    $_SESSION['user'] = $row;
    if ($row['role'] == 0) {
        $path = $app_path . 'admin/dashboard.php';
    } else {
        $path = $app_path;
    }
    $output = json_encode(array(
        'type' => 'success',
        'text' => $path
    ));

    echo $output;
} else {
    $output = json_encode(array(
        'type' => 'error',
        'text' => 2 // email or password does not match 
    ));
    echo $output;
}


?>


