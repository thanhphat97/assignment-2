<?php

require_once '../../../util/main.php';

if (isset($_SESSION['user'])) {
	unset($_SESSION['user']);
	if(isset($_SESSION['cart'])) {
		unset($_SESSION['cart']);
	}
	if(isset($_SESSION['usercart'])){
		unset($_SESSION['usercart']);
	}
	header('Location:' . $app_path);
} 

?>