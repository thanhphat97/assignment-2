<?php
// required header
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files

include_once "../../../modal/database.php";
include_once  "../../../modal/user.php";
 
// instantiate database and user object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$user = new User($db);
 
// query users
$stmt = $user->readAll();
$num = $stmt->rowCount();
 
// check if more than 0 record found
if($num>0){
 
    // products array
    $users_arr=array();
    $users_arr["records"]=array();
 
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        $user_item=array(
            "id" =>  $id,
            "email" => $email,
            "phone" => $phone,
            "password" => $password,
            "fullname" => $fullname,
            "birthdate" =>  $birthdate,
            "role" =>  $role,
            "gender" => $gender,
        );
 
        array_push($users_arr["records"], $user_item);
    }
 
    http_response_code(200);

    echo json_encode($users_arr);
}
 
else{
 
    http_response_code(404);

    echo json_encode(
        array("message" => "No user found.")
    );
}
?>
