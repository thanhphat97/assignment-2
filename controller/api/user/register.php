<?php 
include_once "../../../modal/database.php";
include_once "../../../modal/user.php";
include_once "../../../util/main.php";


$database = new Database();
$db = $database->getConnection();
$user = new User($db);

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

if (!isset($_POST['fullname']) || !isset($_POST['phone']) || !isset($_POST['password']) || !isset($_POST['email']) 
    || !isset($_POST['gender']) || !isset($_POST['birthdate'])) {
        $output = json_encode(array(
            'type' => 'error',
            'text' => 1 //empty input value
        ));
        
        echo $output;
        exit();
    }

$fullname = test_input($_POST['fullname']);
$phone = test_input($_POST['phone']);
$password = test_input($_POST['password']);
$email = test_input($_POST['email']);
$gender = test_input($_POST['gender']);
$birthdate = test_input($_POST['birthdate']);

//Hash password
$password = sha1($email . $password);

//Filter date get form datepicker
$parts = explode('/', $birthdate);//21/02/1997
$birthdate  = "$parts[2]-$parts[1]-$parts[0]";
// $date = date('Y-m-d', strtotime($birthdate));


if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $output = json_encode(array(
        'type' => 'error',
        'text' => 2 //Invalid email format
    ));
    
    echo $output;
    exit();
  }
 
  $user->email = $email ;
  $user->phone = $phone ;
  $user->fullname =$fullname ;
  $user->birthdate = $birthdate ;
  $user->password = $password;
  $user->gender = $gender;
//Check duplicate email or phone
$duplicate = $user->checkDuplicate();
if ($duplicate) {
    $output = json_encode(array(
        'type' => 'error',
        'text' => 3 //Email or phone used
    ));
    echo $output;
    exit();
  }


$sucess = $user->create();
if ($sucess) {
    $output = json_encode(array(
        'type' => 'success',
        'text' =>  $app_path
    ));

    echo $output;
} else {
    $output = json_encode(array(
        'type' => 'error',
        'text' => 4 // Error 
    ));
    echo $output;
}

$database->closeConnection();
exit();
?>