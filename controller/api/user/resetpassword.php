<?php

include "../../../util/PHPMailer-master/src/PHPMailer.php";
include "../../../util/PHPMailer-master/src/Exception.php";
include "../../../util/PHPMailer-master/src/OAuth.php";
include "../../../util/PHPMailer-master/src/POP3.php";
include "../../../util/PHPMailer-master/src/SMTP.php";

include_once "../../../modal/database.php";
include_once "../../../modal/user.php";
include_once "../../../util/main.php";

 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$database = new Database();
$db = $database->getConnection();
$user = new User($db);


//find email in DB
$userEmail = $_POST['email-reset'];
$user->email = $userEmail;
$user->findEmail();

//random newPassword
$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
$charactersLength = strlen($characters);
$new = '';
for ($i = 0; $i < 6; $i++) {
    $new .= $characters[rand(0, $charactersLength - 1)];
}

$user->new_password = $new;

if($user->id ==null){
    $output = json_encode(array(
        'type' => 'error',
        'text' => 1
    ));
    echo $output;
    exit;
}else {
    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        //Server settings
        $mail->SMTPDebug = false;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'phatkoyz@gmail.com';                 // SMTP username
        $mail->Password = 'phatpro123';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to
    
        //Recipients
        $mail->setFrom('phatkoyz@gmail.com', 'BKBook');
        $mail->addAddress($userEmail);               
    
        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'BKBook';
        $mail->Body    = 'Mật khẩu mới để đăng nhập website tikibk.com của quý khách là <b>' . $new . '</b>';
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
    
        if($mail->send()){
            if($user->updatePass()){
                $output = json_encode(array(
                    'type' => 'sucess',
                    'text' => $app_path
                ));
            } else{
                $output = json_encode(array(
                    'type' => 'error',
                    'text' => 2
                ));
            }
            echo $output;
            exit;
        } else {
            $output = json_encode(array(
                'type' => 'error',
                'text' => 3
            ));
            echo $output;
            exit;
        }
}

?>