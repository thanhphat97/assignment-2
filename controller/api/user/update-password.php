<?php 
include_once "../../../modal/database.php";
include_once "../../../modal/user.php";

$database = new Database();
$db = $database->getConnection();
$user = new User($db);

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

$data = json_decode(file_get_contents("php://input"));

if (!$data->user_id || !$data->email || !$data->old_password || !$data->new_password || !$data->confrm_password ) {
    $output = json_encode(array(
        'type' => 'error',
        'text' => 1 //empty input value
    ));
    
    echo $output;
    exit();
}

if ($data->old_password == $data->new_password){
    $output = json_encode(array(
        'type' => 'error',
        'text' => 2 //the same password
    ));
    
    echo $output;
    exit();
}

if ($data->confrm_password != $data->new_password){
    $output = json_encode(array(
        'type' => 'error',
        'text' => 3 //the same password
    ));
    
    echo $output;
    exit();
}

$user_id = test_input($data->user_id);
$user_email = test_input($data->email);
$new_password = test_input($data->new_password);

$user->email = $user_email;
$user->id = $user_id ;
$user->new_password = $new_password ;

$sucess = $user->updatePass();

if ($sucess) {

    $output = json_encode(array(
        'type' => 'success',
        'text' => 'start'
    ));

    echo $output;
} else {
    $output = json_encode(array(
        'type' => 'error',
        'text' => 4, // Error
        '$user->id' => $user->id,
        '$new_password' => $new_password,
        '$user->email' => $user->email

    ));
    echo $output;
}

$database->closeConnection();
exit();
?>
