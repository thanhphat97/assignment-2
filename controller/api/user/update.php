<?php 
include_once "../../../modal/database.php";
include_once "../../../modal/user.php";

$database = new Database();
$db = $database->getConnection();
$user = new User($db);

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

$data = json_decode(file_get_contents("php://input"));

if (!$data->fullname || !$data->phone || !$data->email ) {
    $output = json_encode(array(
        'type' => 'error',
        'text' => 1 //empty input value
    ));
    
    echo $output;
    exit();
}

$fullname = test_input($data->fullname);
$phone = test_input($data->phone);
$email = test_input($data->email);

$user->phone = $phone ;
$user->fullname = $fullname ;
$user->email = $email;

$sucess = $user->update();
if ($sucess) {
	// update session
    session_unset();
	$stmt = $user->validate();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    session_start();
    $_SESSION['user'] = $row;

    $output = json_encode(array(
        'type' => 'success',
        'text' => 'start',
        'fullname' => $_SESSION['user']['fullname']
    ));

    echo $output;
} else {
    $output = json_encode(array(
        'type' => 'error',
        'text' => 4 // Error 
    ));
    echo $output;
}

$database->closeConnection();
exit();
?>
