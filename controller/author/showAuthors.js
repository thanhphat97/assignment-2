$(document).ready(function () {
    document.title = 'Tủ sách';
    showAuthors();
});
function showAuthors() {
    var html = '';
    // get list of products from the API
    $.getJSON("http://localhost/assignment-2/controller/api/authors/read.php", function (data) {
        $.each(data.records, function (key, val) {
            html += `<div class="col-lg-2 col-md-3 col-sm-4 col-6">
                <div class="list-author">
                    <a class="author-item" href="#" target="_self">
                        <div class="img-author">
                            <img src="./assets/authors/${val.image}"
                                alt="${val.name}">
                        </div>
                        <div class="name-author">${val.name}</div>
                    </a>
                </div>
            </div>`;
        });
        // html += '<div class="row"> <a class="view-more">Xem thêm</a></div>'
        //console.log(html)
        $(".author-slide").html(html);
    });
}
