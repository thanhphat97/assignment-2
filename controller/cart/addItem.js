$(document).ready(function (){ 
    $('.buynowbutton').on("click", function (event) {
        var product = {};
        event.preventDefault();
        product.quantity = $('.amount .input-number input').val().trim();
        product.product_id = $('.product-info').data('productID').trim();
        // product.total_quantity = $('.cart-amount').text().trim();
        $.ajax({
            type: 'POST',
            url: '/assignment-2/controller/api/cart/add_item.php',
            data: product,
            success: function (data) {
                var res = JSON.parse(data);
                if (res.type == 'error') {
                    switch (res.text) {
                        case 1:
                            Swal.fire({
                                title: 'Lỗi',
                                text: "Chưa đủ dữ liệu!",
                                icon: 'error',
                                confirmButtonText: 'Đã hiểu',
                                confirmButtonColor: '#dc3545',
                            })
                            break;
                        case 2:
                            Swal.fire({
                                title: 'Lỗi',
                                text: "Số lượng sản phẩm trong kho không đủ!",
                                icon: 'error',
                                confirmButtonText: 'Đã hiểu',
                                confirmButtonColor: '#dc3545',
                            })
                            break;
                        case 3:
                            Swal.fire({
                                title: 'Lỗi',
                                text: "Sản phẩm đã bị xóa!",
                                icon: 'error',
                                confirmButtonText: 'Đã hiểu',
                                confirmButtonColor: '#dc3545',
                            })
                            break;
                    }
                } else {
                    $('.cart-amount').html(res.text);
                    Swal.fire({
                        title: 'Thành công',
                        text: "Đã thêm sản phẩm vào giỏ hàng!",
                        icon: 'success',
                        confirmButtonText: 'Tiếp tục',
                        confirmButtonColor: '#28a745',
                    })
                }
            }
        });

    });
})