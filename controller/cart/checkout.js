$(document).ready(function () {
    $('.plus').on("click", function (event) {
        var data = {};
        event.preventDefault();
        data.quantity = 1;
        data.product_id = $(this).parent().parent().attr('data-id');
        console.log(data.product_id);
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        // product.total_quantity = $('.cart-amount').text().trim();
        $.ajax({
            type: 'POST',
            url: '/assignment-2/controller/api/cart/add_item.php',
            data: data,
            success: function (data) {
                var res = JSON.parse(data);
                if (res.type == 'error') {
                    switch (res.text) {
                        case 1:
                            Swal.fire({
                                title: 'Lỗi',
                                text: "Chưa đủ dữ liệu!",
                                icon: 'error',
                                confirmButtonText: 'Đã hiểu',
                                confirmButtonColor: '#dc3545',
                            })
                            break;
                        case 2:
                            Swal.fire({
                                title: 'Lỗi',
                                text: "Số lượng sản phẩm trong kho không đủ!",
                                icon: 'error',
                                confirmButtonText: 'Đã hiểu',
                                confirmButtonColor: '#dc3545',
                            })
                            break;
                        case 3:
                            Swal.fire({
                                title: 'Lỗi',
                                text: "Sản phẩm đã bị xóa!",
                                icon: 'error',
                                confirmButtonText: 'Đã hiểu',
                                confirmButtonColor: '#dc3545',
                            })
                            break;
                    }
                } else {
                    $('.cart-amount').html(res.text);
                    location.reload('../../header.php')
                }
            }
        });

    });

    // Input counter item cart
    $('.minus').on("click", function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);

        var data = {};
        event.preventDefault();
        data.quantity = -1;
        data.product_id = $(this).parent().parent().attr('data-id');
        console.log(data.product_id);

        // product.total_quantity = $('.cart-amount').text().trim();
        $.ajax({
            type: 'POST',
            url: '/assignment-2/controller/api/cart/add_item.php',
            data: data,
            success: function (data) {
                var res = JSON.parse(data);
                if (res.type == 'error') {
                    switch (res.text) {
                        case 1:
                            Swal.fire({
                                title: 'Lỗi',
                                text: "Chưa đủ dữ liệu!",
                                icon: 'error',
                                confirmButtonText: 'Đã hiểu',
                                confirmButtonColor: '#dc3545',
                            })
                            break;
                        case 2:
                            Swal.fire({
                                title: 'Lỗi',
                                text: "Số lượng sản phẩm trong kho không đủ!",
                                icon: 'error',
                                confirmButtonText: 'Đã hiểu',
                                confirmButtonColor: '#dc3545',
                            })
                            break;
                        case 3:
                            Swal.fire({
                                title: 'Lỗi',
                                text: "Sản phẩm đã bị xóa!",
                                icon: 'error',
                                confirmButtonText: 'Đã hiểu',
                                confirmButtonColor: '#dc3545',
                            })
                            break;
                    }
                } else {
                    $('.cart-amount').html(res.text);
                    location.reload('../../header.php')
                }
            }
        });

    });


    /* Assign actions */
    $('.fa-trash-alt').click(function () {
        removeItem(this);
        $.ajax({
            type: 'post',
            url: '/assignment-2/controller/api/cart/delete_item.php',
            data: {
                id: $(this).parent().parent().attr('data-id')
            },
            success: function () {
                location.reload('../../header.php')
            }
        });
    });

    function removeItem(removeButton) {
        /* Remove row from DOM and recalc cart total */
        var productRow = $(removeButton).parent().parent().parent().parent().parent();
        console.log(productRow)
        productRow.slideUp(fadeTime, function () {
            productRow.remove();
        });
    }

    $('.checkout').on("click", function (e) {
        $.ajax({
            type: 'POST',
            url: '/assignment-2/controller/api/cart/check_out.php',
            data: {
                name_order: $('#user_name').val().trim(),
                phone_order: $('#user_phone').val().trim(),
                email_order: $('#user_email').val().trim(),
                payment: $('#payment_radio').val().trim(),
                shipping_address: $('#address').val().trim()
            },
            success: function (data) {
                var res = JSON.parse(data);
                if (res.type == 'error') {
                    switch (res.text) {
                        case 1:
                            Swal.fire({
                                title: 'Lỗi',
                                text: "Bạn chưa đăng nhập, vui lòng đăng nhập để mua!",
                                icon: 'error',
                                confirmButtonText: 'Đã hiểu',
                                confirmButtonColor: '#dc3545',
                            })
                            break;
                        case 2:
                            Swal.fire({
                                title: 'Lỗi',
                                text: "Vui lòng nhập đẩy đủ thông tin!",
                                icon: 'error',
                                confirmButtonText: 'Đã hiểu',
                                confirmButtonColor: '#dc3545',
                            })
                            break;
                        case 3:
                            Swal.fire({
                                title: 'Lỗi',
                                text: "Số lượng trong kho không đủ!",
                                icon: 'error',
                                confirmButtonText: 'Đã hiểu',
                                confirmButtonColor: '#dc3545',
                            })
                            break;
                        case 4:
                            Swal.fire({
                                title: 'Lỗi',
                                text: "Số phẩm đã bị xóa!",
                                icon: 'error',
                                confirmButtonText: 'Đã hiểu',
                                confirmButtonColor: '#dc3545',
                            })
                            break;
                    }
                } else {
                    Swal.fire({
                        title: 'Thành công',
                        text: "Đặt hàng thành công vào giỏ hàng!",
                        icon: 'success',
                        confirmButtonText: 'Tiếp tục',
                        confirmButtonColor: '#28a745',
                    }).then( function (){
                        $(location).attr('href', res.text);
                    })
                    
                }
            }
        });

    });

})