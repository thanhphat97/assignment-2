$(document).ready(function () {
    showAllCategory();
});


function showAllCategory() {
    let html = '';
    $.getJSON("http://localhost/assignment-2/controller/api/categories/read.php")
        .done(
            function (data) {
                $.each(data.records, function (key, value) {
                    url = "http://localhost/assignment-2/category-detail.php?category_id=" + value.id;
                    html += `<li class="has-sub"><a href="${url}">${value.name}</a>
                                <div class="mega-sub"><a class="title" href="${url}">${value.name}<span>
                                        </span></a>
                                    <ul class="mega-row">
                                        <li class="mega-col">
                                            <ul>`;

                    for (key in value.childrens){
                        sub_item = value.childrens[key];
                        urlSub = "http://localhost/assignment-2/category-detail.php?category_id=" + sub_item.id;
                        html += `<li><a href="${urlSub}" target="_self">${sub_item.name}<span></span></a></li>`;
                    }

                    html += `</ul></li></ul></div></li>`;
                });
                $(".menu-link").html(html);
            });
}
