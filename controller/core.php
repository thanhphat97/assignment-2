<?php
session_start();
ini_set('display_errors', 1);
error_reporting(E_ALL);
 
// home page url
$home_url="http://localhost/assignment-2/";
 
$page = 1;
 
$records_per_page = 5;
 
$from_record_num = ($records_per_page * $page) - $records_per_page;

$list_records_per_page = [3, 5, 7];

$list_view_sort = ['new' => 'Mới nhất', 'asc' => 'Giá: Thấp - Cao', 'desc' => 'Giá: Cao - Thấp'];

$order_by = 'new';

?>
