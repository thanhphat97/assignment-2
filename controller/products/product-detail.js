var id = $('#single-product').attr('product-id');
let price= {};
let key = 0;

// var urlsImage = [];
$(document).ready(function () {
    if (id != '') {
        showProductSummary();
        $('#seller_select').change(function(){
            $('.price-product span').html( numberWithCommas(price[$(this).val()]) + " VND");
        });
    }

});
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function showProductSummary() {
    var urlsImage = new Array();
    $.getJSON("http://localhost/assignment-2/controller/api/product_image/read.php?id=" + id)
        .done(function (data) {
            if (!data.message) {
                $.each(data, function (key, value) {
                    urlsImage[key] = './assets/images/' + value.name;
                });
            }
            $('#myGallery').zoomy(urlsImage);
        });
    $.getJSON("http://localhost/assignment-2/controller/api/products/read_one.php?id=" + id)
        .done(function (data) {
            if (!data.message) {
                $.each(data.record, function (key, value) {
                    let newOption = new Option(value.seller_name, key);
                    $('#seller_select').append(newOption, undefined);
                    price[key] = value.seller_price;
                })
            }
            var name_category = data.record[0].category_name;
            if (urlsImage.length == 0) {
                urlsImage[0] = './assets/images/' + data.record[0].image_name;
            }

            $("li .active").html(name_category);
            $('.product-info').data('productID',data.record[0].id)
            $('.product-name').html(data.record[0].name);
            $('.price-product span').html( numberWithCommas(price[key]) + " VND");
            $('#introduce').html(data.record[0].introduction);
            $('#myGallery').zoomy(urlsImage);
        });
}

