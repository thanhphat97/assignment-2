$(document).on('keyup', 'input#search, input#mobile-search', function(){
    var keyword = $(this).val().trim();
    if (keyword.length > 2){
        console.log(keyword)
        $.ajax({
            type:'GET',
            url:'http://localhost/assignment-2/controller/api/products/search.php',
            data:{
                'keyword': keyword.toLowerCase(),
            },
            success: function(data){
                console.log(data);
                html = '';
                items = data.records;
                for (idx in items){
                    html += `<li>
                                <a href="product-details.php?product_id=${items[idx].id}" target="_self" title="${items[idx].name}">
                                    <img width="50" src="./assets/images/${items[idx].image_name}" alt="${items[idx].name}">
                                    <h3 class="mrt5">${items[idx].name}</h3>
                                    <div class="author">${items[idx].author}</div>
                                    <span class="price">${items[idx].price * (1 - items[idx].discount / 100)}</span>
                                    <span class="price_original">${items[idx].price}</span>
                                </a>
                                <span style="display:none" class="id">${items[idx].id}</span>
                                <div class="clear"></div>
                            </li>`
                }
                $('#suggestsearch-content').html(html);
                $('.suggestsearch').show();
            },
            error: function(error){
                console.log("error")
            }
        });
    }
    else{
        $('.suggestsearch').hide();
    }
});
