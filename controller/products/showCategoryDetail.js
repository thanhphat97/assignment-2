function getCurrentPage(){
    var url = window.location.href;
    return Number(url.match(/\page=([a-z0-9\-]+)\&?/i)[1]);
}

function getAllUrlParams() {
    var url = window.location.href;
    // get query string from url (optional) or window
    var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

    // we'll store the parameters here
    var obj = {};

    // if query string exists
    if (queryString) {

        // stuff after # is not part of query string, so get rid of it
        queryString = queryString.split('#')[0];

        // split our query string into its component parts
        var arr = queryString.split('&');

        for (var i = 0; i < arr.length; i++) {
            // separate the keys and the values
            var a = arr[i].split('=');

            // set parameter name and value (use 'true' if empty)
            var paramName = a[0];
            var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];

            // (optional) keep case consistent
            paramName = paramName.toLowerCase();
            if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();

            // if the paramName ends with square brackets, e.g. colors[] or colors[2]
            if (paramName.match(/\[(\d+)?\]$/)) {

                // create key if it doesn't exist
                var key = paramName.replace(/\[(\d+)?\]/, '');
                if (!obj[key]) obj[key] = [];

                // if it's an indexed array e.g. colors[2]
                if (paramName.match(/\[\d+\]$/)) {
                    // get the index value and add the entry at the appropriate position
                    var index = /\[(\d+)\]/.exec(paramName)[1];
                    obj[key][index] = paramValue;
                } else {
                    // otherwise add the value to the end of the array
                    obj[key].push(paramValue);
                }
            } else {
                // we're dealing with a string
                if (!obj[paramName]) {
                    // if it doesn't exist, create property
                    obj[paramName] = paramValue;
                } else if (obj[paramName] && typeof obj[paramName] === 'string'){
                    // if property does exist and it's a string, convert it to an array
                    obj[paramName] = [obj[paramName]];
                    obj[paramName].push(paramValue);
                } else {
                    // otherwise add the property
                    obj[paramName].push(paramValue);
                }
            }
        }
    }

    return obj;
}

function loadProduct(){
    var params = getAllUrlParams()
    if (!('page' in params)){
        curr_page = 1;
    }
    else{
        curr_page = params.page;
    }
    var url = "http://localhost/assignment-2/controller/api/products/reading_paging.php?category_id=" + params.category_id + "&page="+ curr_page;
    var html = '';

    $.getJSON(url, function (data) {
        document.title = data.records[0].category_name;

        $.each(data.records, function (key, val) {
            html += `<div class="col-6 col-md-3 col-lg-3 col-custom">
                        <a class="product-item" href="http://localhost/assignment-2/product-details.php?product_id=${val.id}" title="${val.name}">
                            <div class="img-book">
                                <img src="./assets/images/${val.image_name}"
                                    alt="${val.name}">
                            </div>
                            <div class="caption">
                                <h3 class="name">${val.name}</h3>
                                <div class="author">${val.seller_name}</div>
                                <div class="price">
                                    <div class="new">${val.min_price} ₫</div>
                                    <div class="old">${val.min_price*(1-val.discount/100)} ₫</div>
                                </div>
                                <div class="customer-satisfaction">
                                    <div class="star-rating">
                                        <span class="fa fa-star checked" data-rating="1"></span>
                                        <span class="fa fa-star checked" data-rating="2"></span>
                                        <span class="fa fa-star checked" data-rating="3"></span>
                                        <span class="fa fa-star checked" data-rating="4"></span>
                                        <span class="fa fa-star" data-rating="5"></span>
                                        <input type="hidden" name="whatever1" class="rating-value" value="2.56">
                                    </div>
                                    <span class="comment">0 nhận xét</span>
                                </div>
                            </div>
                        </a>
                    </div>`
        });
        $("li .active").text(document.title);
        $(".home-title").text(document.title);
        $(".details-silde").html(html);

        var total_rows = data.paging.total_rows;
        var records_per_page = data.paging.records_per_page;

        $("#paging").pagination({
            items: total_rows,
            itemsOnPage: records_per_page,
            currentPage: curr_page,
            cssStyle: 'light-theme'
        });

        $('#count').val(records_per_page);
    });
}

function loadSetting(){

    $.getJSON("http://localhost/assignment-2/controller/api/products/load_setting.php", function (data) {

        var num_display = data.setting.list_records_per_page;
        var count_html = '';
        for (idx in num_display){
            count_html += `<option value='${num_display[idx]}'>${num_display[idx]}</option>`
        }
        $('#count').html(count_html);
        $('#count').val(data.setting.records_per_page);

        var list_sort = data.setting.list_view_sort;
        var sort_html = '';
        for (idx in list_sort){
            sort_html += `<option value='${idx}'>${list_sort[idx]}</option>`
        }
        $('#trend').html(sort_html);
        $('#trend').val(data.setting.current_sort);
    });
}

function saveSetting(){
    var number = $('#count').val();
    var order_by = $('#trend').val();
    sessionStorage.setItem("order_by", order_by);
    loadProduct();   
}


$(document).ready(function () {
    loadSetting();
    loadProduct();
});

$(document).on("change", "#count", function(){
    location.reload();
    saveSetting();
    loadProduct();
});

$(document).on("change", "#trend", function(){
    location.reload();
    saveSetting();
});
