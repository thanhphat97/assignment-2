$(document).ready(function () {
    showIndexProduct();
    showIndex2Product();
    showIndex3Product();
});
function showIndexProduct() {
    var html = '';
    // get list of products from the API
    $.getJSON("http://localhost/assignment-2/controller/api/products/read_index.php?order_by=new", function (data) {
        $.each(data.records, function (key, val) {
            html += `<div class="col-6 col-md-3 col-lg-3 col-custom">
                        <a class="product-item" href="http://localhost/assignment-2/product-details.php?product_id=${val.id}" title="${val.name}">
                            <div class="img-book">
                                <img src="./assets/images/${val.image_name}"
                                    alt="${val.name}">
                            </div>
                            <div class="caption">
                                <h3 class="name">${val.name}</h3>
                                <div class="author">${val.author}</div>
                                <div class="price">
                                    <div class="new"> ${val.price * (1 - val.discount / 100)} ₫</div>
                                    <div class="old">${val.price} ₫</div>
                                    <div class="promotionsale">-${val.discount}%</div>
                                </div>
                                <div class="customer-satisfaction">
                                    <div class="star-rating">
                                        <span class="fa fa-star checked" data-rating="1"></span>
                                        <span class="fa fa-star checked" data-rating="2"></span>
                                        <span class="fa fa-star checked" data-rating="3"></span>
                                        <span class="fa fa-star checked" data-rating="4"></span>
                                        <span class="fa fa-star" data-rating="5"></span>
                                        <input type="hidden" name="whatever1" class="rating-value" value="2.56">
                                    </div>
                                    <span class="comment">0 nhận xét</span>
                                </div>
                            </div>
                        </a>
                    </div>`
        });
        html += '<div class="row"> <a class="view-more">Xem thêm</a></div>'
        $(".homesection-1st .product-slide").html(html);
    });
}

function showIndex2Product() {
    var html = '';
    // get list of products from the API
    $.getJSON("http://localhost/assignment-2/controller/api/products/read_index.php?order_by=discount_desc", function (data) {
        $.each(data.records, function (key, val) {
            html += `<div class="col-6 col-md-3 col-lg-3 col-custom">
                            <a class="product-item" href="http://localhost/assignment-2/product-details.php?product_id=${val.id}" title="${val.name}">
                                <div class="img-book">
                                    <img src="./assets/images/${val.image_name}"
                                        alt="${val.name}">
                                </div>
                                <div class="caption">
                                    <h3 class="name">${val.name}</h3>
                                    <div class="author">${val.author}</div>
                                    <div class="price">
                                        <div class="new"> ${val.price * (1 - val.discount / 100)} ₫</div>
                                        <div class="old">${val.price} ₫</div>
                                        <div class="promotionsale">-${val.discount}%</div>
                                    </div>
                                    <div class="customer-satisfaction">
                                        <div class="star-rating">
                                            <span class="fa fa-star checked" data-rating="1"></span>
                                            <span class="fa fa-star checked" data-rating="2"></span>
                                            <span class="fa fa-star checked" data-rating="3"></span>
                                            <span class="fa fa-star checked" data-rating="4"></span>
                                            <span class="fa fa-star" data-rating="5"></span>
                                            <input type="hidden" name="whatever1" class="rating-value" value="2.56">
                                        </div>
                                        <span class="comment">0 nhận xét</span>
                                    </div>
                                </div>
                            </a>
                        </div>`
        });
        html += '<div class="row"> <a class="view-more">Xem thêm</a></div>'
        $(".homesection-2nd .product-slide").html(html);
    });
}

function showIndex3Product() {
    var html = '';
    // get list of products from the API
    $.getJSON("http://localhost/assignment-2/controller/api/products/read_index.php?order_by=best_selling", function (data) {
        $.each(data.records, function (key, val) {
            html += `<div class="col-6 col-md-3 col-lg-3 col-custom">
                        <a class="product-item" href="http://localhost/assignment-2/product-details.php?product_id=${val.id}" title="Có 500 Năm Như Thế (Tái bản 2019)">
                            <div class="img-book">
                                <img src="./assets/images/${val.image_name}"
                                    alt="${val.name}">
                            </div>
                            <div class="caption">
                                <h3 class="name">${val.name}</h3>
                                <div class="author">${val.author}</div>
                                <div class="price">
                                    <div class="new"> ${val.price * (1 - val.discount / 100)} ₫</div>
                                    <div class="old">${val.price} ₫</div>
                                    <div class="promotionsale">-${val.discount}%</div>
                                </div>
                                <div class="customer-satisfaction">
                                    <div class="star-rating">
                                        <span class="fa fa-star checked" data-rating="1"></span>
                                        <span class="fa fa-star checked" data-rating="2"></span>
                                        <span class="fa fa-star checked" data-rating="3"></span>
                                        <span class="fa fa-star checked" data-rating="4"></span>
                                        <span class="fa fa-star" data-rating="5"></span>
                                        <input type="hidden" name="whatever1" class="rating-value" value="2.56">
                                    </div>
                                    <span class="comment">0 nhận xét</span>
                                </div>
                            </div>
                        </a>
                    </div>`
        });
        html += '<div class="row"> <a class="view-more">Xem thêm</a></div>'
        $(".homesection-3d .product-slide").html(html);
    });
}

