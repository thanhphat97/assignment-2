$(document).ready(function () {
    var tag = document.location.search.substring(1);
    let searchParams = new URLSearchParams(tag);
    let param = searchParams.get("tag")
    switch (param) {
        case '1':
            $(".home-title").html("100 TỰA SÁCH NÊN ĐỌC TRONG ĐỜI");
            break;
        case '2':
            $(".home-title").html("100 TỰA SÁCH THIẾU NHI NÊN ĐỌC");
            break;
        case '3':
            $(".home-title").html("KHỞI NGHIỆP ĐỌC GÌ");
            break;
        case '4':
            $(".home-title").html("NẾU TÔI BIẾT ĐƯỢC KHI CÒN 20");
    }
    var html = '';
    $.getJSON("http://localhost/assignment-2/controller/api/products/recommend.php?" + tag, function (data) {
        $.each(data.records, function (key, val) {
            html += `<div class="col-6 col-md-3 col-lg-3 col-custom">
                        <a class="product-item" href="http://localhost/assignment-2/product-details.php?product_id=${val.id}" title="${val.name}">
                            <div class="img-book">
                                <img src="./assets/images/${val.image_name}"
                                    alt="${val.name}">
                            </div>
                            <div class="caption">
                                <h3 class="name">${val.name}</h3>
                                <div class="author">${val.author}</div>
                                <div class="price">
                                    <div class="new"> ${val.price * (1 - val.discount / 100)} ₫</div>
                                    <div class="old">${val.price} ₫</div>
                                    <div class="promotionsale">-${val.discount}%</div>
                                </div>
                                <div class="customer-satisfaction">
                                    <div class="star-rating">
                                        <span class="fa fa-star checked" data-rating="1"></span>
                                        <span class="fa fa-star checked" data-rating="2"></span>
                                        <span class="fa fa-star checked" data-rating="3"></span>
                                        <span class="fa fa-star checked" data-rating="4"></span>
                                        <span class="fa fa-star" data-rating="5"></span>
                                        <input type="hidden" name="whatever1" class="rating-value" value="2.56">
                                    </div>
                                    <span class="comment">0 nhận xét</span>
                                </div>
                            </div>
                        </a>
                    </div>`
        });
        $("li .active").html("Khuyên đọc")
        $(".details-silde").html(html);
    });
});