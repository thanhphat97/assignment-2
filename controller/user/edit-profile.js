$(document).ready(function(){
    $(".nav-tabs a").click(function(){
        $(this).tab('show');
    });
});

$('#checkbox1').change(function() {
    if($(this).is(":checked")) {
        $('.frm-password').show(500);
    }
    else{
        $('.frm-password').hide(500);
    }       
});

$(document).ready(function(){
    $('#submit-profile').on('click', function(){
        var userID = $(this).data("id");

        var usrInfo = {};
        usrInfo.fullname = $('#p-fullname').val().trim();
        usrInfo.phone = $('#p-phone').val().trim();
        usrInfo.email = $('#p-email').val();

        var usrInfoJSON = JSON.stringify(usrInfo);
        console.log(usrInfoJSON)
        if($('#checkbox1').is(":checked")) {
            var usrPass = {};
            usrPass.user_id = userID;
            usrPass.email = $('#p-email').val().trim();
            usrPass.old_password = $('#p-old-pw').val().trim();
            usrPass.new_password = $('#p-new-pw').val().trim();
            usrPass.confrm_password = $('#p-confrm-pw').val().trim();
            var usrPassJSON = JSON.stringify(usrPass);
            console.log(usrPassJSON);
            $.ajax({
                type: 'POST',
                url: '/assignment-2/controller/api/user/update-password.php',
                data: usrPassJSON,
                success: function (data) {
                    var res = JSON.parse(data);
                    console.log(res);
                    if (res.type == 'error') {
                        switch (res.text) {
                            case 1:
                                $('.err_update').css({
                                    'display': 'block'
                                }).text('Thông tin chưa đầy đủ');
                                break;
                            case 2:
                                $('.err_update').css({
                                    'display': 'block'
                                }).text('Trùng mật khẩu cũ');
                                break;
                            case 3:
                                $('.err_update').css({
                                    'display': 'block'
                                }).text('Xác nhận mật khẩu không khớp');
                                break;
                            case 4:
                                $('.err_update').css({
                                    'display': 'block'
                                }).text('Xảy ra lỗi vui lòng quay lại sau');
                                break;
                        }

                    } else {
                        alert("Đã cập nhật");
                    }
                }
            });
        }
        else{
            $.ajax({
                type: 'POST',
                url: './controller/api/user/update.php',
                data: usrInfoJSON,
                success: function (data) {
                    var res = JSON.parse(data);
                    console.log(res);
                    if (res.type == 'error') {
                        switch (res.text) {
                            case 1:
                                $('.err_update').css({
                                    'display': 'block'
                                }).text('Thông tin chưa đầy đủ');
                                break;
                            case 2:
                                $('.err_update').css({
                                    'display': 'block'
                                }).text('Email không đúng định dạng');
                                break;
                            case 3:
                                $('.err_update').css({
                                    'display': 'block'
                                }).text('Email hoặc số điện thoại đã có được sử dụng');
                                break;
                            case 4:
                                $('.err_update').css({
                                    'display': 'block'
                                }).text('Xảy ra lỗi vui lòng quay lại sau');
                                break;
                        }

                    } else {
                        $('#hi-user-name').text("Hi " + res.fullname.split(" ").splice(-1) + "!");
                        Swal.fire({
                            title: 'Cập nhật thành công!',
                            text: "Mua sách thôi nào!",
                            icon: 'success',
                            confirmButtonText: 'Tiếp tục',
                            confirmButtonColor: '#28a745',
                        })
                    }
                }
            });
        }
    });
    if($('#checkbox1').is(":checked")) {

    }
});
