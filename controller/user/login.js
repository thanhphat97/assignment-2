
$(document).ready(function () {
    $('#login-tab form').submit(function (event) {
        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: '/assignment-2/controller/api/user/login.php',
            data: $('#login-tab form').serialize(),
            success: function (data) {
                var res = JSON.parse(data);
                if (res.type == 'error') {
                    switch (res.text) {
                        case 0:
                            $('.err_login').css({
                                'display': 'block'
                            }).text('Email hoặc mật khẩu trống');
                            break;
                        case 1:
                            $('.err_login').css({
                                'display': 'block'
                            }).text('Email không đúng định dạng');
                            break;
                        case 2:
                            $('.err_login').css({
                                'display': 'block'
                            }).text('Thông tin đăng nhập không đúng');
                            break;
                    }
                } else {
                    $('.err_login').css({
                        'display': 'none'
                    }).text('');
                    Swal.fire({
                        title: 'Chào mừng bạn đến với Tiki',
                        text: "Mua hàng thôi nào!",
                        icon: 'success',
                        confirmButtonText: 'Tiếp tục',
                        confirmButtonColor: '#28a745',
                    }).then(()=>{
                        $(location).attr('href', res.text);
                    })
                }
            }
        });


    });

});