$(document).ready(function () {
    $('#logout-user').on("click", function () {
        console.log();
        Swal.fire({
            title: 'Bạn có chắc chắn muốn thoát!',
            icon: 'warning',
            confirmButtonText: 'Đồng ý',
            confirmButtonColor: '#28a745',
            showCancelButton: true,
            cancelButtonText: 'Hủy bỏ',
            reverseButtons: true,
            allowOutsideClick: true
        }).then((result) => {
            if (result.value) {
                $(location).attr('href', $(this).data('href'));
            }
        })
    })
});