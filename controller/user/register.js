$(document).ready(function () {
    var newUser = {};
    var done = true;
    var valid = {};

    $('#fullname').blur(function () {
        var fullname = $(this).val().trim();
        if (fullname.length > 60 || fullname.length < 2) {
            $('.err_name').css({
                'display': 'block'
            }).text('Tên phải tối thiểu 2 kí tự và tối đa 60 kí tự');
            valid.name = false;
        }
        else {
            newUser.fullname = fullname;
            valid.fullname = true;
            $('.err_name').css({
                'display': 'none'
            }).text('');
        }
    });

    $('#phone-register').blur(function () {
        var phone = $(this).val().trim();
        if (phone.length !== 10) {
            $('.err_phone').css({
                'display': 'block'
            }).text('Số điện phải có 10 chữ số');
            valid.phone = false;
        } else if (!phone.match(/^[0-9]{10}$/)) {
            $('.err_phone').css({
                'display': 'block'
            }).text('Số điện thoại không chữa chữ và kí tự đặc biệt');
            valid.name = false;
        } else {
            newUser.phone = phone;
            valid.phone = true;
            $('.err_phone').css({
                'display': 'none'
            }).text('');
        }
    });

    $('#password-register').blur(function () {
        var pwd = $(this).val().trim();
        if (pwd.length < 6 || pwd.length > 32) {
            $('.err_password').css({
                'display': 'block'
            }).text('Mật khẩu từ 6 đến 32 kí tự');
            valid.password = false;
        } else {
            newUser.password = pwd;
            valid.password = true;
            $('.err_password').css({
                'display': 'none'
            }).text('');
        }
    });

    $('#register-tab form').submit(function (event) {
        event.preventDefault();
        $('.err_password').css({
            'display': 'none'
        }).text('');

        newUser.gender = $("input[name='gender']:checked").val().trim();
        newUser.birthdate = $('#user-date').val().trim();
        newUser.email = $('#email-register').val().trim();
        newUser.fullname = $('#fullname').val().trim();
        newUser.phone = $('#phone-register').val().trim();
        newUser.password = $('#password-register').val().trim();

        for (key in valid) {
            if (valid.hasOwnProperty(key)) {
                if (!valid[key]) {
                    done = false;
                    $('.err_register').css({
                        'display': 'block'
                    }).text('Thông tin chưa chính xác vui lòng kiểm tra lại');
                    break;
                }
            }
            done = true;
        }

        if (done) {
            $.ajax({
                type: 'POST',
                url: '/assignment-2/controller/api/user/register.php',
                data: newUser,
                success: function (data) {
                    var res = JSON.parse(data);
                    if (res.type == 'error') {
                        switch (res.text) {
                            case 1:
                                $('.err_register').css({
                                    'display': 'block'
                                }).text('Thông tin đăng ký chưa đầy đủ');
                                break;
                            case 2:
                                $('.err_register').css({
                                    'display': 'block'
                                }).text('Email không đúng định dạng');
                                break;
                            case 3:
                                $('.err_register').css({
                                    'display': 'block'
                                }).text('Email hoặc số điện thoại đã có được sử dụng');
                                break;
                            case 4:
                                $('.err_register').css({
                                    'display': 'block'
                                }).text('Xảy ra lỗi vui lòng quay lại sau');
                                break;
                        }
                    } else {
                        $('.err_register').css({
                            'display': 'none'
                        }).text('');
                        Swal.fire({
                            title: 'Đăng ký thành công',
                            text: "Mua sách thôi nào!",
                            icon: 'success',
                            confirmButtonText: 'Tiếp tục',
                            confirmButtonColor: '#28a745',
                        }).then(() => {
                            $(location).attr('href', res.text);
                        })
                    }
                }
            });
        }
    });
});