$(document).ready(function () {
    $('.forgotpasswordmodal form').submit(function (event) {
        event.preventDefault();
        
        $.ajax({
            type: 'POST',
            url: '/assignment-2/controller/api/user/resetpassword.php',
            data: $('.forgotpasswordmodal form').serialize(),
            success: function (data) {
                var res = JSON.parse(data);
                if (res.type == 'error') {
                    switch (res.text) {
                        case 1:
                            $('.err_reset').css({
                                'display': 'block'
                            }).text('Email không tồn tại trong hệ thống!');
                            break;
                        case 2:
                            $('.err_reset').css({
                                'display': 'block'
                            }).text('Cập nhật dữ liệu không thành công. Vui lòng thử lại sau!');   
                            break;     
                        case 3:
                            $('.err_reset').css({
                                'display': 'block'
                            }).text('Quá trình gửi mail bị lỗi. Vui lòng thử lại sau!');
                            break;
                    }
                } else {
                    $('.err_reset').css({
                        'display': 'none'
                    }).text('');
                    Swal.fire({
                        title: 'Khôi phục mật khẩu!',
                        text: "Vui lòng kiểm tra email để lấy lại mật khẩu",
                        icon: 'success',
                        confirmButtonText: 'Tiếp tục',
                        confirmButtonColor: '#28a745',
                    }).then(()=>{
                        $(location).attr('href', res.text);
                    })
                }
            }
        });


    });

});