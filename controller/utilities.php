<?php
class Utilities{
 
    public function getPaging($page, $total_rows, $records_per_page, $order_by){
        $paging_arr=array();
 
        $total_pages = ceil($total_rows / $records_per_page);

        $paging_arr["total_rows"] = $total_rows;
 
        $paging_arr['current_page'] = $page;

        $paging_arr['records_per_page'] = $records_per_page;

         $paging_arr['order_by'] = $order_by;
 
        return $paging_arr;
    }

    public function adminGetPaging($page, $total_rows, $records_per_page){
        
        $paging_arr=array();
 
        $total_pages = ceil($total_rows / $records_per_page);

        $paging_arr["total_rows"] = $total_rows;
 
        $paging_arr['current_page'] = $page;

        $paging_arr['records_per_page'] = $records_per_page;
 
        return $paging_arr;
    }
 
}
?>
