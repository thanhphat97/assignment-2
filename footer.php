</main>
<footer>
   <div class="footer-feature">
       <div class="container">
           <div class="row align-items-center">
               <div class="col-12 col-sm-6 col-md-3 col-lg-3">
                   <div class="feature-col">
                       <div class="icon-footer">
                           <img alt="" src="./public/img/footer/icon-books.png" width="40" height="40" /></div>
                       <div class="caption">
                           <div style="font-weight: 500">14.000 SẢN PHẨM</div>
                           <div>Tuyển chọn bởi TikiBK</div>
                       </div>
                   </div>
               </div>
               <div class="col-12 col-sm-6 col-md-3 col-lg-3">
                   <div class="feature-col">
                       <div class="icon-footer">
                           <img alt="" src="./public/img/footer/icon-ship.png" width="40" height="40" /></div>
                       <div class="caption">
                           <div> MIỄN PHÍ GIAO HÀNG </div>
                           <div> Từ 150.000đ ở TP.HCM</div>
                       </div>
                   </div>
               </div>
               <div class="col-12 col-sm-6 col-md-3 col-lg-3">
                   <div class="feature-col">
                       <div class="icon-footer">
                           <img alt="" src="./public/img/footer/icon-gift.png" width="40" height="40" /></div>
                       <div class="caption">
                           <div> QUÀ TẶNG MIỄN PHÍ</div>
                           <div> Quà tặng miễn phí</div>
                       </div>
                   </div>
               </div>
               <div class="col-12 col-sm-6 col-md-3 col-lg-3">
                   <div class="feature-col">
                       <div class="icon-footer">
                           <img alt="" src="./public/img/footer/icon-dt.png" width="40" height="40" /></div>
                       <div class="caption">
                           <div> ĐỔI TRẢ NHANH CHÓNG</div>
                           <div> Hàng lỗi đổi trả nhanh chóng</div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>
   <div class="footer-main mt-3">
       <div class="container">
           <div class="row">
               <div class="col-12 col-sm-6 col-lg-3">
                   <div class="footer-col">
                       <div class="footer-title">HOTLINE</div>
                       <div class="footer-about">
                           <ul>
                               <li><i class="fas fa-phone"></i>&ensp; 039 7400 056</li>
                               <li><i class="far fa-envelope"></i>&ensp; bkbooks@hcmut.edu.vn</li>
                           </ul>
                       </div>
                   </div>
                   <div class="footer-col">
                       <div class="footer-title">VĂN PHÒNG</div>
                       <div class="footer-about">
                           <ul>
                               <li><i class="fas fa-map-marker-alt"></i>&ensp; 268 Lý Thường Kiệt, Phường
                                   14,
                                   Quận
                                   10, TP. HCM</li>
                           </ul>
                       </div>
                   </div>
               </div>
               <div class="col-12 col-sm-6 col-lg-3">
                   <div class="footer-col">
                       <div class="footer-title">HỖ TRỢ KHÁCH HÀNG</div>
                       <div class="footer-link pl-1">
                           <a class="small-text" href="#" target="_self">Các câu hỏi thường gặp</a>
                           <a class="small-text" href="#" target="_self">Hướng dẫn đặt hàng</a>
                           <a class="small-text" href="#" target="_self">Phương thức thanh toán</a>
                           <a class="small-text" href="#" target="_self">Phương thức vận chuyển</a>
                           <a class="small-text" href="#" target="_self">Chính sách đổi trả</a>
                       </div>
                   </div>
               </div>
               <div class="col-12 col-sm-6 col-lg-3">
                   <div class="footer-col">
                       <div class="footer-title">PHƯƠNG THỨC THANH TOÁN</div>
                       <div class="payment-method">
                           <div class="pay-row">
                               <img class="icon-payment" src="./public/img/paymethod/mastercard.svg" width="54"
                                   alt="">
                               <img class="icon-payment" src="./public/img/paymethod/jcb.svg" width="54" alt="">
                               <img class="icon-payment" src="./public/img/paymethod/visa.svg" width="54" alt="">
                           </div>
                           <div class="pay-row">
                               <img class="icon-payment" src="./public/img/paymethod/cash.svg" width="54" alt="">
                               <img class="icon-payment" src="./public/img/paymethod/internet-banking.svg"
                                   width="54" alt="">
                               <img class="icon-payment" src="./public/img/paymethod/installment.svg" width="54"
                                   alt="">
                           </div>
                       </div>
                   </div>
               </div>
               <div class="col-12 col-sm-6 col-lg-3">
                   <div class="footer-col">
                       <div class="footer-title">KẾT NỐI VỚI CHÚNG TÔI</div>
                       <div class="connect-us">
                           <img class="icon-connect" src="./public/img/connect/021-facebook.svg" width="30"
                               alt="Facebook">
                           <img class="icon-connect" src="./public/img/connect/045-linkedin.svg" width="30"
                               alt="Linkedin">
                           <img class="icon-connect" src="./public/img/connect/025-instagram.svg" width="30"
                               alt="Instagram">
                           <img class="icon-connect" src="./public/img/connect/011-youtube.svg" width="35"
                               alt="Youtube">
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>
   <div class="footer-bottom">
       <div class="container justify-content-center">
           <div class="footer-wrap text-center col-sm-12 col-xs-12">
               <div class="copyright">© 2020 – TikiBK</div>
               <span>Đại Học Bách Khoa Thành phố Hồ Chí Minh</span>
           </div>
       </div>
   </div>
</footer>
</div>
</div>
<div class="overlay"></div>
<script src="./controller/user/login.js"></script>
<script src="./controller/user/register.js"></script>
<script src="./controller/user/reset.js"></script>
<script src="./controller/user/logout.js"></script>
<script src="./controller/user/signup-mobile.js"></script>
<script src="./controller/products/search-product.js"></script>
</body>
</html>