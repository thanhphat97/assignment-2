<?php
require_once('./util/main.php');
require_once('./modal/database.php');
require_once('./modal/cart_item.php');

$database = new Database();
$db = $database->getConnection();

$cartModal = new CartItem($db);

//Get cart_item on DB if exists
if (isset($_SESSION['user'])) {
    if ($_SESSION['user']['role'] == 1) {
        $_SESSION['usercart'] = array();

        $cartModal->user_id = $_SESSION['user']['id'];
        $stmt = $cartModal->read();
        $num = $stmt->rowCount();
        
        if($num>0) {
            $total = 0;
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($row);
                $_SESSION['usercart'][$product_id] = $quantity;
                $total += $quantity; 
            }
            $_SESSION['usercart']['total_quantity'] = $total;
        }
        // count() function to count element in cart
        if (isset($_SESSION['cart']) && count($_SESSION['cart']) > 0) {
            // kiem tra product co trong cart?
            foreach($_SESSION['cart'] as $key => $value) {
                if($key != 'total_quantity'){
                    if (array_key_exists($key, $_SESSION['usercart'])) {
                        $new = $_SESSION['usercart'][$key] + $value;
                        $_SESSION['usercart'][$key] = $new;
                        $cartModal->quantity = $new;
                        $cartModal->product_id = $key;
                        $cartModal->update();
                    } else {
                        $_SESSION['usercart'][$key] = $value;
                        $cartModal->quantity = $value;
                        $cartModal->product_id = $key;
                        $cartModal->id = $_SESSION['user']['id'];
                        $cartModal->create();
                    }
                }
            }
            if(isset($_SESSION['usercart']['total_quantity'])) {
                $_SESSION['usercart']['total_quantity'] += $_SESSION['cart']['total_quantity'];
            }else{
                $_SESSION['usercart']['total_quantity'] = $_SESSION['cart']['total_quantity'];
            }
            unset($_SESSION['cart']);           
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>
        Mua Hàng Trực Tuyến Giá Tốt Tại TikiBK
    </title>
    <link rel="shortcut icon" href="./public/img/favicon.jpg" />
    <!-- Font -->
    <link
        href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap&subset=vietnamese"
        rel="stylesheet" />
    <link
        href="https://fonts.googleapis.com/css?family=Crimson+Pro:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap"
        rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Zhi+Mang+Xing&display=swap" rel="stylesheet">
    <!-- Awesome icon -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-0c38nfCMzF8w8DBI+9nTWzApOpr1z0WuyswL4y6x/2ZTtmj/Ki5TedKeUcFusC/k" crossorigin="anonymous" />
    <!-- bootstrap CDN -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>


    <!-- custom CSS -->
    <link rel="stylesheet" type="text/css" href="./public/css/style.css">
    <link rel="stylesheet" type="text/css" href="./public/css/stylenav.css">
    <link rel="stylesheet" type="text/css" href="./public/css/stylelogin.css">
    <link rel="stylesheet" type="text/css" href="./public/css/bookshelf.css">
    <link rel="stylesheet" type="text/css" href="./public/css/cart.css">
    <link rel="stylesheet" type="text/css" href="./public/css/category-moblie.css">
    <link rel="stylesheet" type="text/css" href="./public/css/details-category.css">
    <link rel="stylesheet" type="text/css" href="./public/css/product-details.css">
    
    

   
     <!-- Zoom image -->
    <link rel="stylesheet" href="./plugin/gallery-zoom-zoomy/dist/zoomy.css" />
    <script src="./plugin/gallery-zoom-zoomy/dist/zoomy.js"></script>


   
    <!-- custom JS -->
    <script src="./public/js/handmade.js"></script>  
    <!--Date picker -->
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>


</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
        <div class="sidebar-header">
                <div class="user-login-moblie">
                    <a class="user-name-moblie" href="./login-responsive.php">
                        <div class="icon-login-moblie">
                            <i class="fas fa-user" style="color:#189EFF; font-size: 1.3rem;"></i>
                        </div>
                        <?php if (!isset($_SESSION['user'])): ?>
                            <span>Đăng nhập</span>
                        <?php else: ?>
                            <span><?php echo $_SESSION['user']['fullname'] ?></span>
                        <?php endif; ?>
                    </a>
                </div>
                <div id="dismiss">
                    <i class="fas fa-arrow-left"></i>
                </div>
            </div>
            <ul class="nav">
                <li class="list-unstyled">
                    <a class="components" href="./index.php">
                        <div class="icon-component">
                            <i class="fas fa-home" style="font-size: 1.3rem;"></i>
                        </div>
                        <span>Trang chủ</span>
                    </a>
                </li>
                <li class="list-unstyled">
                    <a class="components" href="./category-responsive.php" href="#">
                        <div class="icon-component">
                            <i class="fa fa-list-alt" style="font-size: 1.3rem;"></i>
                        </div>
                        <span>Danh sách thể loại</span>
                    </a>
                </li>
                <li class="list-unstyled">
                    <a class="components" href="#">
                        <div class="icon-component">
                            <i class="fas fa-user" style="font-size: 1.3rem;"></i>
                        </div>
                        <span>Quản lý tài khoản</span>
                    </a>
                </li>
            </ul>
            <ul class="support-moblie">
                <li class="mb-3">HỖ TRỢ</li>
                <li><i class="fas fa-phone" style="margin-bottom: 12px;"></i>&ensp; 039 7400 056</li>
                <li><i class="far fa-envelope"></i>&ensp; bkbooks@hcmut.edu.vn</li>
            </ul>
        </nav>
        <div id="content">
            <header>
                <div class="header-main">
                    <div class="container d-flex justify-content-center">
                        <div class="row align-items-center pb-2 pt-2">
                            <div id="chir_logo align-items-center"
                                class="col-xl-3 col-lg-2 col-md-12 col-sm-12 col-xs-12">
                                <div class="sitelogo">
                                    <a class="logo" href="index.php">TIKIBK</a>
                                </div>
                                <div class="btn-mobile-bar" id="sidebarCollapse" style="display: none;">
                                    <div class="icon-left"></div>
                                    <div class="icon-right"></div>
                                </div>
                                <div class="search-mobile-btn" style="display: none;">
                                    <a class="search-toggle" href="#">
                                        <div class="search-icon">
                                            <i class="fas fa-search">
                                            </i>
                                        </div>
                                    </a>
                                </div>
                                <div class="cart-mobile" style="display: none;">
                                    <a class="cart-toggle" href="./cart.php">
                                        <div class="cart-icon">
                                            <i class="fas fa-shopping-cart">
                                            </i>
                                            <div class="cart-amount">
                                            <?php if(isset($_SESSION['user'])) {
                                                        if( isset($_SESSION['usercart']) && isset($_SESSION['usercart']['total_quantity'])){
                                                            echo  $_SESSION['usercart']['total_quantity'] ;
                                                        }else{
                                                            echo 0;
                                                        }
                                                }else{
                                                    if( isset($_SESSION['cart']) && isset($_SESSION['cart']['total_quantity'])){
                                                        echo  $_SESSION['cart']['total_quantity'] ;
                                                    }else{
                                                        echo 0;
                                                    } 
                                                  
                                                }?> 
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="box-search col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <form id="search-form" class="search-form" action="" method="GET">
                                    <input type="search" id="search" placeholder="Bạn tìm sản phẩm gì?" />
                                    <input id="search-btn" type="submit" value="Tìm kiếm" />
                                </form>
                                <form id="search-form-mobile" class="search-form" action="" method="GET">
                                    <input type="search" id="mobile-search" placeholder="Bạn tìm sản phẩm gì?" />
                                    <input id="mobile-search-btn" type="submit" value="Tìm kiếm" />
                                </form>
                                <div class="suggestsearch" style="display: none;">
                                    <ul class="wrap-suggestion" id="suggestsearch-content">
                                        <li>
                                            <a href="" target="_self" title="Tiệm Sách Cơn Mưa">
                                                <img width="50" src="https://www.netabooks.vn/Data/Sites/1/Product/34091/thumbs/tiem-sach-con-mua.jpg" alt="Tiệm Sách Cơn Mưa">
                                                <h3 class="mrt5">Tiệm Sách Cơn Mưa</h3>
                                                <div class="author">Rieko Hinata, Hisanori Yoshida</div>
                                                <span class="price">55,200 ₫</span>
                                                <span class="price_original">69,000 ₫</span>
                                            </a>
                                            <span style="display:none" class="id">34091</span>
                                            <div class="clear"></div>
                                        </li>
                                        <li>
                                            <a href="" target="_self" title="Tiếng Hàn Tổng Hợp Dành Cho Người Việt Nam - Sách Bài Tập Sơ Cấp 1">
                                                <img width="50" src="https://www.netabooks.vn/Data/Sites/1/Product/33344/thumbs/tieng-han-tong-hop-danh-cho-nguoi-viet-nam-sach-bai-tap-so-cap-1.jpg" alt="Tiếng Hàn Tổng Hợp Dành Cho Người Việt Nam - Sách Bài Tập Sơ Cấp 1">
                                                <h3 class="mrt5">Tiếng Hàn Tổng Hợp Dành Cho Người Việt Nam - Sách Bài Tập Sơ Cấp 1</h3>
                                                <div class="author">Nhiều Tác Giả</div>
                                                <span class="price">56,000 ₫</span>
                                                <span class="price_original">70,000 ₫</span>
                                            </a><span style="display:none" class="id">33344</span>
                                            <div class="clear"></div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="dropdown col-xl-3 col-lg-4 col-md-3 col-sm-6 col-xs-12 hidden-sm hidden-xs fr scroll-down2">
                                <div class="user-icon">
                                    <div class="user-select2way">
                                        <a class="user-name" href="#">
                                            <div class="icon-2way">
                                                <i class="far fa-user"></i>
                                            </div>
                                            <?php if (!isset($_SESSION['user'])): ?>
                                            <span>Đăng nhập</span>
                                            <?php else: ?>
                                            <span id="hi-user-name">Hi <?php $name = $_SESSION['user']['fullname']; $dirs = explode(' ', $name); echo end($dirs) ?>!</span>
                                            <?php endif; ?>
                                        </a>
                                    </div>
                                    <ul class="dropdown-menu">
                                        <?php if (!isset($_SESSION['user'])): ?>
                                            <li> <input type="button" class="form-control" style="background-color: #f7b500"
                                                    id="login-user" value="Đăng nhập" />
                                            </li>
                                            <li><input type="button" class="form-control" style="background-color: #189EFF; color: white;"
                                                    id="create-user" value="Tạo tài khoản" />
                                            </li>
                                        <?php else: ?>
                                            <li><a href="./profile.php"><input type="button" class="form-control" style="background-color: #fdd835; "
                                                    id="profile-user" value="Quản lý tài khoản" /></a>
                                            </li>
                                            <li><input type="button" data-href="/assignment-2/controller/api/user/logout.php" class="form-control"  style="background-color: #DB4437; color: white;"
                                                    id="logout-user"value="Thoát" />
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                                <div class="cart">
                                    <a class="cart-toggle" href="./cart.php">
                                        <div class="cart-icon">
                                            <i class="fas fa-shopping-cart">
                                            </i>
                                            <div class="cart-amount"> 
                                                <?php if(isset($_SESSION['user'])) {
                                                        if( isset($_SESSION['usercart']) && isset($_SESSION['usercart']['total_quantity'])){
                                                            echo  $_SESSION['usercart']['total_quantity'] ;
                                                        }else{
                                                            echo 0;
                                                        }
                                                }else{
                                                    if( isset($_SESSION['cart']) && isset($_SESSION['cart']['total_quantity'])){
                                                        echo  $_SESSION['cart']['total_quantity'] ;
                                                    }else{
                                                        echo 0;
                                                    } 
                                                  
                                                }?> 
                                            </div>
                                        </div>
                                        <span>Giỏ hàng</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-menu">
                    <div class="container">
                        <div class="header-wrap">
                            <div class="sidebar-menu">
                                <div class="btn-sidebar"><i class="fa fa-align-justify pr-2"
                                        style="font-size:20px"></i><span>TẤT CẢ DANH MỤC</span></div>
                                <div class="menu-wrap">
                                    <ul class="menu-link">
                                    </ul>
                                </div>
                            </div>
                            <ul class="items-header">
                                <li><a href="./bookshelf.php">XU HƯỚNG</a></li>
                                <li><a href="./category-detail.php?page=1">MUA NHIỀU NHẤT</a></li>
                                <li><a href="#">HÀNG MỚI VỀ</a></li>
                                <li><a href="#">VỀ TIKIBK</a></li>
                                <li><a class="support-list" href="">HỖ TRỢ KHÁCH HÀNG <i
                                            class="fa fa-angle-down"></i></a>
                                    <ul class="sub-menu">
                                        <li><a href="./question.php">Các câu hỏi thường gặp</a></li>
                                        <li><a href="#">Hướng dẫn đặt hàng</a></li>
                                        <li><a href="#">Phương thức thanh toán</a></li>
                                        <li><a href="#">Phương thức vận chuyển</a></li>
                                        <li><a href="#">Chính sách đổi trả</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
            <!-- modal -->
            <div id="modal" class="remodal">
                <div class="modal-pharse">
                    <div class="description-box">
                        <div id="login-tab" class="tab-item">
                            <h1>Đăng nhập</h1>
                            <p class="text-justify">
                                Đăng nhập để theo dõi đơn hàng, lưu danh sách sản phẩm yêu thích
                                và nhận nhiều ưu đãi hấp dẫn.
                            </p>
                        </div>
                        <div id="register-tab" class="tab-item">
                            <h1>Tạo tài khoản</h1>
                            <p class="text-justify">
                                Tạo tài khoản để theo dõi đơn hàng, lưu danh sách sản phẩm yêu
                                thích, nhận nhiều ưu đãi hấp dẫn.
                            </p>
                        </div>
                        <div class="modal-img"></div>
                    </div>
                    <div class="tab-box">
                        <div class="tab">
                            <button class="tablinks">
                                <a href="#login-tab"></a>Đăng nhập
                            </button>
                            <button class="tablinks">
                                <a href="#register-tab"></a>Tạo tài khoản
                            </button>
                        </div>
                        <div class="tab-content">
                            <div id="login-tab" class="tab-item">
                                <form method='POST'>
                                    <div class="form-row mt-3">
                                        <label for="emailphone-login" class="col-2 col-form-label">Email</label>
                                        <div class="col-7 ml-2">
                                            <input type="email" class="form-control" id="emailphone-login"
                                            name="emailphone-login" placeholder="Nhập Email" required />
                                        </div>
                                    </div>
                                    <div class="form-row mt-3">
                                        <label for="password-login" class="col-2 col-form-label">Mật khẩu</label>
                                        <div class="col-7 ml-2 pb-5">
                                            <input type="password" class="form-control" id="password-login" name="password-login"
                                                placeholder="Mật khẩu từ 6 đến 32 ký tự" required />
                                            <div class="err_msg err_login" style="font-size:small; color: red"></div>
                                            <p class="mt-2">
                                                Quên mật khẩu? Nhấn vào <a class="forgetpass" href="#">đây</a>
                                            </p>
                                            <button type="submit" class="form-control" style="background-color: #fdd835"
                                                id="submit-login" >Đăng nhập</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="register-tab" class="tab-item">
                                <form method='POST'>
                                    <div class="form-row mt-3">
                                        <label for="fullname" class="col-2 col-form-label">Họ và tên</label>
                                        <div class="col-7 ml-2">
                                            <input type="text" class="form-control" id="fullname" name="fullname"
                                                placeholder="Nhập họ tên" required />
                                                <div class="err_msg err_name" style="font-size:small; color: red"></div>
                                        </div>
                                    </div>
                                    <div class="form-row mt-3">
                                        <label for="phone-register" class="col-2 col-form-label">SĐT</label>
                                        <div class="col-7 ml-2">
                                            <input type="tel" class="form-control" id="phone-register" name="phone-register"
                                                placeholder="Nhập số điện thoại" required />
                                            <div class="err_msg err_phone" style="font-size:small; color: red"></div>
                                        </div>
                                    </div>
                                    <div class="form-row mt-3">
                                        <label for="email-register" class="col-2 col-form-label">Email</label>
                                        <div class="col-7 ml-2">
                                            <input type="email" class="form-control" id="email-register" name="email-register"
                                                placeholder="Nhập email" required />
                                            <div class="err_msg err_email" style="font-size:small; color: red"></div>
                                        </div>
                                    </div>
                                    <div class="form-row mt-3">
                                        <label for="password-register" class="col-2 col-form-label">Mật khẩu</label>
                                        <div class="col-7 ml-2">
                                            <input type="password" class="form-control" id="password-register" name="password-register"
                                                placeholder="Mật khẩu từ 6 đến 32 ký tự" required />
                                            <div class="err_msg err_password" style="font-size:small; color: red"></div>
                                        </div>
                                    </div>
                                    <div class="form-row mt-3">
                                        <label for="gender" class="col-2 col-form-label">Giới tính</label>
                                        <div class="col-7 ml-2" >
                                            <div class="custom-control custom-radio custom-control-inline mt-2">
                                                <input type="radio" id="radio-male" name="gender"
                                                    class="custom-control-input" value="1"  required />
                                                <label class="custom-control-label" for="radio-male">Nam</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline mt-2">
                                                <input type="radio" id="radio-female" name="gender"
                                                    class="custom-control-input"  value="0" required />
                                                <label class="custom-control-label" for="radio-female">Nữ</label>
                                            </div>
                                            <div class="err_msg err_date" style="font-size:small; color: red"></div>
                                        </div>
                                    </div>
                                    <div class="form-row mt-3">
                                        <label for="user-date" class="col-2 col-form-label">Ngày sinh</label>
                                        <div class="col-7 ml-2">
                                            <input id="user-date" class="datepicker" width="100%" autocomplete="off"
                                                name="birthday" placeholder="Chọn ngày sinh"
                                                data-date-format="dd/mm/yyyy" required />
                                            <button type="submit" class="form-control mt-4"
                                                style="background-color: #fdd835" id="submit-register" >Tạo tài khoản</button>
                                                <div class="err_msg err_register" style="font-size:small; color: red"></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="forgot-password">
                <div class="forgotpasswordmodal">
                    <h4 class="title-forgot">Quên mật khẩu?</h4>
                    <p class="description">Vui lòng cung cấp email hoặc số điện thoại đăng nhập để lấy lại mật khẩu.
                    </p>
                    <form >
                        <input class="input-forgot" type="text" id="email" name="email-reset"
                            placeholder="Nhập email hoặc số điện thoại" value="">
                        <div class="err_msg err_reset mb-2" style="font-size:small; color: red"></div>
                        <button type="submit" class="btn-submit">Gửi</button>
                    </form>
                </div>
            </div>
    <script src="./controller/categories/showAllCategory.js"></script>
