<main>
    <div class="home-flex">
        <div class="container">
            <section class="home-menu">
                <ul class="menu-link">  
                    <!-- <li class="has-sub"><a href="#">Sách Kinh Tế - Kỹ Năng</a>
                        <div class="mega-sub"><a class="title" href="./details-category.php">Sách Kinh Tế - Kỹ Năng<span>
                                    (235)</span></a>
                            <ul class="mega-row">
                                <li class="mega-col">
                                    <ul>
                                        <li><a href="#" target="_self">Kinh Tế - Chính Trị<span>
                                                    (221)</span></a></li>
                                        <li><a href="#" target="_self">Sách Khởi Nghiệp<span>
                                                    (101)</span></a>
                                        </li>
                                        <li><a href="#" target="_self">Sách Quản Trị - Lãnh
                                                Đạo<span>(238)</span></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </li> -->
                </ul>
            </section>
            <section class="home-banner-wrap">
                <div class="home-banner">
                    <div id="carouselBanner" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carouselBanner" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselBanner" data-slide-to="1"></li>
                            <li data-target="#carouselBanner" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <img src="./public/img/courasel/2bafb7e286568f2443d39cd14a637fb9.jpg" alt="nhung-con-duong-to-lua-1200x630">
                            </div>
                            <div class="carousel-item">
                                <img src="./public/img/courasel/8a74e6136074fffd0e27ed09ac4f094e.jpg" alt="nxb-tri-thuc-1200x630">
                            </div>
                            <div class="carousel-item">
                                <img src="./public/img/courasel/8016e66a8a8acc555eea53553aacec41.png" alt="sachmoi-1200x630">
                            </div>
                            <div class="carousel-item">
                                <img src="./public/img/courasel/af8476d69b1f221865f7433c096c5dc3.jpg" alt="sachmoi-1200x630">
                            </div>
                        </div>

                        <!-- Controls -->
                        <a class="carousel-control-prev" href="#carouselBanner" role="button"
                            data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselBanner" role="button"
                            data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <div class="banner-ads">
                    <div class="item">
                        <a class="img" href="#">
                            <img src="./public/img/12ee231a56e22d0aa6b4c7e3475dca5f.jpg" alt="Nguyễn Nhật Ánh 2">
                        </a>
                    </div>
                    <div class="item">
                        <a class="img" href="#">
                            <img src="./public/img/5d29396a3bfe7fa7c3d5117ea61b4161.jpg" alt="Sách Mới Phát Hành">
                        </a>
                    </div>
                    <div class="item">
                        <a class="img"  href="#">
                            <img style="border-bottom-right-radius: 5px;" src="./public/img/eac96795818b1b81634426032e23fa2a.jpg"
                                alt="NetaBooks miễn phí giao hàng toàn quốc">
                        </a>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <script src="./controller/categories/showAllCategory.js"></script>
