<?php
include 'header.php';
include 'home-flex.php';
?>
    <section class="homesection-1st">
        <div class="container">
            <div class="home-title">SÁCH MỚI TUYỂN CHỌN</div>
            <div class="product-slide row"></div>
        </div>
    </section>
    <section class="homesection-2nd">
        <div class="container">
            <div class="home-title">SÁCH HOT - GIẢM GIÁ</div>
            <div class="product-slide row"></div>
        </div>
    </section>
    <section class="homesection-3d">
        <div class="container">
            <div class="home-title">SÁCH MUA NHIỀU / CÒN ÍT</div>
            <div class="product-slide row"></div>
        </div>
    </section>
    <section class="homesection-4th">
        <div class="container">
            <div class="home-title">TIKIBK KHUYÊN MUA</div>
            <div class="recommend-slide">
                <div class="recommendations" data-id='1'>
                    <a class="ads-banner" href="#" title="">
                        <img src="./public/img/recommend/1.jpg" alt="">
                    </a>
                </div>
                <div class="recommendations" data-id='2'>
                    <a class="ads-banner" href="#" title="">
                        <img src="./public/img/recommend/2.jpg" alt="">
                    </a>
                </div>
                <div class="recommendations" data-id='3'>
                    <a class="ads-banner" href="#" title="">
                        <img src="./public/img/recommend/3.jpg" alt="">
                    </a>
                </div>
                <div class="recommendations" data-id='4'>
                    <a class="ads-banner" href="#" title="">
                        <img src="./public/img/recommend/4.jpg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </section>
<script src="./controller/products/showProduct.js"></script>
<script src="./controller/products/redirect-product-recommend.js"></script>

<?php
include 'footer.php';
?>