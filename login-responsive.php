<?php
include 'header.php';
?>  
<main>
<div class="page-breadcrumb">
    <div class="container">
        <ul id="breadcrumb">
            <li><a href="#">Trang chủ</a></li>
            <li><a href="#">Đăng nhặp</a></li>
            <li><a href="#">Đăng ký</a></li>
            <li><a href="#">Tủ sách</a></li>
            <li><a class="active" href="#">Khuyên đọc</a></li>
        </ul>
    </div>
</div>
    <section class="loginsection">
    <div class="container">
        <div class="home-moblie-title">ĐĂNG NHẬP / ĐĂNG KÝ</div>
        <div class="box-login row">
            <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="tab-pane login-form pb-4">
                    <form>
                        <div class="title-login text-center">ĐĂNG NHẬP</div>
                        <div class="form-group">
                            <label for="email-phone-login">Email</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                                </div>
                                <input type="text" class="form-control form-control-md" id="email-phone-login" name="email-phone" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="user-pwd-login">Mật khẩu</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-key"></i></span>
                                </div>
                                <input type="password" class="form-control form-control-md" id="user-pwd-login" pattern=".{6,32}" title="Mật khẩu của bạn" name="pwd" required="">
                            </div>
                        </div>
                        <a class="forgetpass" href="#" style="display:block; text-align:center;color:#ffc107;margin-bottom: 1rem;">Quên mật khẩu?</a>
                        <button type="submit" class="btn btn-success btn-block">Đăng nhập</button>
                    </form>
                </div>
            </div>
            <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="tab-pane signup-form pb-4" id="create">
                    <form>
                        <div class="title-login text-center">ĐĂNG KÝ THÀNH VIÊN MỚI</div>
                        <div class="form-group">
                            <label for="full-name">Họ và tên</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                                </div>
                                <input type="text" class="form-control form-control-md" id="full-name" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="user-phone">Số điện thoại</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                </div>
                                <input id="user-phone" type="tel" class="form-control form-control-md" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="user-email">Email</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                                </div>
                                <input id="user-email" type="email" class="form-control form-control-md" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="user-pwd">Mật khẩu</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-key"></i></span>
                                </div>
                                <input id="user-pwd" type="password" class="form-control form-control-md" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="user-gender">Giới tính</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-transgender"></i></span>
                                </div>
                                <select id="user-gender" name="sex" class="form-control" required="">
                                    <option value="" disabled="" selected="">Chọn giới tính của bạn</option>
                                    <option value="1">Nam</option>
                                    <option value="0">Nữ</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="user-date">Ngày sinh</label>
                            <div class="input-group">
                                <input id="user-date" class="datepicker" width="100%" autocomplete="off" name="birthday" placeholder="Chọn ngày sinh" data-date-format="dd/mm/yyyy" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address">Địa chỉ</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-address-book"></i></span>
                                </div>
                                <input type="text" class="form-control form-control-md" id="address" required="">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success btn-block mt-4">Đăng ký</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include 'footer.php';
?>