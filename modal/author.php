<?php

class Author {
	private $conn;
	private $table_name = "seller";

	// object properties
	public $id;
	public $storeName;

	function __construct($db) {
		$this->conn = $db;
	}

	/**
	 * Get all authors
	 * @param none
	 * @return list of authors
	*/
	public function readAll() {
		// SELECT DISTINCT author FROM `products` ORDER BY author ASC;
		$query = "SELECT
				id, storeName
				FROM
				" . $this->table_name . "
				ORDER BY
				id";

		$stmt = $this->conn->prepare($query);

		// execute query
		$stmt->execute();

		return $stmt;
	}

}
?>
