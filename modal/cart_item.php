<?php
class CartItem{
    
    // database connection and table name
    private $conn;
    private $table_name = "cart_item";
 
    // object properties
    public $id;
    public $product_id;
    public $user_id;
    public $quantity;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

     // create product -
     public function create(){
        
        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                quantity = ?, product_id= ? , user_id= ?";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        $this->user_id=htmlspecialchars(strip_tags($this->user_id));
        $this->product_id=htmlspecialchars(strip_tags($this->product_id));
        $this->quantity=htmlspecialchars(strip_tags($this->quantity));
    
        // bind new values
        $stmt->bindParam(1, $this->quantity);
        $stmt->bindParam(2, $this->product_id);
        $stmt->bindParam(3 , $this->user_id);
    
        // execute query
        if($stmt->execute()){
            return true;
        }
        return false;
    }

    // read image  by product id
    public function read(){
    
        // select all query
        $query = "SELECT
                    id, product_id, user_id, quantity
                FROM
                    " . $this->table_name . " 
                WHERE 
                user_id = ?";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);

        $this->user_id = htmlspecialchars(strip_tags($this->user_id));
        // bind id of product to be updated
        $stmt->bindParam(1, $this->user_id);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }


    // update the product -
    public function update(){
    
        // update query
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    quantity = :quantity
                WHERE
                    product_id = :product_id";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->product_id=htmlspecialchars(strip_tags($this->product_id));
        $this->quantity=htmlspecialchars(strip_tags($this->quantity));
    
        // bind new values
        $stmt->bindParam(':product_id', $this->product_id);
        $stmt->bindParam(':quantity', $this->quantity);
    
        // execute the query
        if($stmt->execute()){
            return true;
        }
    
        return false;
    }

    // // delete the product
    // function delete(){
    
    //     // delete query
    //     $query = "DELETE FROM " . $this->table_name . " WHERE product_id = ?";
    
    //     // prepare query
    //     $stmt = $this->conn->prepare($query);
    
    //     // sanitize
    //     $this->id=htmlspecialchars(strip_tags($this->id));
    
    //     // bind id of record to delete
    //     $stmt->bindParam(1, $this->id);
    
    //     // execute query
    //     if($stmt->execute()){
    //         return true;
    //     }
    
    //     return false;
    // }

    public function readJoin() {
		$query = "SELECT 
                p.name, p.price, p.discount, p.image_name, p.id, c.quantity 
                FROM 
                " . $this->table_name . " c, products AS p 
                WHERE 
                c.user_id = ? AND c.product_id = p.id";

        $stmt = $this->conn->prepare($query);
        $this->user_id=htmlspecialchars(strip_tags($this->user_id));

        $stmt->bindParam(1, $this->user_id);
		$stmt->execute();

		return $stmt;
    }
    
    public function count() {
		$query = "";
			$query = "SELECT COUNT(*) FROM " . $this->table_name . "";
		$stmt = $this->conn->prepare($query);

		$stmt->execute();
		return $stmt->fetchColumn();
	}
}