<?php

class Category {
	private $conn;
	private $table_name = "category";

	// object properties
	public $id;
	public $parentID;
	public $name;
	public $created;
	public $modified;


	function __construct($db) {
		$this->conn = $db;
	}

	public function create() {
		$query = "INSERT INTO
				" . $this->table_name . "
				SET
				name= :name";

		$stmt = $this->conn->prepare($query);

		$name = htmlspecialchars(strip_tags($this->name));

		$stmt->bindParam(":name", $name);

		// execute query
		if ($stmt->execute()) {
			return true;
		}

		return false;
	}
	public function readParent() {
		$query = "SELECT
				id, name, created
				FROM
				" . $this->table_name . "
				WHERE
				parentID = 0
				ORDER BY
				created";

		$stmt = $this->conn->prepare($query);

		// execute query
		$stmt->execute();

		return $stmt;
	}
	public function readSub() {
		$query = "SELECT
				id, name, created
				FROM
				" . $this->table_name . "
				WHERE
				parentID = ?
				ORDER BY
				created";

		$stmt = $this->conn->prepare($query);
		
		$this->id=htmlspecialchars(strip_tags($this->id));
        $stmt->bindParam(1, $this->id);

		// execute query
		$stmt->execute();

		return $stmt;
	}


	public function readAll() {
		$query = "SELECT
				id, name, created
				FROM
				" . $this->table_name . "
				ORDER BY
				created";

		$stmt = $this->conn->prepare($query);

		// execute query
		$stmt->execute();

		return $stmt;
	}

	public function readOne() {
			$query = "SELECT
				id, name, created
				FROM
				" . $this->table_name . "
				WHERE
				id = ?
				LIMIT
				0,1";
		   // prepare query statement
		$stmt = $this->conn->prepare($query);
		
		$id=htmlspecialchars(strip_tags($this->id));
        $stmt->bindParam(1, $this->id);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $this->name = $row['name'];

	}

	public function update() {
		$query = "UPDATE
				" . $this->table_name . "
				SET
				name= :name
				WHERE
				id= :id";

		   // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $name=htmlspecialchars(strip_tags($this->name));
        $id=htmlspecialchars(strip_tags($this->id));
    
        // bind new values
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':id', $id);
    
        // execute the query
        if($stmt->execute()){
            return true;
        }
    
        return false;

	}

	public function delete() {
		$query = "DELETE FROM
				" . $this->table_name . "
				WHERE
				id= ?";

		$stmt = $this->conn->prepare($query);
        $id=htmlspecialchars(strip_tags($this->id));
        $stmt->bindParam(1, $id);
        if($stmt->execute()){
            return true;
        }
    
        return false;
        

	}
	
	public function count() {
		$query = "";
			$query = "SELECT COUNT(*) FROM " . $this->table_name . "";
		$stmt = $this->conn->prepare($query);

		$stmt->execute();
		return $stmt->fetchColumn();
	}
}

?>