<?php

class Product {
	private $conn;
	private $table_name = "product";

	// object properties
	public $id;
	public $name;
	public $description;
	public $introduction;
	public $quantity;
	public $status;
	public $image_name;
	public $category_id;
	public $category_name;
	public $created;

	function __construct($db) {
		$this->conn = $db;
	}

	public function create($name, $seller, $category_id, $description, $introduction, $price, $quantity, $image_name) {
		// query to insert record
		$query = "CALL AddProducts(:name,:description,:introduction, :category_id, :image_name,:seller, :quantity, :price, @varOutParam)";
                   
		// prepare query
		$stmt = $this->conn->prepare($query);
		$name = htmlspecialchars(strip_tags($name));
		$author = htmlspecialchars(strip_tags($seller));
		$category_id = htmlspecialchars(strip_tags($category_id));
		$description = htmlspecialchars(strip_tags($description));
		$introduction = htmlspecialchars($introduction);
		$price = htmlspecialchars(strip_tags($price));
		$quantity = htmlspecialchars(strip_tags($quantity));
		$image_name = htmlspecialchars(strip_tags($image_name));
	

		// bind values
		$stmt->bindParam(":name", $name);
		$stmt->bindParam(":seller", $seller);
		$stmt->bindParam(":price", $price);
		$stmt->bindParam(":introduction", $introduction);
		$stmt->bindParam(":description", $description);
		$stmt->bindParam(":category_id", $category_id);
		$stmt->bindParam(":quantity", $quantity);
		$stmt->bindParam(":image_name", $image_name);

		$stmt->execute();

		return $stmt;
	}

	public function read() {
		$query = "SELECT
				s.storeName as seller_name, d.price as seller_price, d.quantity as seller_quantity, c.name as category_name, p.id, p.name, p.description, p.introduction, p.image_name, p.category_id
				FROM
                    " . $this->table_name . " p
                   	INNER JOIN
					   category c ON p.category_id = c.id
				   INNER JOIN
					   provide d ON p.id = d.product_id
				   INNER JOIN
					   seller s ON s.id = d.seller_id
			   		WHERE
				    p.id = ?
                	ORDER BY
                    p.id";

		$stmt = $this->conn->prepare($query);
		$this->id = htmlspecialchars(strip_tags($this->id));

		$stmt->bindParam(1, $this->id);

		$stmt->execute();

		return $stmt;
	}

	public function readOne() {
		$query = "SELECT
				s.storeName as seller_name, d.price as seller_price, d.quantity as seller_quantity, c.name as category_name, p.id, p.name, p.description, p.introduction, p.image_name, p.category_id
				FROM
                    " . $this->table_name . " p
					INNER JOIN
						category c ON p.category_id = c.id
					INNER JOIN
						provide d ON p.id = d.product_id
					INNER JOIN
						seller s ON s.id = d.seller_id
                WHERE
                	p.id = ?
                LIMIT
					0,12";
		$stmt = $this->conn->prepare($query);
		$this->id = htmlspecialchars(strip_tags($this->id));

		$stmt->bindParam(1, $this->id);

		$stmt->execute();

		return $stmt;
	}

	public function readOneAdmin($provide) {
		$query = "SELECT
				s.storeName as seller_name, d.seller_id as seller_id, d.price as seller_price, d.quantity as seller_quantity, c.name as category_name, p.id, p.name, p.description, p.introduction, p.image_name, p.category_id
				FROM
                    " . $this->table_name . " p
					INNER JOIN
						category c ON p.category_id = c.id
					INNER JOIN
						provide d ON p.id = d.product_id
					INNER JOIN
						seller s ON s.id = d.seller_id
                WHERE
                	p.id = ? and d.id = ?
                LIMIT
					0,12";
		$stmt = $this->conn->prepare($query);
		$this->id = htmlspecialchars(strip_tags($this->id));

		$stmt->bindParam(1, $this->id);
		$stmt->bindParam(2, $provide);


		$stmt->execute();

		return $stmt;

	}

	public function read_byCreated() {
		$query = "SELECT
				c.name as category_name, p.id, p.name, p.author, p.description, p.introduction,  p.quantity, p.discount, p.image_name, p.category_id
				FROM
                    " . $this->table_name . " p
                    LEFT JOIN
					category c
                            ON p.category_id = c.id
               	ORDER BY p.created DESC
                LIMIT 12";

		$stmt = $this->conn->prepare($query);

		$stmt->execute();

		return $stmt;
	}

	public function read_random() {
		$query = "SELECT
				c.name as category_name, p.id, p.name, p.author, p.description, p.introduction,  p.quantity, p.discount, p.image_name, p.category_id
				FROM
                    " . $this->table_name . " p
                    LEFT JOIN
						category c
                            ON p.category_id = c.id
               	ORDER BY RAND()
                LIMIT 15";

		$stmt = $this->conn->prepare($query);

		$stmt->execute();

		return $stmt;
	}

	public function delete($id) {
		// delete query
			// query to insert record
			$query = "CALL deleteProduct(:id, @varOutParam)";
                   
			// prepare query
			$stmt = $this->conn->prepare($query);
			$id = htmlspecialchars(strip_tags($id));
		
	
			// bind values
			$stmt->bindParam(":id", $id);
	
			$stmt->execute();
	
			return $stmt;

	}

	public function update($name, $seller, $id, $category_id, $description, $introduction, $price, $quantity, $image_name) {
		$query = "CALL updateProduct(:name,:description,:introduction, :category_id, :image_name, :seller, :quantity, :price, :id, @varOutParam)";
                   
		// prepare query
		$stmt = $this->conn->prepare($query);
		$name = htmlspecialchars(strip_tags($name));
		$author = htmlspecialchars(strip_tags($seller));
		$category_id = htmlspecialchars(strip_tags($category_id));
		$description = htmlspecialchars(strip_tags($description));
		$introduction = htmlspecialchars($introduction);
		$price = htmlspecialchars(strip_tags($price));
		$quantity = htmlspecialchars(strip_tags($quantity));
		$image_name = htmlspecialchars(strip_tags($image_name));
		$id = htmlspecialchars(strip_tags($id));
	
		// bind values
		$stmt->bindParam(":name", $name);
		$stmt->bindParam(":seller", $seller);
		$stmt->bindParam(":price", $price);
		$stmt->bindParam(":introduction", $introduction);
		$stmt->bindParam(":description", $description);
		$stmt->bindParam(":category_id", $category_id,  PDO::PARAM_INT);
		$stmt->bindParam(":quantity", $quantity);
		$stmt->bindParam(":image_name", $image_name);
		$stmt->bindParam(":id", $id);


		$stmt->execute();

		return $stmt;

	}

	public function search($keywords) {

		$query = "SELECT
				 c.name as category_name, p.*
			   FROM
				   " . $this->table_name . " p
				   LEFT JOIN
					   category c
					   ON p.category_id = c.id
			   WHERE
				   p.name LIKE ? OR p.description LIKE ? OR p.introduction LIKE ? OR c.name LIKE ? 
			   ORDER BY
				   p.created DESC
			   LIMIT 6";
   
	   // prepare query statement
	   $stmt = $this->conn->prepare($query);
   
	   // sanitize
	   $keywords=htmlspecialchars(strip_tags($keywords));
	   $keywords = "%{$keywords}%";
   
	   // bind
	   $stmt->bindParam(1, $keywords);
	   $stmt->bindParam(2, $keywords);
	   $stmt->bindParam(3, $keywords);
	   $stmt->bindParam(4, $keywords);
   
	   // execute query
	   $stmt->execute();
   
	   return $stmt;
   }


	public function readFilter($from_record_num, $records_per_page, $order_by) {

		$order = "";
		if($order_by== "new")
			$order = "ORDER BY p.created DESC";
		else if($order_by == "price_asc")
			$order = "ORDER BY p.price DESC";
		else if($order_by == "price_desc")
			$order = "ORDER BY p.price DESC";
		else if ($order_by == "random")
            $order = "ORDER BY RAND()";
        $query="";
        if(empty($this->category_id)){
        	 $query = "SELECT
                        c.name as category_name, p.id, p.name, p.description,  p.quantity, p.image_name, p.category_id, p.created
                    FROM
                        " . $this->table_name . " p
                        LEFT JOIN
						category c
                                ON p.category_id = c.id
                    $order
                    LIMIT ?, ?";
        }
        else{
        	 $query = "SELECT
                        c.name as category_name, p.id, p.name, p.description,  p.quantity, p.image_name, p.category_id, p.created
                    FROM
                        " . $this->table_name . " p
                        LEFT JOIN
						category c
                                ON p.category_id = $this->category_id
                    $order
                    LIMIT ?, ?";
        }


        $stmt = $this->conn->prepare($query);
    
        // bind variable values
        $stmt->bindParam(1, $from_record_num);
        $stmt->bindParam(2, $records_per_page);
    
        // execute query
        $stmt->execute();
    
        // return values from database
        return $stmt;
	}

	public function readAll($category_id, $order_by){

		$order = "";
		if($order_by== "new")
			$order = "ORDER BY created DESC";
		else if($order_by == "desc")
			$order = "ORDER BY price DESC";
		else if ($order_by == "asc")
            $order = "ORDER BY price ASC";
	    $query="CALL getAllCategory(:category, :order_by, @return_text)";
		// prepare query statement
	    $stmt = $this->conn->prepare( $query );
	    // bind variable values
	    $stmt->bindParam(':category', $category_id);
	    $stmt->bindParam(':order_by', $order_by);
	    // execute query
		$stmt->execute();
		// $stmt->closeCursor();

		// $row =  $this->conn->query("SELECT @return_text AS return_text")->fetch(PDO::FETCH_ASSOC);
		// var_dump($row);
	    // return values from database
	    return $stmt;
	}


	public function adminReadPaging($from_record_num, $records_per_page){

	    // select query
	    $query="SELECT
	              	s.storeName as seller_name, d.id as provide_id, d.price as price, d.quantity as quantity, c.name as category_name, p.id, p.name, p.description, p.introduction, p.image_name, p.category_id, p.created
	            FROM
	                " . $this->table_name . " p
	           INNER JOIN
			   category c ON p.category_id = c.id
		   		INNER JOIN
			   provide d ON p.id = d.product_id
		   		INNER JOIN
			   seller s ON s.id = d.seller_id
					ORDER BY created DESC
				LIMIT ?, ?";
		
		// prepare query statement
	    $stmt = $this->conn->prepare( $query );
	 
	    // bind variable values
	    $stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
	    $stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);
	 
	    // execute query
	    $stmt->execute();
	 
	    // return values from database
	    return $stmt;
	}



	public function readIndex($from_record_num, $records_per_page, $order_by) {

		$order = "";
		if($order_by== "new")
			$order = "ORDER BY p.created DESC";
		else if($order_by == "discount_desc")
			$order = "ORDER BY p.discount DESC";
		else if ($order_by == "best_selling")
            $order = "ORDER BY p.quantity ASC";
		$query = "SELECT
				c.name as category_name, p.id, p.name, p.author, p.description, p.introduction,  p.quantity, p.discount, p.image_name, p.category_id
			FROM
			" . $this->table_name . " p
                    LEFT JOIN
                        category c
                            ON p.category_id = c.id
			$order
			LIMIT ?, ?";


        $stmt = $this->conn->prepare($query);
    
        // bind variable values
        $stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
        $stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);
    
        // execute query
        $stmt->execute();
    
		// return values from database
		return $stmt;

		
	}

	public function readRecommend($tag) {
		$order = "";
		if($tag== "1")
			$order = "ORDER BY p.price ASC";
		else if($tag == "2")
			$order = "ORDER BY p.discount DESC";
		else if ($tag == "3")
			$order = "ORDER BY p.quantity ASC";
		else
			$order = "ORDER BY p.quantity DESC";


		$query = "SELECT
					c.name as category_name, p.id, p.name, p.author, p.description, p.introduction,  p.quantity, p.discount, p.image_name, p.category_id
				FROM
					" . $this->table_name . " p
					LEFT JOIN
						category c
							ON p.category_id = c.id
				$order
				LIMIT 0, 20";


        $stmt = $this->conn->prepare($query);
        // execute query
        $stmt->execute();
		return $stmt;
	}

	
	public function readOrderbyId($user_id) {
		$order = "";

		$query = "SELECT orders.id, order_items.name, order_items.item_price ,order_items.modified
				FROM orders 
				LEFT JOIN order_items
				ON orders.id = order_items.order_id AND orders.user_id=" .$user_id;

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
		return $stmt;
	}


	public function count() {
		$query = "";
		if (empty($this->category_id)) {
			$query = "SELECT COUNT(*) FROM " . $this->table_name . "";
		} else {
			$query = "SELECT COUNT(*) FROM " . $this->table_name . " WHERE category_id=$this->category_id";
		}
		$stmt = $this->conn->prepare($query);

		$stmt->execute();
		return $stmt->fetchColumn();
	}
}
?>