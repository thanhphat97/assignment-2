<?php

class User {

	public $conn;
	public $table_name = "user";

	// object properties
	public $id;
	public $email;
	public $phone;
	public $password;
	public $new_password;
	public $fullname;
	public $birthdate;
	public $role;
	public $gender;
	public $created;

	// constructor with $db as database connection
	public function __construct($db) {
		$this->conn = $db;
	}

	public function create() {
		$query = "INSERT INTO
		" . $this->table_name . "
		SET
		email=:email, phone=:phone, fullname=:fullname, birthdate=:birthdate, role=:role, password=:password, gender=:gender";

		// prepare query
		$stmt = $this->conn->prepare($query);

		$this->email = htmlspecialchars(strip_tags($this->email));
		$this->phone = htmlspecialchars(strip_tags($this->phone));
		$this->fullname = htmlspecialchars(strip_tags($this->fullname));
		$this->birthdate = htmlspecialchars($this->birthdate);
		$this->role = htmlspecialchars(strip_tags($this->role));
		$this->password = htmlspecialchars(strip_tags($this->password));		
		$this->gender = htmlspecialchars(strip_tags($this->gender));

		$stmt->bindValue(':email', $this->email);
		$stmt->bindValue(':phone', $this->phone);
		$stmt->bindValue(':password', $this->password);
		$stmt->bindValue(':fullname', $this->fullname);
		$stmt->bindValue(':birthdate', $this->birthdate);
		$stmt->bindValue(':role', 1);
		$stmt->bindValue(':gender', $this->gender);

		// execute query
		if ($stmt->execute()) {
			$this->id = $this->conn->lastInsertId();
			return true;
		}
		return false;

	}

	public function readAll() {
		$query = "SELECT
				id, email, phone, password, fullname, birthdate, role, gender
				FROM
				" . $this->table_name . "
				ORDER BY
				created";

		$stmt = $this->conn->prepare($query);

		// execute query
		$stmt->execute();

		return $stmt;
	}

	public function readOne() {
		$query = "SELECT
		id , email, phone, fullname, birthdate, role, gender, created
		FROM
		" . $this->table_name . "
		WHERE
		id = ?
		LIMIT
		0,1";

		$stmt = $this->conn->prepare($query);
		$id=htmlspecialchars(strip_tags($this->id));
    
		$stmt->bindParam(1, $id);
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		$this->email = $row['email'];
		$this->phone = $row['phone'];
		$this->fullname = $row['fullname'];
		$this->birthdate = $row['birthdate'];
		$this->role = $row['role'];
		$this->gender = $row['gender'];
		$this->created = $row['created'];
	}

	public function readbyEmail() {
		$query = "SELECT
		id , email, phone, fullname, birthdate, role, gender, created
		FROM
		" . $this->table_name . "
		WHERE
		email = ?
		LIMIT
		0,1";

		$stmt = $this->conn->prepare($query);
		$email=htmlspecialchars(strip_tags($this->email));
    
		$stmt->bindParam(1, $email);
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		$this->id = $row['id'];
		$this->phone = $row['phone'];
		$this->fullname = $row['fullname'];
		$this->birthdate = $row['birthdate'];
		$this->role = $row['role'];
		$this->gender = $row['gender'];
		$this->created = $row['created'];
	}

	public function findEmail() {
		$query = "SELECT
		id , email, phone, fullname, birthdate, role, gender, created
		FROM
		" . $this->table_name . "
		WHERE
		email = ?
		LIMIT
		0,1";

		$stmt = $this->conn->prepare($query);
		$this->id=htmlspecialchars(strip_tags($this->email));
    
		$stmt->bindParam(1, $this->email);
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		$this->id = $row['id'];
		$this->phone = $row['phone'];
		$this->fullname = $row['fullname'];
		$this->birthdate = $row['birthdate'];
		$this->role = $row['role'];
		$this->gender = $row['gender'];
		$this->created = $row['created'];
	}

	public function update() {
		$query = "UPDATE
		" . $this->table_name . "
		SET
			phone= :phone, fullname= :fullname
		WHERE
		email = :email";

		$stmt = $this->conn->prepare($query);

		$phone = htmlspecialchars(strip_tags($this->phone));
		$fullname = htmlspecialchars(strip_tags($this->fullname));
		$email = htmlspecialchars(strip_tags($this->email));

		$stmt->bindParam(':phone', $phone);
		$stmt->bindParam(':fullname', $fullname);
		$stmt->bindParam(':email', $email);

		if ($stmt->execute()) {
			return true;
		}

		return false;
	}


	public function delete() {
		$query = "DELETE FROM " . $this->table_name . " WHERE id = ?";

		  // prepare query statement
		$stmt = $this->conn->prepare($query);

		$id=htmlspecialchars(strip_tags($this->id));
		$stmt->bindParam(1, $id);

		if ($stmt->execute()) {
			return true;
		}

		return false;

	}

	public function updatePass() {
		$query = "UPDATE
		" . $this->table_name . "
		SET
			password = :password
		WHERE
			id = :id";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// sanitize
		$this->new_password=htmlspecialchars(strip_tags($this->new_password));

		$password = sha1($this->email . $this->new_password);

		// bind new values
		$stmt->bindParam(':password', $password);
		$stmt->bindParam(':id', $this->id);

		// execute the query
		if($stmt->execute()){
			return true;
		}
		return false;
}

	public function updateRole($id, $role) {
		$query = "UPDATE
		" . $this->table_name . "
		SET
		role= :role, 
		WHERE
		id = :id";

		$stmt = $this->conn->prepare($query); 
        $role=htmlspecialchars(strip_tags($this->role));
      	$id=htmlspecialchars(strip_tags($this->id));
        
        $stmt->bindParam(':role', $role);
        $stmt->bindParam(':id', $id);
        if($stmt->execute()){
            return true;
        }
    
        return false;
	}

	public function validate() {
	   $query = "SELECT
				   	id , email, phone, fullname, birthdate, role, gender, password
			   FROM
				   " . $this->table_name . "
			   WHERE
				   email = ?";
   
	   $stmt = $this->conn->prepare( $query );
   
	   $stmt->bindParam(1, $this->email, PDO::PARAM_STR);
   
	   $stmt->execute();
	
		return $stmt;
	}

	public function checkDuplicate() {
		$query = "SELECT
					COUNT(*)
				FROM
					" . $this->table_name . "
				WHERE
					email = ? OR phone = ?
				LIMIT
					0,1";
	
		$stmt = $this->conn->prepare( $query );
	
		$stmt->bindParam(1, $this->email);
		$stmt->bindParam(2, $this->phone);
	
		$stmt->execute();
		$number = $stmt->fetchColumn();
		if($number > 0)
			return true;
		return false;
	 }

	public function count() {
		$query = "";
			$query = "SELECT COUNT(*) FROM " . $this->table_name . "";
		$stmt = $this->conn->prepare($query);

		$stmt->execute();
		return $stmt->fetchColumn();
	}
	 
}

?>