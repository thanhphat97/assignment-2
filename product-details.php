<?php
include 'header.php';
$productID = isset($_GET['product_id']) ? $_GET['product_id'] : '';
?>
<main>
    <div class="page-breadcrumb">
        <div class="container">
            <ul id="breadcrumb">
                <li><a href="#">Trang chủ</a></li>
                <li><a class="active" href="#"></a></li>
            </ul>
        </div>
    </div>
    <section class="detailsection mt-4" id="single-product" product-id="<?php echo $productID ?>">
    <div class="container">
        <div class="details-product row justify-content-center">
            <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 mt-2">
                <div class="product-thumbnai">
                    <div id="myGallery"> </div>
                </div>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12">
                <div class="product-info" data-productID="">
                    <h1 class="product-name"></h1>
                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-sm-12">
                            <div class="seller-product">Nhà cung cấp: 
                                    <span class=""> 
                                            <select class="custom-select select-sort cost-sort" id="seller_select"  style="width:  67% !important">
                                                <!-- <option value='new'>Chọn nhà cung cấp</option>
                                                <option value='asc'>Giá: Thấp - Cao</option>
                                                <option value='desc'>Giá: Cao - Thấp</option> -->
                                            </select>
                                    </span>
                            </div>
                            <div class="price-product">Giá: <span class="style-price pl-1"
                                    style="color: red; font-size: 1.5rem; font-weight: 600;"></span></div>
                            <div class="amount">
                                <div class="input-number">
                                    <span class="mr-2">Số lượng:</span>
                                    <span class="minusP">-</span>
                                    <input type="text" maxlength="10" value="1" />
                                    <span class="plusP">+</span>
                                </div>
                            </div>
                            <div class="product-gift mb-3">
                                <p class="mb-2"><strong>Khuyến mãi &amp; Ưu đãi tại TikiBK:</strong>
                                </p>
                                <ul>
                                    <li>Miễn phí giao hàng cho đơn hàng từ 250k</li>
                                    <li>Hoàn tiền 15% cho mọi chi tiêu với TikiCARD</li>
                                    <li>Miễn phí vận chuyển tiêu chuẩn 100% cho đơn hàng từ 250k được phân phối bởi Tiki Trading.
                                    </li>
                                </ul>
                            </div>
                            <button class="buynowbutton btn btn-block mt-3">CHỌN MUA</button>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12">
                            <div class="book-info mt-3">
                                <ul>
                                <li>Nguyên seal, mới 100%, chưa Active</li><li>Miễn phí giao hàng toàn quốc</li><li>Hệ điều hành: IOS 13</li><li>Bộ nhớ: 32GB, RAM: 3 GB</li><li>Camera chính: 8MP</li><li>Camera phụ: 1.2MP</li><li>Pin: 30.2 Wh</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="detail-description justify-content-center">
            <div class="tab-product">
                <button class="tablinks-product active" onclick="openCity(event, 'introduce')"
                    id="defaultOpen">GIỚI THIỆU</button>
                <button class="tablinks-product" onclick="openCity(event, 'evaluate')">ĐÁNH GIÁ</button>
            </div>
            <div id="introduce" class="tabcontent" style="display:block">
            </div>

            <div id="evaluate" class="tabcontent">
                <p>Sử dụng facebook comment</p>
            </div>
        </div>
    </div>
</section>
<script src="./controller/products/product-detail.js"></script>
<script src="./controller/cart/addItem.js"></script>
<?php
include 'footer.php';
?>

