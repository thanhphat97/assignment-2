<?php
require_once('./util/main.php');
include 'header.php';
?>

<section class="account-page">

<ul class="nav nav-tabs nav-tabs2" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link  active" href="#info">Thông tin tài khoản</a>
    </li>
    <li class="nav-item">
        <a class="nav-link " href="#order">Danh sách đơn hàng</a>
    </li>
</ul>
<div class="tab-content">
    <div id="info" class="tab-pane account-info active">        
        <div class="module-title">
            <div class="row">
                <div class="col-25">
                </div>
                <div class="col-75">
                    Thông tin tài khoản
                </div>
            </div>
        </div>
        <form action="/action_page.php">
            <div class="row">
                <div class="col-25">
                    <label for="fname">Họ tên</label>
                </div>
                <div class="col-75">
                    <input type="text" id="p-fullname" name="" value="<?php echo isset($_SESSION['user']) ? $_SESSION['user']['fullname'] : ''?>">
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="lname">Số điện thoại</label>
                </div>
                <div class="col-75">
                    <input type="text" id="p-phone" name="" value="<?php echo isset($_SESSION['user']) ? $_SESSION['user']['phone'] : ''?>">
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="lname">Email</label>
                </div>
                <div class="col-75">
                    <input type="text" id="p-email" name="" placeholder="" value= "<?php echo isset($_SESSION['user']) ? $_SESSION['user']['email'] : ''?>" disabled>
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="lname">Giới tính</label>
                </div>
                <div class="col-75">
                    <input type="text" name="" placeholder="" value= "<?php echo isset($_SESSION['user']) ? ($_SESSION['user']['gender'] == 1 ? 'Nam' : 'Nữ') : ''?>" disabled>
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="">Ngày sinh</label>
                </div>
                <div class="col-75">
                    <input type="text" name="" placeholder="" value= "<?php echo isset($_SESSION['user']) ? $_SESSION['user']['birthdate'] : ''?>" disabled>
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for=""></label>
                </div>
                <div class="col-75">
                    <label class="checkbox-inline"><input id="checkbox1" type="checkbox" value="">Thay đổi mật khẩu</label>
                </div>
            </div>
            <div class="row frm-password" style="display: none;">
                <div class="col-25">
                    <label for="">Mật khẩu cũ</label>
                </div>
                <div class="col-75">
                    <input type="password" id="p-old-pw" name="" placeholder="" >
                    <div class="err_msg err_old_password" style="font-size:small; color: red"></div>
                </div>
            </div>
            <div class="row frm-password" style="display: none;">
                <div class="col-25">
                    <label for="">Mật khẩu mới</label>
                    <div class="err_msg err_password" style="font-size:small; color: red"></div>
                </div>
                <div class="col-75">
                    <input type="password" id="p-new-pw" name="" placeholder="" >
                </div>
            </div>
            <div class="row frm-password" style="display: none;">
                <div class="col-25">
                    <label for="">Xác nhận mật khẩu mới</label>
                    <div class="err_msg err_confrm_password" style="font-size:small; color: red"></div>
                </div>
                <div class="col-75">
                    <input type="password" id="p-confrm-pw" name="" placeholder="" >
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for=""></label>
                </div>
                <div class="col-75">
                    <input class="float-left btn-submit" id="submit-profile" type="button" value="Cập nhật" data-id="<?php echo isset($_SESSION['user']['id']) ? $_SESSION['user']['id'] : '' ?>">
                    <div class="err_msg err_update" style="font-size:small; color: red"></div>
                </div>
            </div>
        </form>
    </div>

    <div id="order" class="tab-pane account-order">
        <div class="row">
            <table class="table table-order table-striped">
                <thead>
                    <tr>
                        <th>Mã đơn hàng</th>
                        <th class="d-none">Ngày mua</th>
                        <th>Sản phẩm</th>
                        <th>Đơn giá</th>
                        <th>Ngày tạo</th>
                    </tr>
                </thead>
                <tbody id="myorder">
                </tbody>
            </table>
        </div>
    </div>
</div>
</section>
<script src="./controller/user/showOrder.js"></script>

<style>
    
    .account-page {
        background: #fff;
        max-width: 960px;
        margin: 20px auto;
        padding: 20px;
    }
    .nav-tabs {
        margin: 0;
        padding: 0;
        border-bottom: 1px solid #f2f2f2;
        display: block;
    }
    .nav {
        display: flex;
        flex-wrap: wrap;
        padding-left: 0;
        margin-bottom: 0;
        list-style: none;
    }

    .nav-tabs .nav-item {
        display: inline-block;
        margin-bottom: -1px;
    }

    .nav-tabs .nav-item .nav-link.active {
        color: #333;
        border-bottom-color: #f5a623;
    }

    .nav-tabs2 .nav-item .nav-link.active {
        border: 1px solid #f2f2f2;
        border-top: 3px solid #f5a623;
        background: #fff;
        border-bottom-color: #fff;
    }

    .nav-tabs2 .nav-item .nav-link {
        text-transform: uppercase;
        border-radius: 0;
        padding: 10px 20px;
        line-height: 20px;
        margin-bottom: -1px;
    }
    .nav-tabs .nav-item .nav-link {
        border: none;
        border-bottom: 2px solid transparent;
       /* font-size: .875rem;*/
        line-height: 20px;
        text-transform: uppercase;
        color: #999;
    }

    .nav-tabs .nav-item .nav-link:hover {
        color: #f5a623;
    }

    .account-page .account-form {
        max-width: 70%;
    }

    .account-page .account-info .module-title {
        font-size: 1.125rem;
        color: #ccc;
        margin-bottom: 10px;
    }

    .table-order{
        width: 100%;
        margin-top: 20px;
    }

    .table-order tr {
        position: relative;
    }

    .table-order th {
       /* font-size: .875rem;*/
        font-weight: 400;
        padding: 10px 15px;
        background: #dadada;
    }

    .table-order td {
       /* font-size: .875rem;*/
        font-weight: 400;
        padding: 10px 15px;
    }

    #checkbox1{
        width: auto;
    }

    input[type=text], select, textarea {
      width: 100%;
      padding: 5px;
      border: 1px solid #ccc;
      border-radius: 4px;
      resize: vertical;
    }
    input[type=password] {
      width: 100%;
      padding: 5px;
      border: 1px solid #ccc;
      border-radius: 4px;
      resize: vertical;
    }

    input:disabled{
        background-color: #e9ecef;
    }

    label {
      padding: 5px 12px 0px 0;
      display: inline-block;
    }

    input.btn-submit {
      background-color: #f5a623;
      color: white;
      padding: 10px 20px;
      border: none;
      border-radius: 4px;
      cursor: pointer;
      float: right;
      text-align: center;
    }

    input.btn-submit:hover {
      background-color: #fd8205;
    }

    .col-25 {
      float: left;
      width: 25%;
      margin-top: 6px;
    }

    .col-75 {
      float: left;
      width: 75%;
      margin-top: 6px;
    }

    /* Clear floats after the columns */
    .row:after {
      content: "";
      display: table;
      clear: both;
    }

    /* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
    @media screen and (max-width: 600px) {
      .col-25, .col-75, input.btn-submit {
        width: 100%;
        margin-top: 0;
      }
      .submit{
        margin-top: 10px;
      }
    }
</style>

<script src="./controller/user/edit-profile.js"></script>

<?php
include 'footer.php';
?>
