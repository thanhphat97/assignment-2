// Both cases also saved the previous state
// When the user clicked outside, modal will close
var fadeTime = 300;
var shippingCharge = 15.000;

window.onclick = function (e) {
    if (e.target == $('#modal')[0]) {
        $('#modal').hide();
    }
    if (e.target == $('.forgot-password')[0]) {
        $('.forgot-password').hide();
    }

}
// When the user press ESC, modal will close
$(document).keydown(function (e) {
    if (e.keyCode == 27) {
        $('#modal').hide();
        $('.forgot-password').hide();
    }
});

function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks-product");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

$(document).ready(function () {
    function activeTab(obj) {
        $('.tablinks').removeClass('active');
        $(obj).addClass('active');
        var id = $(obj).find('a').attr('href');
        $('.tab-item').hide();
        $('div').filter(id).show();
    }
    // Always active tab default
    activeTab($('.tab .tablinks:first-child'));

    $('#login-user').click(function () {
        $('#modal').css({
            "display": "block"
        });
    });

    $('#create-user').click(function () {
        $('#modal').css({
            "display": "block"
        });
        activeTab($('.tab .tablinks:last-child'));
    });
    // Switch tab
    $('.tablinks').click(function () {
        activeTab(this);
        return false;
    });

    $('.forgetpass').click(function () {
        $('#modal').hide();
        $('.forgot-password').css({
            "display": "block"
        });
    });


    $('.search-mobile-btn').click(function () {
        $('#search-form-mobile').slideToggle(200);
    });

    // hamburger menu 
    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });

    $('#dismiss, .overlay').on('click', function () {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
    });
    // Datepicker
    $('.datepicker').datepicker({
        uiLibrary: 'bootstrap4'
    });
    // Input counter item cart
    $('.minusP').on("click", function () {
        var $input = $(this).parent().find('input');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
    });
    $('.plusP').on("click", function () {
        var $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
    });
    
    $('.step-title').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active').siblings('.step-content').slideToggle();
        $(this).parent().siblings().find('.step-title').removeClass('active').siblings('.step-content').slideUp();
    });






});









