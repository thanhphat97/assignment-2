-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 17, 2020 at 07:42 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tikidatabase`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `AddProducts` (IN `NAME` VARCHAR(255), IN `DESCRIPTION` TEXT, IN `INTRODUCTION` LONGTEXT, IN `CATEGORY_ID` INT, IN `IMAGE_NAME` VARCHAR(255), IN `SELLER_ID` INT, IN `QUANTITY` INT, IN `PRICE` DOUBLE, OUT `RETURN_TEXT` VARCHAR(225))  BEGIN
	IF(CATEGORY_ID='') THEN
        SET RETURN_TEXT = 'Fill out the CategoryID';
    ELSEIF(SELLER_ID='') THEN
        SET RETURN_TEXT = 'Fill out the SellerID';
    ELSEIF(QUANTITY <0) THEN
        SET RETURN_TEXT = 'Quantity must be greater than zero';
    ELSEIF(PRICE < 0) THEN
        SET RETURN_TEXT = 'Price must be greater than zero';
	ELSEIF (NOT EXISTS(SELECT * FROM category WHERE id=CATEGORY_ID)) THEN
            SET RETURN_TEXT = 'Category not founded';
    ELSEIF(NAME='') then
        SET RETURN_TEXT = 'Fill out the Name product';
    ELSE
    	INSERT INTO product(name,category_id,description,introduction,image_name) VALUES(NAME,CATEGORY_ID, DESCRIPTION, INTRODUCTION, IMAGE_NAME);
    INSERT INTO provide(seller_id, product_id, quantity, price) 
VALUES(SELLER_ID, LAST_INSERT_ID(), QUANTITY, PRICE);
        SET RETURN_TEXT = 'Insert success';
    END IF;
        SELECT RETURN_TEXT;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteProduct` (IN `ID_PRODUCT` INT, OUT `RETURN_TEXT` VARCHAR(225))  BEGIN
IF (NOT EXISTS (SELECT * FROM product WHERE id=ID_PRODUCT)) THEN
  		SET RETURN_TEXT = 'Product not found';
ELSE
	DELETE FROM product WHERE id = ID_PRODUCT;
       SET RETURN_TEXT = 'Deleted success';
END IF;
	  SELECT RETURN_TEXT;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getAllCategory` (IN `CATEGORY_ID` INT, OUT `RETURN_TEXT` VARCHAR(225))  BEGIN
	IF(CATEGORY_ID='') THEN
   		SIGNAL SQLSTATE '45000'
  		SET MESSAGE_TEXT = 'Fill out the CategoryID';
        SET RETURN_TEXT='Fill out the CategoryID';
	ELSEIF (NOT EXISTS(SELECT * FROM category WHERE id=CATEGORY_ID)) THEN
    	SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = 'Category not founded';
        SET RETURN_TEXT='Category not founded';
    ELSE
    	
    	SELECT s.storeName as seller_name, MIN(d.promotion_price) as promotion_price, MIN(d.price) as min_price, c.name as category_name, c.parentID, p.id, p.name, p.status, p.description, p.introduction, p.quantity, p.image_name, p.category_id, p.created
	            FROM
	                product p
	            INNER JOIN
					category c ON p.category_id = c.id
				INNER JOIN
					provide d ON p.id = d.product_id
				INNER JOIN
					seller s ON s.id = d.seller_id
					WHERE c.id = CATEGORY_ID or c.parentID = CATEGORY_ID
					GROUP BY p.id;
       SET RETURN_TEXT='Success query';
    END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `searchProduct` (IN `SEARCH_TEXT` VARCHAR(500))  BEGIN
SELECT
    c.name AS category_name,
    p.*,
    MIN(d.price) AS min_price
FROM
    product p
LEFT JOIN category c 
    ON (p.category_id = c.id OR p.category_id = c.parentID)
LEFT JOIN provide d 
	ON d.product_id = p.id
WHERE
    p.name LIKE SEARCH_TEXT OR p.description LIKE SEARCH_TEXT OR p.introduction LIKE SEARCH_TEXT OR c.name LIKE SEARCH_TEXT
GROUP BY
    p.id
ORDER BY
    p.created DESC
LIMIT 6;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `updateProduct` (IN `NAME` VARCHAR(225), IN `DESCRIPTION` TEXT, IN `INTRODUCTION` LONGTEXT, IN `CATEGORY_ID` INT, IN `IMAGE_NAME` VARCHAR(225), IN `SELLER_ID` INT, IN `QUANTITY` INT, IN `PRICE` INT, IN `ID_PRODUCT` INT, OUT `RETURN_TEXT` VARCHAR(225))  NO SQL
BEGIN
	IF (NOT EXISTS(SELECT * FROM product WHERE id=ID_PRODUCT)) THEN
  		SET RETURN_TEXT = 'Product not found';
	ELSEIF(CATEGORY_ID='') THEN
  		SET RETURN_TEXT = 'Fill out the CategoryID';
    ELSEIF(SELLER_ID='') THEN
  		SET RETURN_TEXT = 'Fill out the SellerID';
    ELSEIF(QUANTITY <0) THEN
  		SET RETURN_TEXT = 'Quantity must be greater than zero';
    ELSEIF(PRICE < 0) THEN
  		SET RETURN_TEXT = 'Price must be greater than zero';
	ELSEIF (NOT EXISTS(SELECT * FROM category WHERE id=CATEGORY_ID)) THEN
		SET RETURN_TEXT = 'Category not founded';
    ELSEIF(NAME='') then
  		SET RETURN_TEXT = 'Fill out the Name product';
    ELSE
    	UPDATE product
        SET name=NAME,category_id=CATEGORY_ID,description=DESCRIPTION,introduction=INTRODUCTION,image_name=IMAGE_NAME
        WHERE id = ID_PRODUCT;
    UPDATE provide SET quantity= QUANTITY, price=PRICE
		WHERE seller_id= SELLER_ID AND product_id = ID_PRODUCT;
         SET RETURN_TEXT = 'Updated success';
    END IF;
    SELECT RETURN_TEXT;

END$$

--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `tinhTuoiNhanVien` (`ma_nhan_vien` INT) RETURNS VARCHAR(255) CHARSET utf8 COLLATE utf8_unicode_ci NO SQL
BEGIN
DECLARE tuoi INT;
SELECT ROUND(DATEDIFF(CURDATE(),NV.ngaySinhNV) / 365, 0) INTO tuoi FROM nhanvien NV WHERE NV.maNhanVien=ma_nhan_vien;
RETURN CONCAT('Nhân viên ',ma_nhan_vien,':',tuoi,' tuổi');
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `benthubagh`
--

CREATE TABLE `benthubagh` (
  `maBenThuBa` int(15) NOT NULL,
  `tenCongTy` varchar(50) NOT NULL,
  `diaChiBenThuBa` varchar(150) NOT NULL,
  `sdtBenThuBa` varchar(12) DEFAULT NULL,
  `maVanChuyenBTB` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cart_item`
--

CREATE TABLE `cart_item` (
  `id` int(11) NOT NULL,
  `provide_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `parentID` int(11) NOT NULL DEFAULT 0,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `parentID`, `created`, `modified`) VALUES
(1, 'Điện thoại - Máy tính bảng', 0, '2020-07-08 22:55:39', '2020-07-08 22:55:39'),
(2, 'Điện tử - Điện lạnh', 0, '2020-07-08 23:01:12', '2020-07-08 23:01:12'),
(3, 'Phụ kiện - Thiết bị số', 0, '2020-07-08 23:01:12', '2020-07-08 23:01:12'),
(4, 'Laptop - Thiết bị IT', 0, '2020-07-08 23:01:12', '2020-07-08 23:01:12'),
(5, 'Máy ảnh - Quay phim', 0, '2020-07-08 23:01:12', '2020-07-08 23:01:12'),
(6, 'Điện gia dụng', 0, '2020-07-08 23:01:12', '2020-07-08 23:01:12'),
(7, 'Nhà cửa đời sống', 0, '2020-07-08 23:01:12', '2020-07-08 23:01:12'),
(8, 'Hàng tiêu dùng - Thực phẩm', 0, '2020-07-08 23:01:12', '2020-07-08 23:01:12'),
(9, 'Đồ chơi, mẹ và bé ', 0, '2020-07-08 23:01:12', '2020-07-08 23:01:12'),
(10, 'Làm đẹp - Sức khỏe', 0, '2020-07-08 23:01:12', '2020-07-08 23:01:12'),
(11, 'Thời trang - Phụ kiện', 0, '2020-07-08 23:01:12', '2020-07-08 23:01:12'),
(12, 'Apple', 1, '2020-07-08 23:04:02', '2020-07-08 23:04:02'),
(13, 'Samsung', 1, '2020-07-08 23:04:02', '2020-07-08 23:04:02'),
(14, 'Âm thanh - Phụ kiện', 2, '2020-07-08 23:05:53', '2020-07-08 23:06:38'),
(15, 'Máy lạnh - Điều hòa', 2, '2020-07-08 23:05:53', '2020-07-08 23:05:53'),
(16, 'Thể thao - Dã ngoại', 0, '2020-07-08 23:12:48', '2020-07-08 23:12:48'),
(17, 'Xe máy - Ô tô', 0, '2020-07-08 23:12:48', '2020-07-08 23:12:48');

-- --------------------------------------------------------

--
-- Table structure for table `donvivanchuyen`
--

CREATE TABLE `donvivanchuyen` (
  `maDonViVanChuyen` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hoadon`
--

CREATE TABLE `hoadon` (
  `soHoaDon` char(9) NOT NULL,
  `ngayXuatHoaDon` datetime DEFAULT NULL,
  `maDonHang_HD` int(11) NOT NULL,
  `maNhanVienBanHang_HD` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hoidap`
--

CREATE TABLE `hoidap` (
  `maKhachHang_HD` int(15) NOT NULL,
  `maNhanVienCSKH_HD` int(15) NOT NULL,
  `thoiGianHoi` datetime NOT NULL,
  `cauHoi` varchar(100) NOT NULL,
  `traLoi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kho`
--

CREATE TABLE `kho` (
  `maKho` int(15) NOT NULL,
  `tenKho` varchar(12) DEFAULT NULL,
  `diaChiKho` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kienhang`
--

CREATE TABLE `kienhang` (
  `maKienHang` int(5) NOT NULL,
  `khoiLuong` decimal(5,1) NOT NULL,
  `kichThuoc` varchar(9) DEFAULT NULL,
  `loaiKienHang` varchar(15) DEFAULT NULL,
  `maDonHang_KH` int(11) NOT NULL,
  `maKho_KH` int(15) NOT NULL,
  `maVanChuyen_KH` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `nhanvien`
--

CREATE TABLE `nhanvien` (
  `maNhanVien` int(11) NOT NULL,
  `hoNV` varchar(12) DEFAULT NULL,
  `tenLotNV` varchar(15) DEFAULT NULL,
  `tenNV` varchar(20) DEFAULT NULL,
  `sdtNV` varchar(15) DEFAULT NULL,
  `diaChiNV` varchar(150) DEFAULT NULL,
  `ngaySinhNV` date DEFAULT NULL,
  `gioiTinhNV` varchar(5) DEFAULT NULL
) ;

-- --------------------------------------------------------

--
-- Table structure for table `nhanvienbanhang`
--

CREATE TABLE `nhanvienbanhang` (
  `maNhanVienBanHang` int(11) NOT NULL,
  `soNamKinhNghiem` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `nhanviencskh`
--

CREATE TABLE `nhanviencskh` (
  `maNhanVienCSKH` int(15) NOT NULL,
  `bangCap` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `nhanviengiaohang`
--

CREATE TABLE `nhanviengiaohang` (
  `maNhanVienGiaoHang` int(11) NOT NULL,
  `giayPhepLaiXe` varchar(15) DEFAULT NULL,
  `maVanChuyen_NVGH` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `nhanvienkho`
--

CREATE TABLE `nhanvienkho` (
  `maNhanVienKho` int(15) NOT NULL,
  `giayKhamSucKhoe` varchar(20) DEFAULT NULL,
  `maNhanVienQuanLy` int(15) NOT NULL,
  `maKho_NVK` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment` varchar(512) NOT NULL,
  `shipping_address` varchar(512) NOT NULL,
  `total` decimal(10,3) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `item_price` decimal(10,3) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(512) NOT NULL,
  `category_id` int(11) NOT NULL,
  `description` text DEFAULT NULL,
  `introduction` longtext DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `status` varchar(200) DEFAULT NULL,
  `image_name` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `category_id`, `description`, `introduction`, `quantity`, `status`, `image_name`, `created`, `modified`) VALUES
(1, 'Điện Thoại iPhone SE 64GB ( 2020) - Hàng Chính Hãng', 12, NULL, 'Mang tình yêu trở lại với thiết kế sang trọng\r\nĐiện Thoại iPhone SE 64GB ( 2020) có một thiết kế quen thuộc, thiết kế đã dành được rất nhiều tình cảm của người dùng trong những năm qua. Kiểu dáng nhỏ gọn, bo tròn các cạnh, thân máy siêu mỏng tinh tế và hoàn thiện từ chất liệu nhôm kết hợp kính cao cấp, iPhone SE 2020 mang đến cảm hứng bất tận cho người dùng. Chiếc iPhone nhỏ gọn rất dễ dàng cầm nắm, thao tác hay cho vào túi mang đi bất cứ đâu nhưng vẫn toát lên vẻ sang trọng đậm chất Apple, iPhone SE 2020 cho bạn cảm giác thân thuộc, thoải mái hơn bao giờ hết', 0, '1', 'iphone8.jpg', '2020-07-09 06:33:46', '2020-07-09 09:39:44'),
(2, 'iPad 10.2 Inch WiFi 32GB New 2019 - Hàng Nhập Khẩu Chính Hãng', 12, NULL, NULL, 0, '1', 'ipadgen7.jpg', '2020-07-09 07:54:45', '2020-07-09 09:39:34'),
(3, 'Đồng Hồ Thông Minh Apple Watch Series 5', 12, NULL, NULL, 0, '1', 'applewatch.jpg', '2020-07-09 09:00:12', '2020-07-09 09:40:16'),
(4, 'Tai Nghe Bluetooth Apple AirPods Pro True Wireless', 12, NULL, NULL, 0, '1', 'airpod.jpg', '2020-07-09 09:05:21', '2020-07-09 09:40:23'),
(5, 'Apple Macbook Pro 2020 - 13 Inchs', 12, NULL, NULL, 0, NULL, 'macbook.jpg', '2020-07-09 09:11:29', '2020-07-09 09:11:29'),
(6, 'Điện Thoại Samsung Galaxy S20 Ultra', 13, NULL, NULL, 0, NULL, 'samsungdt.jpg', '2020-07-09 09:15:53', '2020-07-09 09:15:53'),
(7, 'Đồng Hồ Samsung Galaxy Watch 46mm', 13, NULL, NULL, 0, NULL, 'samsungwatch.jpg', '2020-07-09 09:19:08', '2020-07-09 09:19:08'),
(8, 'Tai Nghe True Wireless Samsung Galaxy Buds+ ', 13, NULL, NULL, 0, NULL, 'samsungbud.jpg', '2020-07-09 09:22:19', '2020-07-09 09:22:19'),
(9, 'Máy tính bảng Samsung Galaxy Tab S6', 13, NULL, NULL, 0, NULL, 'samsungtab.jpg', '2020-07-09 09:24:38', '2020-07-09 09:24:38'),
(10, 'Smart Tivi Samsung 4K 55 inch', 13, NULL, NULL, 0, NULL, 'samsungtv.jpg', '2020-07-09 09:27:12', '2020-07-09 09:27:12');

-- --------------------------------------------------------

--
-- Table structure for table `promotion`
--

CREATE TABLE `promotion` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `percent` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `promotion`
--

INSERT INTO `promotion` (`id`, `category_id`, `percent`) VALUES
(19, 10, 10),
(20, 13, 50),
(21, 12, 10);

--
-- Triggers `promotion`
--
DELIMITER $$
CREATE TRIGGER `after_poromotion_update` AFTER UPDATE ON `promotion` FOR EACH ROW BEGIN
UPDATE provide c
LEFT JOIN product p ON p.id = c.product_id
SET c.promotion_price = c.price-(c.price*NEW.percent)/100
WHERE p.category_id=NEW.category_id;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `after_promotion_delete` AFTER DELETE ON `promotion` FOR EACH ROW BEGIN
UPDATE provide c
LEFT JOIN product p ON p.id = c.product_id
SET c.promotion_price = NULL
WHERE p.category_id=OLD.category_id;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `after_promotion_insert` AFTER INSERT ON `promotion` FOR EACH ROW BEGIN
UPDATE provide c
LEFT JOIN product p ON p.id = c.product_id
SET c.promotion_price = c.price-(c.price*NEW.percent)/100
WHERE p.category_id=NEW.category_id;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `provide`
--

CREATE TABLE `provide` (
  `id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` double DEFAULT NULL,
  `promotion_price` double DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `provide`
--

INSERT INTO `provide` (`id`, `seller_id`, `product_id`, `price`, `promotion_price`, `quantity`) VALUES
(1, 1, 1, 9000000, 8100000, 8),
(2, 2, 1, 8900000, 8010000, 15),
(3, 3, 1, 10000000, 9000000, 10),
(4, 3, 2, 12000000, 10800000, 1),
(5, 1, 2, 20000000, 18000000, 5),
(6, 1, 3, 8000000, 7200000, 10),
(7, 2, 3, 8120000, 7308000, 4),
(8, 3, 3, 8500000, 7650000, 12),
(9, 1, 4, 5800000, 5220000, 31),
(10, 2, 4, 6000000, 5400000, 23),
(11, 3, 4, 5950000, 5355000, 6),
(12, 1, 5, 33000000, 29700000, 8),
(13, 2, 5, 35000000, 31500000, 10),
(14, 3, 5, 32500000, 29250000, 15),
(15, 1, 6, 21500000, 10750000, 3),
(16, 2, 6, 20000000, 10000000, 2),
(17, 3, 2, 22000000, 19800000, 6),
(18, 1, 7, 7500000, 3750000, 12),
(19, 2, 7, 8000000, 4000000, 2),
(20, 3, 7, 7200000, 3600000, 5),
(21, 1, 8, 2100000, 1050000, 9),
(22, 2, 8, 2150000, 1075000, 10),
(23, 3, 8, 2300000, 1150000, 20),
(24, 1, 9, 16000000, 8000000, 4),
(25, 2, 9, 16500000, 8250000, 7),
(26, 3, 9, 17000000, 8500000, 12),
(27, 1, 10, 10500000, 5250000, 25),
(28, 2, 10, 10000000, 5000000, 12),
(29, 3, 10, 11000000, 5500000, 12),
(34, 3, 5, 35000000, 31500000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `seller`
--

CREATE TABLE `seller` (
  `id` int(11) NOT NULL,
  `storeName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seller`
--

INSERT INTO `seller` (`id`, `storeName`) VALUES
(1, 'Tiki Trading'),
(2, 'NewTechShop'),
(3, 'FPT Shop');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(60) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `password` varchar(60) NOT NULL,
  `fullname` varchar(60) NOT NULL,
  `birthdate` date DEFAULT NULL,
  `role` tinyint(1) NOT NULL DEFAULT 1,
  `address` varchar(512) DEFAULT NULL,
  `gender` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `phone`, `password`, `fullname`, `birthdate`, `role`, `address`, `gender`, `created`, `modified`) VALUES
(1, 'thanhphattran@gmail.com', '0397400056', 'd1708279c5e2306830202ada5b66a3964d95529f', 'Trần Thành Phát', '0000-00-00', 0, NULL, 1, '2020-07-08 18:43:31', '2020-07-09 14:00:57'),
(2, 'phatkoyz@gmail.com', '0397400057', 'caa3596721856027546aff983438d399a4593734', 'Trần Thành Phát', '2020-08-07', 1, NULL, 0, '2020-07-08 18:57:12', '2020-07-08 18:57:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `benthubagh`
--
ALTER TABLE `benthubagh`
  ADD PRIMARY KEY (`maBenThuBa`),
  ADD KEY `benThuBaGH_ibfk_1` (`maVanChuyenBTB`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `cart_item`
--
ALTER TABLE `cart_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donvivanchuyen`
--
ALTER TABLE `donvivanchuyen`
  ADD PRIMARY KEY (`maDonViVanChuyen`);

--
-- Indexes for table `hoadon`
--
ALTER TABLE `hoadon`
  ADD PRIMARY KEY (`soHoaDon`),
  ADD KEY `hoaDon_ibfk_1` (`maDonHang_HD`),
  ADD KEY `hoaDon_ibfk_2` (`maNhanVienBanHang_HD`);

--
-- Indexes for table `hoidap`
--
ALTER TABLE `hoidap`
  ADD PRIMARY KEY (`maKhachHang_HD`,`maNhanVienCSKH_HD`,`thoiGianHoi`,`cauHoi`,`traLoi`),
  ADD KEY `hoiDap_ibfk_2` (`maNhanVienCSKH_HD`);

--
-- Indexes for table `kho`
--
ALTER TABLE `kho`
  ADD PRIMARY KEY (`maKho`),
  ADD UNIQUE KEY `tenKho` (`tenKho`);

--
-- Indexes for table `kienhang`
--
ALTER TABLE `kienhang`
  ADD PRIMARY KEY (`maKienHang`),
  ADD KEY `kienHang_ibfk_1` (`maDonHang_KH`),
  ADD KEY `kienHang_ibfk_2` (`maKho_KH`),
  ADD KEY `kienHang_ibfk_3` (`maVanChuyen_KH`);

--
-- Indexes for table `nhanvienbanhang`
--
ALTER TABLE `nhanvienbanhang`
  ADD PRIMARY KEY (`maNhanVienBanHang`);

--
-- Indexes for table `nhanviencskh`
--
ALTER TABLE `nhanviencskh`
  ADD PRIMARY KEY (`maNhanVienCSKH`);

--
-- Indexes for table `nhanviengiaohang`
--
ALTER TABLE `nhanviengiaohang`
  ADD PRIMARY KEY (`maNhanVienGiaoHang`),
  ADD KEY `nhanVienGiaoHang_ibfk_2` (`maVanChuyen_NVGH`);

--
-- Indexes for table `nhanvienkho`
--
ALTER TABLE `nhanvienkho`
  ADD PRIMARY KEY (`maNhanVienKho`),
  ADD KEY `nhanVienKho_ibfk_1` (`maNhanVienQuanLy`),
  ADD KEY `nhanVienKho_ibfk_2` (`maKho_NVK`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `provide`
--
ALTER TABLE `provide`
  ADD PRIMARY KEY (`id`),
  ADD KEY `seller_id` (`seller_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `seller`
--
ALTER TABLE `seller`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cart_item`
--
ALTER TABLE `cart_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_item`
--
ALTER TABLE `order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `promotion`
--
ALTER TABLE `promotion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `provide`
--
ALTER TABLE `provide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `seller`
--
ALTER TABLE `seller`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `benthubagh`
--
ALTER TABLE `benthubagh`
  ADD CONSTRAINT `benThuBaGH_ibfk_1` FOREIGN KEY (`maVanChuyenBTB`) REFERENCES `donvivanchuyen` (`maDonViVanChuyen`) ON DELETE CASCADE;

--
-- Constraints for table `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `hoadon`
--
ALTER TABLE `hoadon`
  ADD CONSTRAINT `hoaDon_ibfk_1` FOREIGN KEY (`maDonHang_HD`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `hoaDon_ibfk_2` FOREIGN KEY (`maNhanVienBanHang_HD`) REFERENCES `nhanvienbanhang` (`maNhanVienBanHang`) ON DELETE CASCADE;

--
-- Constraints for table `hoidap`
--
ALTER TABLE `hoidap`
  ADD CONSTRAINT `hoiDap_ibfk_1` FOREIGN KEY (`maKhachHang_HD`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `hoiDap_ibfk_2` FOREIGN KEY (`maNhanVienCSKH_HD`) REFERENCES `nhanviencskh` (`maNhanVienCSKH`) ON DELETE CASCADE;

--
-- Constraints for table `kienhang`
--
ALTER TABLE `kienhang`
  ADD CONSTRAINT `kienHang_ibfk_1` FOREIGN KEY (`maDonHang_KH`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `kienHang_ibfk_2` FOREIGN KEY (`maKho_KH`) REFERENCES `kho` (`maKho`) ON DELETE CASCADE,
  ADD CONSTRAINT `kienHang_ibfk_3` FOREIGN KEY (`maVanChuyen_KH`) REFERENCES `donvivanchuyen` (`maDonViVanChuyen`) ON DELETE CASCADE;

--
-- Constraints for table `nhanvienbanhang`
--
ALTER TABLE `nhanvienbanhang`
  ADD CONSTRAINT `nhanVienBanHang_ibfk_1` FOREIGN KEY (`maNhanVienBanHang`) REFERENCES `nhanvien` (`maNhanVien`) ON DELETE CASCADE;

--
-- Constraints for table `nhanviencskh`
--
ALTER TABLE `nhanviencskh`
  ADD CONSTRAINT `nhanVienCSKH_ibfk_1` FOREIGN KEY (`maNhanVienCSKH`) REFERENCES `nhanvien` (`maNhanVien`) ON DELETE CASCADE;

--
-- Constraints for table `nhanviengiaohang`
--
ALTER TABLE `nhanviengiaohang`
  ADD CONSTRAINT `nhanVienGiaoHang_ibfk_1` FOREIGN KEY (`maNhanVienGiaoHang`) REFERENCES `nhanvien` (`maNhanVien`) ON DELETE CASCADE,
  ADD CONSTRAINT `nhanVienGiaoHang_ibfk_2` FOREIGN KEY (`maVanChuyen_NVGH`) REFERENCES `donvivanchuyen` (`maDonViVanChuyen`) ON DELETE CASCADE;

--
-- Constraints for table `nhanvienkho`
--
ALTER TABLE `nhanvienkho`
  ADD CONSTRAINT `nhanVienKho_ibfk_1` FOREIGN KEY (`maNhanVienQuanLy`) REFERENCES `nhanvienkho` (`maNhanVienKho`) ON DELETE CASCADE,
  ADD CONSTRAINT `nhanVienKho_ibfk_2` FOREIGN KEY (`maKho_NVK`) REFERENCES `kho` (`maKho`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_item`
--
ALTER TABLE `order_item`
  ADD CONSTRAINT `order_item_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_item_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `provide`
--
ALTER TABLE `provide`
  ADD CONSTRAINT `provide_ibfk_1` FOREIGN KEY (`seller_id`) REFERENCES `seller` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `provide_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
