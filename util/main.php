<?php
// Get the document root
$doc_root = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT', FILTER_SANITIZE_STRING);
// doc_root is C:/xampp/htdocs

// Get the application path
$uri = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_SANITIZE_STRING);
// uri is here /assignment-2/main.php link to main.php

$dirs = explode('/', $uri);
$app_path = '/' . $dirs[1] . '/';

// Set the include path
set_include_path($doc_root . $app_path);


// Get common code

function display_error($error_message) {
    global $app_path;
    include 'view/error.php';
    exit;
}

function display_success($msg) {
    global $app_path;
    include 'view/success.php';
    exit;
}

function redirect($url) {
    session_write_close();
    header("Location: " . $url);
    exit;
}

// Start session to store user and cart data
session_start();
?>